<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue child scripts
 */
if ( ! function_exists( 'hdia_child_enqueue_scripts' ) ) {
	function hdia_child_enqueue_scripts() {
		wp_enqueue_style( 'hdia-style', HDIA_THEME_URI . "/style.css" );
		wp_enqueue_style( 'hdia-child-style', get_stylesheet_directory_uri() . '/style.css', array( 'hdia-style' ), wp_get_theme()->get( 'Version' ) );

		// Enqueue BS Script for Dev.
		$domain = wp_parse_url( get_stylesheet_directory_uri() );
		$host   = $domain['host'];

		if ( strpos( $host, '.local' ) !== false || 'localhost' === $host ) {
			$url = sprintf( 'http://%s:3000/browser-sync/browser-sync-client.js', $host );
			$ch  = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
			$header = curl_exec( $ch );
			curl_close( $ch );
			if ( $header && strpos( $header[0], '400' ) === false ) {
				wp_enqueue_script( '__bs_script__', $url, array(), null, true );
			}
		}
	}
}
add_action( 'wp_enqueue_scripts', 'hdia_child_enqueue_scripts' );
