<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue child scripts
 */
if ( ! function_exists( 'hdia_child_enqueue_scripts' ) ) {
	function hdia_child_enqueue_scripts() {
		wp_enqueue_style( 'hdia-style', HDIA_THEME_URI . "/style.css" );
		wp_enqueue_style( 'hdia-child-style', get_stylesheet_directory_uri() . '/style.css', array( 'hdia-style' ), wp_get_theme()->get( 'Version' ) );
	}
}
add_action( 'wp_enqueue_scripts', 'hdia_child_enqueue_scripts' );
