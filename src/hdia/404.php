<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link    https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Hdia
 * @since   1.0
 */

get_header();
?>

	<div class="container">
		<div class="row row-xs-center maintenance-page" id="maintenance-wrap">
			<div class="col-md-12">
				<h1 class="error-404-big-title"><?php echo esc_html( '404', 'hdia' ) ?></h1>
				<h2 class="error-404-title">
					<?php echo esc_html( Hdia::setting( 'error404_page_title' ) ); ?>
				</h2>
				<div class="error-404-sub-title">
					<?php echo esc_html( Hdia::setting( 'error404_sub_title' ) ); ?>
				</div>

				<div class="error-404-search-form-wrap">
					<?php get_search_form(); ?>
				</div>
			</div>
		</div>
	</div>

<?php get_footer();
