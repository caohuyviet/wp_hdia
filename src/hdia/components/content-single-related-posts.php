<?php
$number_post = Hdia::setting( 'single_post_related_number' );
$results     = Hdia_Query::get_related_posts( array(
	'post_id'      => get_the_ID(),
	'number_posts' => $number_post,
) );

if ( $results !== false && $results->have_posts() ) : ?>
	<div class="related-posts">
		<h3 class="related-title">
			<?php esc_html_e( 'Bài viết liên quan', 'hdia' ); ?>
		</h3>
		<div class="tm-swiper equal-height"
		     data-lg-items="2"
		     data-md-items="2"
		     data-sm-items="1"
		     data-lg-gutter="30"
		     data-pagination="1"
		     data-slides-per-group="inherit"
		>
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<?php while ( $results->have_posts() ) : $results->the_post(); ?>
						<div class="swiper-slide">
							<div class="related-post-item">
								<div class="post-item-wrap">

									<?php if ( has_post_thumbnail() ) { ?>
										<div class="post-feature-wrap">
											<div class="post-feature post-thumbnail">
												<?php if ( has_category() ) : ?>
													<div class="post-categories">
														<?php the_category( ' ' ); ?>
													</div>
												<?php endif; ?>

												<a href="<?php the_permalink(); ?>">
													<?php
													$full_image_size = get_the_post_thumbnail_url( null, 'full' );
													Hdia_Helper::get_lazy_load_image( array(
														'url'    => $full_image_size,
														'width'  => 470,
														'height' => 298,
														'crop'   => true,
														'echo'   => true,
														'alt'    => get_the_title(),
													) );
													?>
												</a>
											</div>
										</div>
									<?php } ?>

									<div class="post-info">
										<div class="post-meta">
											<div class="post-date">
												<span class="meta-icon"><i class="far fa-calendar"></i></span>
												<?php echo get_the_date(); ?>
											</div>

											<?php if ( class_exists( 'InsightCore_View' ) ) : ?>
												<div class="post-view">
													<span class="meta-icon">
														<i class="far fa-eye"></i>
													</span>

													<?php
													$views = InsightCore_View::get_views();
													printf( esc_html( _n( '%1$s View', '%1$s Views', $views, 'hdia' ) ), $views );
													?>
												</div>
											<?php endif; ?>
										</div>

										<?php get_template_part( 'loop/blog/title' ); ?>

										<div class="post-excerpt">
											<?php Hdia_Templates::excerpt( array(
												'limit' => 120,
												'type'  => 'character',
											) ); ?>
										</div>

										<div class="post-read-more">
											<a href="<?php the_permalink(); ?>">
												<span class="btn-text">
													<?php esc_html_e( 'Chi tiết', 'hdia' ); ?>
												</span>
												<span class="btn-icon ion-arrow-right-c"></span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif;
wp_reset_postdata();
