<?php
/**
 * Template part for displaying single post pages.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hdia
 * @since   1.0
 */

$_post_title = Hdia::setting( 'single_post_title_enable' );
$format      = '';
if ( get_post_format() !== false ) {
	$format = get_post_format();
}
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( Hdia::setting( 'single_post_feature_enable' ) === '1' ) : ?>
			<?php get_template_part( 'loop/blog-single/format', $format ); ?>
		<?php endif; ?>

		<div class="entry-header">
			<?php if ( Hdia::setting( 'single_post_categories_enable' ) === '1' && has_category() ) : ?>
				<div class="post-categories">
					<?php the_category( ' ' ); ?>
				</div>
			<?php endif; ?>

			<?php if ( $_post_title === '1' ) : ?>
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<?php endif; ?>

			<?php get_template_part( 'loop/blog-single/meta' ); ?>
		</div>

		<div class="entry-content">
			<?php
			the_content( sprintf( wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'hdia' ), array( 'span' => array( 'class' => array() ) ) ), the_title( '<span class="screen-reader-text">"', '"</span>', false ) ) );

			Hdia_Templates::page_links();
			?>
		</div>

		<?php if ( ( Hdia::setting( 'single_post_tags_enable' ) === '1' && has_tag() ) || Hdia::setting( 'single_post_share_enable' ) === '1' ) : ?>
			<?php
			if ( ( Hdia::setting( 'single_post_tags_enable' ) === '1' && has_tag() ) && Hdia::setting( 'single_post_share_enable' ) === '1' ) {
				$entry_footer_col = 'col-md-6';
			} else {
				$entry_footer_col = 'col-md-12';
			}
			?>
			<div class="entry-footer">
				<div class="row row-xs-center">
					<div class="<?php echo esc_attr( $entry_footer_col ); ?>">
						<?php if ( Hdia::setting( 'single_post_tags_enable' ) === '1' && has_tag() ) : ?>
							<div class="post-tags">
								<span class="heading-color"><?php esc_html_e( 'Tags', 'hdia' ); ?></span>
								<?php the_tags( '', ', ', '' ); ?>
							</div>
						<?php endif; ?>
					</div>
					<div class="<?php echo esc_attr( $entry_footer_col ); ?>">
						<?php if ( Hdia::setting( 'single_post_share_enable' ) === '1' ) : ?>
							<?php Hdia_Templates::post_sharing(); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

	</article>
<?php
$author_desc = get_the_author_meta( 'description' );
if ( Hdia::setting( 'single_post_author_box_enable' ) === '1' && ! empty( $author_desc ) ) {
	Hdia_Templates::post_author();
}
