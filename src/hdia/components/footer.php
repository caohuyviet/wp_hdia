<?php
$footer_page = Hdia_Global::instance()->get_footer_type();

if ( $footer_page === '' ) {
	return;
}

$footer = get_page_by_path( $footer_page, OBJECT, 'ic_footer' );
if ( empty( $footer->ID ) ) {
	return;
}

$_hdia_args = array(
	'post_type'   => 'ic_footer',
	'post_status' => 'publish',
	'p'           => $footer->ID,
);

$_hdia_query = new WP_Query( $_hdia_args );
?>
<?php if ( $_hdia_query->have_posts() ) { ?>
	<?php while ( $_hdia_query->have_posts() ) : $_hdia_query->the_post(); ?>
		<?php
		$footer_options      = unserialize( get_post_meta( get_the_ID(), 'insight_footer_options', true ) );
		$_effect             = Hdia_Helper::get_the_post_meta( $footer_options, 'effect', '' );
		$_style              = Hdia_Helper::get_the_post_meta( $footer_options, 'style', '01' );
		$footer_wrap_classes = "page-footer-wrapper $footer_page footer-style-$_style";

		if ( $_effect !== '' ) {
			$footer_wrap_classes .= " {$_effect}";
		}
		?>
		<div id="page-footer-wrapper" class="<?php echo esc_attr( $footer_wrap_classes ); ?>">
			<div id="page-footer" <?php Hdia::footer_class(); ?>>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="page-footer-inner">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	endwhile;
	wp_reset_postdata();
}
