<header id="page-header" <?php Hdia::header_class(); ?>>
	<div id="page-header-inner" class="page-header-inner" data-sticky="1">
		<div class="header-above">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="header-wrap">
							<?php get_template_part( 'components/branding' ); ?>
							<div class="header-right">
								<?php Hdia_Templates::header_info_slider(); ?>
								<?php Hdia_Templates::header_open_mobile_menu_button(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="header-below">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="header-below-inner">
							<div class="header-below-left">
								<?php get_template_part( 'components/navigation' ); ?>
							</div>
							<div class="header-below-right">
								<?php Hdia_Templates::header_language_switcher(); ?>
								<?php Hdia_Templates::header_social_networks(); ?>
								<?php Hdia_Templates::header_search_button(); ?>
								<?php Hdia_Woo::header_mini_cart(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
