<header id="page-header" <?php Hdia::header_class(); ?>>
	<div id="page-header-inner" class="page-header-inner" data-sticky="1">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="header-wrap">

						<?php get_template_part( 'components/branding' ); ?>

						<?php get_template_part( 'components/navigation' ); ?>

						<div class="header-right">
							<div class="header-right-wrap">
								<div id="header-right-inner" class="header-right-inner">
									<?php Hdia_Templates::header_language_switcher(); ?>
									<?php Hdia_Templates::header_search_button(); ?>
									<?php Hdia_Woo::header_mini_cart(); ?>
									<?php Hdia_Templates::header_button(); ?>
								</div>
							</div>

							<?php Hdia_Templates::header_more_tools_button(); ?>
							<?php Hdia_Templates::header_open_mobile_menu_button(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
