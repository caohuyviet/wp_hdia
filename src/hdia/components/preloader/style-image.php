<div class="sk-image">
	<img src="<?php echo esc_url( Hdia::setting( 'preloader_image' ) ); ?>"
	     alt="<?php echo esc_attr( 'preloader', 'hdia' ); ?>"/>
</div>
