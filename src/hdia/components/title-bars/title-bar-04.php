<?php
$enabled = Hdia::setting( "title_bar_04_breadcrumb_enable" );
?>

<div id="page-title-bar" class="page-title-bar page-title-bar-04">
	<div class="page-title-bar-overlay"></div>

	<div class="page-title-bar-inner">
		<div class="container">
			<div class="row row-xs-center">
				<div class="col-md-8">
					<?php Hdia_Templates::get_title_bar_title(); ?>

					<?php if ( '1' === $enabled ) {
						get_template_part( 'components/breadcrumb' );
					} ?>
				</div>
			</div>
		</div>
	</div>
</div>
