<?php
$panel    = 'advanced';
$priority = 1;

Hdia_Kirki::add_section( 'advanced', array(
	'title'    => esc_html__( 'Advanced', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'preloader', array(
	'title'    => esc_html__( 'Pre Loader', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'light_gallery', array(
	'title'    => esc_html__( 'Light Gallery', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
