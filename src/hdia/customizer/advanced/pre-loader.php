<?php
$section  = 'preloader';
$priority = 1;
$prefix   = 'preloader_';

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'enable',
	'label'    => esc_html__( 'Enable Preloader', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'No', 'hdia' ),
		'1' => esc_html__( 'Yes', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => $prefix . 'style',
	'label'    => esc_html__( 'Style', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'circle',
	'choices'  => Hdia_Helper::get_preloader_list(),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'background_color',
	'label'       => esc_html__( 'Background Color', 'hdia' ),
	'description' => esc_html__( 'Controls the background color for pre loader', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => 'rgba(255, 255, 255, 1)',
	'output'      => array(
		array(
			'element'  => '.page-loading',
			'property' => 'background-color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'shape_color',
	'label'       => esc_html__( 'Shape Color', 'hdia' ),
	'description' => esc_html__( 'Controls the background color for pre loader', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Hdia::PRIMARY_COLOR,
	'output'      => array(
		array(
			'element'  => '
			.page-loading .sk-bg-self,
			.page-loading .sk-bg-child > div,
			.page-loading .sk-bg-child-before > div:before
			',
			'property' => 'background-color',
			'suffix'   => '!important',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'image',
	'settings' => 'preloader_image',
	'label'    => esc_html__( 'Image (for Image style)', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => HDIA_THEME_IMAGE_URI . '/preloader.gif',
) );
