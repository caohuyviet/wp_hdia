<?php
$panel    = 'blog';
$priority = 1;

Hdia_Kirki::add_section( 'blog_archive', array(
	'title'    => esc_html__( 'Blog Archive', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'blog_single', array(
	'title'    => esc_html__( 'Blog Single Post', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
