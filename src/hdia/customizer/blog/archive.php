<?php
$section  = 'blog_archive';
$priority = 1;
$prefix   = 'blog_archive_';

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => $prefix . 'style',
	'label'       => esc_html__( 'Blog Style', 'hdia' ),
	'description' => esc_html__( 'Select blog style that display for archive pages.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'grid_classic_05',
	'choices'     => array(
		'list'            => esc_html__( 'List Large Image', 'hdia' ),
		'grid_classic_01' => esc_html__( 'Grid Classic 01', 'hdia' ),
		'grid_classic_02' => esc_html__( 'Grid Classic 02', 'hdia' ),
		'grid_classic_03' => esc_html__( 'Grid Classic 03', 'hdia' ),
		'grid_classic_04' => esc_html__( 'Grid Classic 04', 'hdia' ),
		'grid_classic_05' => esc_html__( 'Grid Classic 05', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => 'blog_archive_columns',
	'label'       => esc_html__( 'Columns', 'hdia' ),
	'description' => esc_html__( 'Columns of grid style', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'xs:1;sm:2;md:2;lg:2',
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'number',
	'settings' => 'blog_archive_gutter',
	'label'    => esc_html__( 'Gutter', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 30,
	'choices'  => array(
		'min'  => 0,
		'step' => 1,
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'number',
	'settings' => 'blog_archive_row_gutter',
	'label'    => esc_html__( 'Row Gutter', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 70,
	'choices'  => array(
		'min'  => 0,
		'step' => 1,
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'blog_archive_animation',
	'label'       => esc_html__( 'Animation', 'hdia' ),
	'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'move-up',
	'choices'     => array(
		'none'             => esc_attr__( 'None', 'hdia' ),
		'fade-in'          => esc_attr__( 'Fade In', 'hdia' ),
		'move-up'          => esc_attr__( 'Move Up', 'hdia' ),
		'scale-up'         => esc_attr__( 'Scale Up', 'hdia' ),
		'fall-perspective' => esc_attr__( 'Fall Perspective', 'hdia' ),
		'fly'              => esc_attr__( 'Fly', 'hdia' ),
		'flip'             => esc_attr__( 'Flip', 'hdia' ),
		'helix'            => esc_attr__( 'Helix', 'hdia' ),
		'pop-up'           => esc_attr__( 'Pop Up', 'hdia' ),
	),
) );
