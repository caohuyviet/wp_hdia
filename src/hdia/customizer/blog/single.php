<?php
$section  = 'blog_single';
$priority = 1;
$prefix   = 'single_post_';

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'background',
	'settings'    => $prefix . 'banner_background',
	'label'       => esc_html__( 'Banner Background', 'hdia' ),
	'description' => esc_html__( 'Controls the background of banner in single blog posts style 02.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'background-color'      => '#222',
		'background-image'      => HDIA_THEME_IMAGE_URI . '/title-bar-bg-blog.jpg',
		'background-repeat'     => 'no-repeat',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
		'background-position'   => 'center center',
	),
	'output'      => array(
		array(
			'element' => '.entry-banner',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_feature_enable',
	'label'       => esc_html__( 'Featured Image', 'hdia' ),
	'description' => esc_html__( 'Turn on to display featured image on blog single posts.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_title_enable',
	'label'       => esc_html__( 'Post Title', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the post title.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_categories_enable',
	'label'       => esc_html__( 'Categories', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the categories on blog single posts.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_tags_enable',
	'label'       => esc_html__( 'Tags', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the tags on blog single posts.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_date_enable',
	'label'       => esc_html__( 'Post Meta Date', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the date on blog single posts.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_author_enable',
	'label'       => esc_html__( 'Post Author', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the author on blog single posts.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_like_enable',
	'label'       => esc_html__( 'Post Like', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the like button on blog single posts.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_view_enable',
	'label'       => esc_html__( 'Post View', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the view button on blog single posts.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_comment_count_enable',
	'label'       => esc_html__( 'Comment Count', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the comment count on blog single posts.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_share_enable',
	'label'       => esc_html__( 'Post Sharing', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the social sharing on blog single posts.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_author_box_enable',
	'label'       => esc_html__( 'Author Info Box', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the author info box on blog single posts.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_pagination_enable',
	'label'       => esc_html__( 'Previous/Next Pagination', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the previous/next post pagination on blog single posts.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_related_enable',
	'label'       => esc_html__( 'Related', 'hdia' ),
	'description' => esc_html__( 'Turn on to display related posts on blog single posts.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'            => 'number',
	'settings'        => 'single_post_related_number',
	'label'           => esc_html__( 'Number of related posts item', 'hdia' ),
	'section'         => $section,
	'priority'        => $priority ++,
	'default'         => 3,
	'choices'         => array(
		'min'  => 0,
		'max'  => 50,
		'step' => 1,
	),
	'active_callback' => array(
		array(
			'setting'  => 'single_post_related_enable',
			'operator' => '==',
			'value'    => '1',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_comment_enable',
	'label'       => esc_html__( 'Comments', 'hdia' ),
	'description' => esc_html__( 'Turn on to display comments on blog single posts.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );
