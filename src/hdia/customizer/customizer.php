<?php
/**
 * Theme Customizer
 *
 * @package Hdia
 * @since   1.0
 */

/**
 * Setup configuration
 */
Hdia_Kirki::add_config( 'theme', array(
	'option_type' => 'theme_mod',
	'capability'  => 'edit_theme_options',
) );

/**
 * Add sections
 */
$priority = 1;

Hdia_Kirki::add_section( 'layout', array(
	'title'    => esc_html__( 'Layout', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'color_', array(
	'title'    => esc_html__( 'Colors', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'background', array(
	'title'    => esc_html__( 'Background', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'typography', array(
	'title'    => esc_html__( 'Typography', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_panel( 'top_bar', array(
	'title'    => esc_html__( 'Top bar', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_panel( 'header', array(
	'title'    => esc_html__( 'Header', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'logo', array(
	'title'    => esc_html__( 'Logo', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_panel( 'navigation', array(
	'title'    => esc_html__( 'Navigation', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'sliders', array(
	'title'    => esc_html__( 'Sliders', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_panel( 'title_bar', array(
	'title'    => esc_html__( 'Page Title Bar', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'sidebars', array(
	'title'    => esc_html__( 'Sidebars', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_panel( 'footer', array(
	'title'    => esc_html__( 'Footer', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_panel( 'blog', array(
	'title'    => esc_html__( 'Blog', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_panel( 'shop', array(
	'title'    => esc_html__( 'Shop', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_panel( 'project', array(
	'title'    => esc_html__( 'Project', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_panel( 'service', array(
	'title'    => esc_html__( 'Service', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'socials', array(
	'title'    => esc_html__( 'Social Networks', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'social_sharing', array(
	'title'    => esc_html__( 'Social Sharing', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_panel( 'search', array(
	'title'    => esc_html__( 'Search', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'error404_page', array(
	'title'    => esc_html__( 'Error 404 Page', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_panel( 'shortcode', array(
	'title'    => esc_html__( 'Shortcodes', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_panel( 'advanced', array(
	'title'    => esc_html__( 'Advanced', 'hdia' ),
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'additional_js', array(
	'title'    => esc_html__( 'Additional JS', 'hdia' ),
	'priority' => $priority ++,
) );

/**
 * Load panel & section files
 */
$files = array(
	HDIA_CUSTOMIZER_DIR . '/section-color.php',

	HDIA_CUSTOMIZER_DIR . '/top_bar/_panel.php',
	HDIA_CUSTOMIZER_DIR . '/top_bar/general.php',
	HDIA_CUSTOMIZER_DIR . '/top_bar/style-01.php',

	HDIA_CUSTOMIZER_DIR . '/header/_panel.php',
	HDIA_CUSTOMIZER_DIR . '/header/general.php',
	HDIA_CUSTOMIZER_DIR . '/header/sticky.php',
	HDIA_CUSTOMIZER_DIR . '/header/more-options.php',
	HDIA_CUSTOMIZER_DIR . '/header/style-01.php',
	HDIA_CUSTOMIZER_DIR . '/header/style-02.php',
	HDIA_CUSTOMIZER_DIR . '/header/style-03.php',
	HDIA_CUSTOMIZER_DIR . '/header/style-04.php',

	HDIA_CUSTOMIZER_DIR . '/navigation/_panel.php',
	HDIA_CUSTOMIZER_DIR . '/navigation/desktop-menu.php',
	HDIA_CUSTOMIZER_DIR . '/navigation/off-canvas-menu.php',
	HDIA_CUSTOMIZER_DIR . '/navigation/mobile-menu.php',

	HDIA_CUSTOMIZER_DIR . '/section-sliders.php',

	HDIA_CUSTOMIZER_DIR . '/title_bar/_panel.php',
	HDIA_CUSTOMIZER_DIR . '/title_bar/general.php',
	HDIA_CUSTOMIZER_DIR . '/title_bar/style-01.php',
	HDIA_CUSTOMIZER_DIR . '/title_bar/style-02.php',
	HDIA_CUSTOMIZER_DIR . '/title_bar/style-03.php',
	HDIA_CUSTOMIZER_DIR . '/title_bar/style-04.php',
	HDIA_CUSTOMIZER_DIR . '/title_bar/style-05.php',
	HDIA_CUSTOMIZER_DIR . '/title_bar/style-06.php',

	HDIA_CUSTOMIZER_DIR . '/footer/_panel.php',
	HDIA_CUSTOMIZER_DIR . '/footer/general.php',
	HDIA_CUSTOMIZER_DIR . '/footer/style-01.php',
	HDIA_CUSTOMIZER_DIR . '/footer/style-02.php',
	HDIA_CUSTOMIZER_DIR . '/footer/style-03.php',
	HDIA_CUSTOMIZER_DIR . '/footer/style-04.php',
	HDIA_CUSTOMIZER_DIR . '/footer/style-05.php',
	HDIA_CUSTOMIZER_DIR . '/footer/style-06.php',
	HDIA_CUSTOMIZER_DIR . '/footer/style-07.php',
	HDIA_CUSTOMIZER_DIR . '/footer/style-08.php',
	HDIA_CUSTOMIZER_DIR . '/footer/style-10.php',
	HDIA_CUSTOMIZER_DIR . '/footer/style-11.php',
	HDIA_CUSTOMIZER_DIR . '/footer/footer-simple.php',

	HDIA_CUSTOMIZER_DIR . '/advanced/_panel.php',
	HDIA_CUSTOMIZER_DIR . '/advanced/advanced.php',
	HDIA_CUSTOMIZER_DIR . '/advanced/pre-loader.php',
	HDIA_CUSTOMIZER_DIR . '/advanced/light-gallery.php',

	HDIA_CUSTOMIZER_DIR . '/shortcode/_panel.php',
	HDIA_CUSTOMIZER_DIR . '/shortcode/animation.php',

	HDIA_CUSTOMIZER_DIR . '/section-background.php',
	HDIA_CUSTOMIZER_DIR . '/section-error404.php',
	HDIA_CUSTOMIZER_DIR . '/section-layout.php',
	HDIA_CUSTOMIZER_DIR . '/section-logo.php',

	HDIA_CUSTOMIZER_DIR . '/blog/_panel.php',
	HDIA_CUSTOMIZER_DIR . '/blog/archive.php',
	HDIA_CUSTOMIZER_DIR . '/blog/single.php',

	HDIA_CUSTOMIZER_DIR . '/project/_panel.php',
	HDIA_CUSTOMIZER_DIR . '/project/archive.php',
	HDIA_CUSTOMIZER_DIR . '/project/single.php',

	HDIA_CUSTOMIZER_DIR . '/service/_panel.php',
	HDIA_CUSTOMIZER_DIR . '/service/archive.php',
	HDIA_CUSTOMIZER_DIR . '/service/single.php',

	HDIA_CUSTOMIZER_DIR . '/shop/_panel.php',
	HDIA_CUSTOMIZER_DIR . '/shop/archive.php',
	HDIA_CUSTOMIZER_DIR . '/shop/single.php',
	HDIA_CUSTOMIZER_DIR . '/shop/cart.php',

	HDIA_CUSTOMIZER_DIR . '/search/_panel.php',
	HDIA_CUSTOMIZER_DIR . '/search/search-page.php',
	HDIA_CUSTOMIZER_DIR . '/search/search-popup.php',

	HDIA_CUSTOMIZER_DIR . '/section-sharing.php',
	HDIA_CUSTOMIZER_DIR . '/section-sidebars.php',
	HDIA_CUSTOMIZER_DIR . '/section-socials.php',
	HDIA_CUSTOMIZER_DIR . '/section-typography.php',
	HDIA_CUSTOMIZER_DIR . '/section-additional-js.php',
);

Hdia::require_files( $files );
