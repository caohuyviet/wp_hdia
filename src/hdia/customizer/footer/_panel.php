<?php
$panel    = 'footer';
$priority = 1;

Hdia_Kirki::add_section( 'footer', array(
	'title'    => esc_html__( 'General', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'footer_01', array(
	'title'    => esc_html__( 'Style 01', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'footer_02', array(
	'title'    => esc_html__( 'Style 02', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'footer_03', array(
	'title'    => esc_html__( 'Style 03', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'footer_04', array(
	'title'    => esc_html__( 'Style 04', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'footer_05', array(
	'title'    => esc_html__( 'Style 05', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'footer_06', array(
	'title'    => esc_html__( 'Style 06', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'footer_07', array(
	'title'    => esc_html__( 'Style 07', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'footer_08', array(
	'title'    => esc_html__( 'Style 08', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'footer_10', array(
	'title'    => esc_html__( 'Style 10', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'footer_11', array(
	'title'    => esc_html__( 'Style 11', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'footer_simple', array(
	'title'    => esc_html__( 'Footer Simple', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
