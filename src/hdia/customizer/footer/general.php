<?php
$section  = 'footer';
$priority = 1;
$prefix   = 'footer_';

$footers = Hdia_Footer::get_list_footers( true );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => $prefix . 'page',
	'label'       => esc_html__( 'Footer', 'hdia' ),
	'description' => esc_html__( 'Select a default footer for all pages.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'footer-01',
	'choices'     => Hdia_Footer::get_list_footers(),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_service_footer_page',
	'label'       => esc_html__( 'Single Service', 'hdia' ),
	'description' => esc_html__( 'Select default footer that displays on all single service pages.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'footer-02',
	'choices'     => $footers,
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_project_footer_page',
	'label'       => esc_html__( 'Single Project', 'hdia' ),
	'description' => esc_html__( 'Select default footer that displays on all single project pages.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'footer-01',
	'choices'     => $footers,
) );
