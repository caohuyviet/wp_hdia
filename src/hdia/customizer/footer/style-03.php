<?php
$section  = 'footer_03';
$priority = 1;
$prefix   = 'footer_03_';

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'widget_title_color',
	'label'     => esc_html__( 'Widget Title Color', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => '#fff',
	'output'    => array(
		array(
			'element'  => '.footer-style-03 .widgettitle, .footer-style-03 .tm-mailchimp-form .title',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'widget_title_margin_bottom',
	'label'     => esc_html__( 'Widget Title Margin Bottom', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 40,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 100,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.footer-style-03 .widgettitle, .footer-style-03 .tm-mailchimp-form .title',
			'property' => 'margin-bottom',
			'units'    => 'px',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'text_color',
	'label'     => esc_html__( 'Text Color', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => '#777',
	'output'    => array(
		array(
			'element'  => '.footer-style-03, .footer-style-03 .widget_text, .footer-style-03 .tm-mailchimp-form.style-11 input[type=\'email\']',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'link_color',
	'label'     => esc_html__( 'Link Color', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => '#777',
	'output'    => array(
		array(
			'element'  => '
			.footer-style-03 a,
            .footer-style-03 .widget_recent_entries li a,
            .footer-style-03 .widget_recent_comments li a,
            .footer-style-03 .widget_archive li a,
            .footer-style-03 .widget_categories li a,
            .footer-style-03 .widget_meta li a,
            .footer-style-03 .widget_product_categories li a,
            .footer-style-03 .widget_rss li a,
            .footer-style-03 .widget_pages li a,
            .footer-style-03 .widget_nav_menu li a,
            .footer-style-03 .insight-core-bmw li a
			',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'link_hover_color',
	'label'     => esc_html__( 'Link Hover Color', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => '#FF732F',
	'output'    => array(
		array(
			'element'  => '
			.footer-style-03 a:hover,
            .footer-style-03 .widget_recent_entries li a:hover,
            .footer-style-03 .widget_recent_comments li a:hover,
            .footer-style-03 .widget_archive li a:hover,
            .footer-style-03 .widget_categories li a:hover,
            .footer-style-03 .widget_meta li a:hover,
            .footer-style-03 .widget_product_categories li a:hover,
            .footer-style-03 .widget_rss li a:hover,
            .footer-style-03 .widget_pages li a:hover,
            .footer-style-03 .widget_nav_menu li a:hover,
            .footer-style-03 .insight-core-bmw li a:hover 
			',
			'property' => 'color',
		),
	),
) );
