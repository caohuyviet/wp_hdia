<?php
$section  = 'footer_05';
$priority = 1;
$prefix   = 'footer_05_';

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'kirki_typography',
	'settings'    => $prefix . 'widget_title_typo',
	'label'       => esc_html__( 'Title Font', 'hdia' ),
	'description' => esc_html__( 'These settings control the typography for title.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => Hdia::PRIMARY_FONT,
		'variant'        => '300',
		'line-height'    => '1.23',
		'letter-spacing' => '0em',
		'text-transform' => 'capitalize',
	),
	'output'      => array(
		array(
			'element' => '.footer-style-05 .widgettitle, .footer-style-05 .tm-mailchimp-form .title',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'widget_title_font_size',
	'label'     => esc_html__( 'Font size', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 30,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.footer-style-05 .widgettitle, .footer-style-05 .tm-mailchimp-form .title',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'widget_title_color',
	'label'     => esc_html__( 'Widget Title Color', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => '#fff',
	'output'    => array(
		array(
			'element'  => '.footer-style-05 .widgettitle, .footer-style-05 .tm-mailchimp-form .title',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'widget_title_margin_bottom',
	'label'     => esc_html__( 'Widget Title Margin Bottom', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 40,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 100,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.footer-style-05 .widgettitle, .footer-style-05 .tm-mailchimp-form .title',
			'property' => 'margin-bottom',
			'units'    => 'px',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'text_color',
	'label'     => esc_html__( 'Text Color', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => '#777',
	'output'    => array(
		array(
			'element'  => '.footer-style-05, .footer-style-05 .widget_text, .footer-style-05 .tm-mailchimp-form.style-11 input[type=\'email\']',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'link_color',
	'label'     => esc_html__( 'Link Color', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => '#777',
	'output'    => array(
		array(
			'element'  => '
			.footer-style-05 a,
            .footer-style-05 .widget_recent_entries li a,
            .footer-style-05 .widget_recent_comments li a,
            .footer-style-05 .widget_archive li a,
            .footer-style-05 .widget_categories li a,
            .footer-style-05 .widget_meta li a,
            .footer-style-05 .widget_product_categories li a,
            .footer-style-05 .widget_rss li a,
            .footer-style-05 .widget_pages li a,
            .footer-style-05 .widget_nav_menu li a,
            .footer-style-05 .insight-core-bmw li a
			',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'link_hover_color',
	'label'     => esc_html__( 'Link Hover Color', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => '#F07036',
	'output'    => array(
		array(
			'element'  => '
			.footer-style-05 a:hover,
            .footer-style-05 .widget_recent_entries li a:hover,
            .footer-style-05 .widget_recent_comments li a:hover,
            .footer-style-05 .widget_archive li a:hover,
            .footer-style-05 .widget_categories li a:hover,
            .footer-style-05 .widget_meta li a:hover,
            .footer-style-05 .widget_product_categories li a:hover,
            .footer-style-05 .widget_rss li a:hover,
            .footer-style-05 .widget_pages li a:hover,
            .footer-style-05 .widget_nav_menu li a:hover,
            .footer-style-05 .insight-core-bmw li a:hover 
			',
			'property' => 'color',
		),
	),
) );
