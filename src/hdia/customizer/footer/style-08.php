<?php
$section  = 'footer_08';
$priority = 1;
$prefix   = 'footer_08_';

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'widget_title_color',
	'label'     => esc_html__( 'Widget Title Color', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => '#fff',
	'output'    => array(
		array(
			'element'  => '.footer-style-08 .widgettitle, .footer-style-08 .tm-mailchimp-form .title',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'widget_title_border_color',
	'label'     => esc_html__( 'Widget Title Border Color', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => 'rgba(238, 238, 238, 0.13)',
	'output'    => array(
		array(
			'element'  => '.footer-style-08 .widgettitle, .footer-style-08 .tm-mailchimp-form .title',
			'property' => 'border-color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'widget_title_padding_bottom',
	'label'     => esc_html__( 'Widget Title Padding Bottom', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 26,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 100,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.footer-style-08 .widgettitle, .footer-style-08 .tm-mailchimp-form .title',
			'property' => 'padding-bottom',
			'units'    => 'px',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'widget_title_margin_bottom',
	'label'     => esc_html__( 'Widget Title Margin Bottom', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 42,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 100,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.footer-style-08 .widgettitle, .footer-style-08 .tm-mailchimp-form .title',
			'property' => 'margin-bottom',
			'units'    => 'px',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'text_color',
	'label'     => esc_html__( 'Text Color', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => 'rgba(255, 255, 255, 0.5)',
	'output'    => array(
		array(
			'element'  => '.footer-style-08, .footer-style-08 .widget_text, .footer-style-08 .tm-mailchimp-form.style-11 input[type=\'email\']',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'link_color',
	'label'     => esc_html__( 'Link Color', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => 'rgba(255, 255, 255, 0.5)',
	'output'    => array(
		array(
			'element'  => '
			.footer-style-08 a,
            .footer-style-08 .widget_recent_entries li a,
            .footer-style-08 .widget_recent_comments li a,
            .footer-style-08 .widget_archive li a,
            .footer-style-08 .widget_categories li a,
            .footer-style-08 .widget_meta li a,
            .footer-style-08 .widget_product_categories li a,
            .footer-style-08 .widget_rss li a,
            .footer-style-08 .widget_pages li a,
            .footer-style-08 .widget_nav_menu li a,
            .footer-style-08 .insight-core-bmw li a
			',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'link_hover_color',
	'label'     => esc_html__( 'Link Hover Color', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => '#7F60D1',
	'output'    => array(
		array(
			'element'  => '
			.footer-style-08 a:hover,
            .footer-style-08 .widget_recent_entries li a:hover,
            .footer-style-08 .widget_recent_comments li a:hover,
            .footer-style-08 .widget_archive li a:hover,
            .footer-style-08 .widget_categories li a:hover,
            .footer-style-08 .widget_meta li a:hover,
            .footer-style-08 .widget_product_categories li a:hover,
            .footer-style-08 .widget_rss li a:hover,
            .footer-style-08 .widget_pages li a:hover,
            .footer-style-08 .widget_nav_menu li a:hover,
            .footer-style-08 .insight-core-bmw li a:hover 
			',
			'property' => 'color',
		),
	),
) );
