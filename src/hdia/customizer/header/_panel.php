<?php
$panel    = 'header';
$priority = 1;

Hdia_Kirki::add_section( 'header', array(
	'title'    => esc_html__( 'General', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_sticky', array(
	'title'    => esc_html__( 'Header Sticky', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_more_options', array(
	'title'    => esc_html__( 'Header More Options', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_01', array(
	'title'    => esc_html__( 'Header Style 01', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_02', array(
	'title'    => esc_html__( 'Header Style 02', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_03', array(
	'title'    => esc_html__( 'Header Style 03', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_04', array(
	'title'    => esc_html__( 'Header Style 04', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_05', array(
	'title'    => esc_html__( 'Header Style 05', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_06', array(
	'title'    => esc_html__( 'Header Style 06', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_07', array(
	'title'    => esc_html__( 'Header Style 07', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_08', array(
	'title'    => esc_html__( 'Header Style 08', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_09', array(
	'title'    => esc_html__( 'Header Style 09', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_10', array(
	'title'    => esc_html__( 'Header Style 10', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_11', array(
	'title'    => esc_html__( 'Header Style 11', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_12', array(
	'title'    => esc_html__( 'Header Style 12', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_13', array(
	'title'    => esc_html__( 'Header Style 13', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_14', array(
	'title'    => esc_html__( 'Header Style 14', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_15', array(
	'title'    => esc_html__( 'Header Style 15', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_16', array(
	'title'    => esc_html__( 'Header Style 16', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_17', array(
	'title'    => esc_html__( 'Header Style 17', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_18', array(
	'title'    => esc_html__( 'Header Style 18', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_19', array(
	'title'    => esc_html__( 'Header Style 19', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_20', array(
	'title'    => esc_html__( 'Header Style 20', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_21', array(
	'title'    => esc_html__( 'Header Style 21', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_22', array(
	'title'    => esc_html__( 'Header Style 22', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_23', array(
	'title'    => esc_html__( 'Header Style 23', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_24', array(
	'title'    => esc_html__( 'Header Style 24', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_25', array(
	'title'    => esc_html__( 'Header Style 25', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_26', array(
	'title'    => esc_html__( 'Header Style 26', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_27', array(
	'title'    => esc_html__( 'Header Style 27', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'header_style_28', array(
	'title'    => esc_html__( 'Header Style 28', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
