<?php
$section  = 'header';
$priority = 1;
$prefix   = 'header_';

$headers = Hdia_Helper::get_header_list( true );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'global_header',
	'label'       => esc_html__( 'Default Header', 'hdia' ),
	'description' => esc_html__( 'Select default header type for your site.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '04',
	'choices'     => Hdia_Helper::get_header_list(),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_page_header_type',
	'label'       => esc_html__( 'Single Page', 'hdia' ),
	'description' => esc_html__( 'Select default header type that displays on all single pages.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $headers,
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_post_header_type',
	'label'       => esc_html__( 'Single Blog', 'hdia' ),
	'description' => esc_html__( 'Select default header type that displays on all single blog post pages.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $headers,
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_product_header_type',
	'label'       => esc_html__( 'Single Product', 'hdia' ),
	'description' => esc_html__( 'Select default header type that displays on all single product pages.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $headers,
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_service_header_type',
	'label'       => esc_html__( 'Single Service', 'hdia' ),
	'description' => esc_html__( 'Select default header type that displays on all single service pages.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '22',
	'choices'     => $headers,
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_project_header_type',
	'label'       => esc_html__( 'Single Project', 'hdia' ),
	'description' => esc_html__( 'Select default header type that displays on all single project pages.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $headers,
) );
