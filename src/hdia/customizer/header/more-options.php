<?php
$section  = 'header_more_options';
$priority = 1;
$prefix   = 'header_more_options_';

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'background',
	'settings'    => $prefix . 'background',
	'label'       => esc_html__( 'Background', 'hdia' ),
	'description' => esc_html__( 'Controls the background of header more options.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'background-color'      => '#ffffff',
		'background-image'      => '',
		'background-repeat'     => 'no-repeat',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
		'background-position'   => 'center center',
	),
	'output'      => array(
		array(
			'element' => '.header-more-tools-opened .header-right-inner',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'multicolor',
	'settings'    => $prefix . 'icon_color',
	'label'       => esc_html__( 'Icon Color', 'hdia' ),
	'description' => esc_html__( 'Controls the color of icons on header.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'choices'     => array(
		'normal' => esc_attr__( 'Normal', 'hdia' ),
		'hover'  => esc_attr__( 'Hover', 'hdia' ),
	),
	'default'     => array(
		'normal' => Hdia::HEADING_COLOR,
		'hover'  => Hdia::PRIMARY_COLOR,
	),
	'output'      => array(
		array(
			'choice'   => 'normal',
			'element'  => '
			.header-more-tools-opened .header-right-inner .header-icon,
			.header-more-tools-opened .header-right-inner .page-open-popup-search i,
			.header-more-tools-opened .header-right-inner .switcher-language-wrapper > a
			',
			'property' => 'color',
			'suffix'   => '!important',
		),
		array(
			'choice'   => 'hover',
			'element'  => '
			.header-more-tools-opened .header-right-inner .header-icon:hover
			',
			'property' => 'color',
			'suffix'   => '!important',
		),
		array(
			'choice'   => 'hover',
			'element'  => '.header-more-tools-opened .header-right-inner .wpml-ls-slot-shortcode_actions:hover > .js-wpml-ls-item-toggle',
			'property' => 'color',
			'suffix'   => '!important',
		),
	),
) );
