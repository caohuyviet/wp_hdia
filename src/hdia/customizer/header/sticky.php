<?php
$section  = 'header_sticky';
$priority = 1;
$prefix   = 'header_sticky_';

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'toggle',
	'settings'    => $prefix . 'enable',
	'label'       => esc_html__( 'Enable', 'hdia' ),
	'description' => esc_html__( 'Enable this option to turn on header sticky feature.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 1,
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'behaviour',
	'label'       => esc_html__( 'Behaviour', 'hdia' ),
	'description' => esc_html__( 'Controls the behaviour of header sticky when you scroll down to page', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'both',
	'choices'     => array(
		'both' => esc_html__( 'Sticky on scroll up/down', 'hdia' ),
		'up'   => esc_html__( 'Sticky on scroll up', 'hdia' ),
		'down' => esc_html__( 'Sticky on scroll down', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'padding_top',
	'label'     => esc_html__( 'Padding top', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 0,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 200,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.headroom--not-top .page-header-inner',
			'property' => 'padding-top',
			'units'    => 'px',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'padding_bottom',
	'label'     => esc_html__( 'Padding bottom', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 0,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 200,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.headroom--not-top .page-header-inner',
			'property' => 'padding-bottom',
			'units'    => 'px',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'spacing',
	'settings'    => $prefix . 'item_padding',
	'label'       => esc_html__( 'Item Padding', 'hdia' ),
	'description' => esc_html__( 'Controls the navigation item level 1 padding of navigation when sticky.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'top'    => '6px',
		'bottom' => '6px',
		'left'   => '14px',
		'right'  => '14px',
	),
	'transport'   => 'auto',
	'output'      => array(
		array(
			'element'  => array(
				'.desktop-menu .headroom--not-top:not(.header-15) .menu--primary .menu__container > li > a',
				'.desktop-menu .headroom--not-top:not(.header-15) .menu--primary .menu__container > ul > li > a',
			),
			'property' => 'padding',
		),
	),
) );
