<?php
$section  = 'header_style_02';
$priority = 1;
$prefix   = 'header_style_02_';

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'overlay',
	'label'    => esc_html__( 'Header Overlay', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '0',
	'choices'  => array(
		'0' => esc_html__( 'No', 'hdia' ),
		'1' => esc_html__( 'Yes', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'logo',
	'label'    => esc_html__( 'Logo', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'dark',
	'choices'  => array(
		'light' => esc_html__( 'Light', 'hdia' ),
		'dark'  => esc_html__( 'Dark', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'sticky_logo',
	'label'    => esc_html__( 'Sticky Logo', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'sticky-dark',
	'choices'  => array(
		'sticky-light' => esc_html__( 'Light', 'hdia' ),
		'sticky-dark'  => esc_html__( 'Dark', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'search_enable',
	'label'    => esc_html__( 'Search', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'Hide', 'hdia' ),
		'1' => esc_html__( 'Show', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'cart_enable',
	'label'    => esc_html__( 'Mini Cart', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'Hide', 'hdia' ),
		'1' => esc_html__( 'Show', 'hdia' )
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'cart_enable',
	'label'    => esc_html__( 'Mini Cart', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '0',
	'choices'  => array(
		'0' => esc_html__( 'Hide', 'hdia' ),
		'1' => esc_html__( 'Show', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'info_icon',
	'label'    => esc_html__( 'Info Icon Class', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'icomoon-phone-call',
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'info_text',
	'label'    => esc_html__( 'Info Text', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => esc_html__( 'Hotline', 'hdia' ),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'info_sub_text',
	'label'    => esc_html__( 'Info Sub Text', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => esc_html__( '02203866699', 'hdia' ),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'info_link',
	'label'    => esc_html__( 'Info Link', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'tel:02203866699',
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'info_icon_color',
	'label'       => esc_html__( 'Info Icon Color', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Hdia::PRIMARY_COLOR,
	'output'      => array(
		array(
			'element'  => '.header-02 .info-icon',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'info_text_color',
	'label'       => esc_html__( 'Info Text Color', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Hdia::HEADING_COLOR,
	'output'      => array(
		array(
			'element'  => '.header-02 .info-text',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'info_sub_text_color',
	'label'       => esc_html__( 'Info Subtext Color', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Hdia::SECONDARY_COLOR,
	'output'      => array(
		array(
			'element'  => '.header-02 .info-sub-text a',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'border_width',
	'label'     => esc_html__( 'Border Bottom Width', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 1,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 50,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.header-02 .page-header-inner',
			'property' => 'border-bottom-width',
			'units'    => 'px',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'border_color',
	'label'       => esc_html__( 'Border Color', 'hdia' ),
	'description' => esc_html__( 'Controls the border color.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#eee',
	'output'      => array(
		array(
			'element'  => '.header-02 .page-header-inner',
			'property' => 'border-bottom-color',
		),
		array(
			'element'  => '.header-02 .branding',
			'property' => 'border-right-color',
		),
		array(
			'element'  => '.header-02 .header-right-info',
			'property' => 'border-left-color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'box_shadow',
	'label'       => esc_html__( 'Box Shadow', 'hdia' ),
	'description' => esc_html__( 'Input box shadow for header, e.g 0 0 5px #ccc', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'output'      => array(
		array(
			'element'  => '.header-02 .page-header-inner',
			'property' => 'box-shadow',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'background',
	'settings'    => $prefix . 'background',
	'label'       => esc_html__( 'Background', 'hdia' ),
	'description' => esc_html__( 'Controls the background of header.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'background-color'      => '#fff',
		'background-image'      => '',
		'background-repeat'     => 'no-repeat',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
		'background-position'   => 'center center',
	),
	'output'      => array(
		array(
			'element' => '.header-02 .page-header-inner',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'header_social_icon_color',
	'label'       => esc_html__( 'Social Icon Color', 'hdia' ),
	'description' => esc_html__( 'Controls the color of social icons on header.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#999',
	'output'      => array(
		array(
			'element'  => '
			.header-02 .header-social-networks a',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'header_social_icon_hover_color',
	'label'       => esc_html__( 'Social Icon Hover Color', 'hdia' ),
	'description' => esc_html__( 'Controls the color of social icons hover on header.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Hdia::PRIMARY_COLOR,
	'output'      => array(
		array(
			'element'  => '
			.header-02 .header-social-networks a:hover',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'header_icon_color',
	'label'       => esc_html__( 'Icon Color', 'hdia' ),
	'description' => esc_html__( 'Controls the color of icons on header.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Hdia::HEADING_COLOR,
	'output'      => array(
		array(
			'element'  => '
			.header-02 .page-open-mobile-menu i,
			.header-02 .popup-search-wrap i',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'header_icon_hover_color',
	'label'       => esc_html__( 'Icon Hover Color', 'hdia' ),
	'description' => esc_html__( 'Controls the color when hover of icons on header.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Hdia::PRIMARY_COLOR,
	'output'      => array(
		array(
			'element'  => '
			.header-02 .page-open-mobile-menu:hover i,
			.header-02 .popup-search-wrap:hover i,
			.header-02 .mini-cart .mini-cart-icon:hover
			',
			'property' => 'color',
		),
	),
) );


Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'cart_badge_background_color',
	'label'       => esc_html__( 'Cart Badge Background Color', 'hdia' ),
	'description' => esc_html__( 'Controls the background color of cart badge.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Hdia::PRIMARY_COLOR,
	'output'      => array(
		array(
			'element'  => '.header-02 .mini-cart .mini-cart-icon:after',
			'property' => 'background-color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'cart_badge_color',
	'label'       => esc_html__( 'Cart Badge Color', 'hdia' ),
	'description' => esc_html__( 'Controls the color of cart badge.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#222',
	'output'      => array(
		array(
			'element'  => '.header-02 .mini-cart .mini-cart-icon:after',
			'property' => 'color',
		),
	),
) );

/*--------------------------------------------------------------
# Navigation
--------------------------------------------------------------*/

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Main Menu Level 1', 'hdia' ) . '</div>',
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'spacing',
	'settings'  => $prefix . 'navigation_item_padding',
	'label'     => esc_html__( 'Item Padding', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => array(
		'top'    => '6px',
		'bottom' => '6px',
		'left'   => '14px',
		'right'  => '14px',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => array(
				'.desktop-menu .header-02 .menu--primary .menu__container > li > a',
			),
			'property' => 'padding',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'spacing',
	'settings'  => $prefix . 'navigation_item_margin',
	'label'     => esc_html__( 'Item Margin', 'hdia' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => array(
		'top'    => '0px',
		'bottom' => '0px',
		'left'   => '0px',
		'right'  => '0px',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => array(
				'.desktop-menu .header-02  .menu--primary .menu__container > li',
			),
			'property' => 'margin',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'kirki_typography',
	'settings'    => $prefix . 'navigation_typography',
	'label'       => esc_html__( 'Typography', 'hdia' ),
	'description' => esc_html__( 'These settings control the typography for menu items.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => '',
		'variant'        => '600',
		'line-height'    => '1.26',
		'letter-spacing' => '0em',
		'text-transform' => 'uppercase',
	),
	'output'      => array(
		array(
			'element' => '.header-02 .menu--primary a',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => $prefix . 'navigation_item_font_size',
	'label'       => esc_html__( 'Font Size', 'hdia' ),
	'description' => esc_html__( 'Controls the font size for main menu items.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 15,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'  => '.header-02 .menu--primary a',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_color',
	'label'       => esc_html__( 'Color', 'hdia' ),
	'description' => esc_html__( 'Controls the color for main menu items.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Hdia::HEADING_COLOR,
	'output'      => array(
		array(
			'element'  => '
			.header-02 .menu--primary a
			',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_hover_color',
	'label'       => esc_html__( 'Hover Color', 'hdia' ),
	'description' => esc_html__( 'Controls the color when hover for main menu items.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Hdia::PRIMARY_COLOR,
	'output'      => array(
		array(
			'element'  => '
            .header-02 .menu--primary li:hover > a,
            .header-02 .menu--primary > ul > li > a:hover,
            .header-02 .menu--primary > ul > li > a:focus,
            .header-02 .menu--primary .current-menu-ancestor > a,
            .header-02 .menu--primary .current-menu-item > a',
			'property' => 'color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_background_color',
	'label'       => esc_html__( 'Background Color', 'hdia' ),
	'description' => esc_html__( 'Controls the background color for main menu items.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '',
	'output'      => array(
		array(
			'element'  => '.header-02 .menu--primary .menu__container > li > a',
			'property' => 'background-color',
		),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_hover_background_color',
	'label'       => esc_html__( 'Hover Background Color', 'hdia' ),
	'description' => esc_html__( 'Controls the background color when hover for main menu items.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '',
	'output'      => array(
		array(
			'element'  => '
            .header-02 .menu--primary .menu__container > li > a:hover,
            .header-02 .menu--primary .menu__container > li.current-menu-item > a',
			'property' => 'background-color',
		),
	),
) );
