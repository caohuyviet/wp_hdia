<?php
$panel    = 'navigation';
$priority = 1;

Hdia_Kirki::add_section( 'navigation', array(
	'title'    => esc_html__( 'Desktop Menu', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'navigation_minimal', array(
	'title'    => esc_html__( 'Off Canvas Menu', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'navigation_mobile', array(
	'title'    => esc_html__( 'Mobile Menu', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
