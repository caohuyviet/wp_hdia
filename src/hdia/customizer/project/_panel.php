<?php
$panel    = 'project';
$priority = 1;

Hdia_Kirki::add_section( 'archive_project', array(
	'title'    => esc_html__( 'Project Archive', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'single_project', array(
	'title'    => esc_html__( 'Project Single', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
