<?php
$section  = 'single_project';
$priority = 1;
$prefix   = 'single_project_';

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'comment_enable',
	'label'       => esc_html__( 'Comments', 'hdia' ),
	'description' => esc_html__( 'Turn on to display comments on single project posts.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );
