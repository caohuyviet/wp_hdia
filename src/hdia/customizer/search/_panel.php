<?php
$panel    = 'search';
$priority = 1;

Hdia_Kirki::add_section( 'search_page', array(
	'title'    => esc_html__( 'Search Page', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'search_popup', array(
	'title'    => esc_html__( 'Search Popup', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
