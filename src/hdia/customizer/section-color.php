<?php
$section  = 'color_';
$priority = 1;
$prefix   = 'color_';

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => 'primary_color',
	'label'       => esc_html__( 'Primary Color', 'hdia' ),
	'description' => esc_html__( 'A light color.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Hdia::PRIMARY_COLOR,
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => 'secondary_color',
	'label'       => esc_html__( 'Secondary Color', 'hdia' ),
	'description' => esc_html__( 'A dark color.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Hdia::SECONDARY_COLOR,
) );
