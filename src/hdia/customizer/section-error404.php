<?php
$section  = 'error404_page';
$priority = 1;
$prefix   = 'error404_page_';

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'logo',
	'label'    => esc_html__( 'Logo', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'light',
	'choices'  => array(
		'light' => esc_html__( 'Light', 'hdia' ),
		'dark'  => esc_html__( 'Dark', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => 'error404_page_title',
	'label'       => esc_html__( 'Title', 'hdia' ),
	'description' => esc_html__( 'Controls the title that display on error 404 page.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'OOPS! That page can be not found!', 'hdia' ),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => 'error404_sub_title',
	'label'       => esc_html__( 'Sub Title', 'hdia' ),
	'description' => esc_html__( 'Controls the sub title that display on error 404 page.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Leverage agile frameworks to provide a robust synopsis for high level.', 'hdia' ),
) );
