<?php
$section  = 'social_sharing';
$priority = 1;
$prefix   = 'social_sharing_';

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'multicheck',
	'settings'    => $prefix . 'items',
	'label'       => esc_attr__( 'Sharing Links', 'hdia' ),
	'description' => esc_html__( 'Check to the box to enable social share links.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array( 'facebook', 'twitter', 'linkedin', 'email' ),
	'choices'     => array(
		'facebook'    => esc_attr__( 'Facebook', 'hdia' ),
		'twitter'     => esc_attr__( 'Twitter', 'hdia' ),
		'linkedin'    => esc_attr__( 'Linkedin', 'hdia' ),
		'google_plus' => esc_attr__( 'Google+', 'hdia' ),
		'tumblr'      => esc_attr__( 'Tumblr', 'hdia' ),
		'email'       => esc_attr__( 'Email', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'sortable',
	'settings'    => $prefix . 'order',
	'label'       => esc_attr__( 'Order', 'hdia' ),
	'description' => esc_html__( 'Controls the order of social share links.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'facebook',
		'twitter',
		'google_plus',
		'tumblr',
		'linkedin',
		'email',
	),
	'choices'     => array(
		'facebook'    => esc_attr__( 'Facebook', 'hdia' ),
		'twitter'     => esc_attr__( 'Twitter', 'hdia' ),
		'google_plus' => esc_attr__( 'Google+', 'hdia' ),
		'tumblr'      => esc_attr__( 'Tumblr', 'hdia' ),
		'linkedin'    => esc_attr__( 'Linkedin', 'hdia' ),
		'email'       => esc_attr__( 'Email', 'hdia' ),
	),
) );
