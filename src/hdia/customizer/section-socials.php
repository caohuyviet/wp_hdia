<?php
$section  = 'socials';
$priority = 1;
$prefix   = 'social_';

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'social_link_target',
	'label'    => esc_html__( 'Open link in a new tab.', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'No', 'hdia' ),
		'1' => esc_html__( 'Yes', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'      => 'repeater',
	'settings'  => 'social_link',
	'section'   => $section,
	'priority'  => $priority ++,
	'choices'   => array(
		'labels' => array(
			'add-new-row' => esc_html__( 'Add new social network', 'hdia' ),
		),
	),
	'row_label' => array(
		'type'  => 'field',
		'field' => 'tooltip',
	),
	'default'   => array(
		array(
			'tooltip'    => esc_html__( 'Facebook', 'hdia' ),
			'icon_class' => 'fab fa-facebook-f',
			'link_url'   => 'https://www.facebook.com/C%C3%B4ng-ty-c%E1%BB%95-ph%E1%BA%A7n-t%E1%BB%B1-%C4%91%E1%BB%99ng-h%C3%B3a-c%C3%B4ng-nghi%E1%BB%87p-H%E1%BA%A3i-D%C6%B0%C6%A1ng-145557085862099',
		),
		array(
			'tooltip'    => esc_html__( 'Twitter', 'hdia' ),
			'icon_class' => 'fab fa-twitter',
			'link_url'   => 'https://twitter.com',
		),
		array(
			'tooltip'    => esc_html__( 'Instagram', 'hdia' ),
			'icon_class' => 'fab fa-instagram',
			'link_url'   => 'https://instagram.com',
		),
		array(
			'tooltip'    => esc_html__( 'Youtube', 'hdia' ),
			'icon_class' => 'fab fa-youtube',
			'link_url'   => 'https://www.youtube.com',
		),
	),
	'fields'    => array(
		'tooltip'    => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Tooltip', 'hdia' ),
			'description' => esc_html__( 'Enter your hint text for your icon', 'hdia' ),
			'default'     => '',
		),
		'icon_class' => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Icon Class', 'hdia' ),
			'description' => esc_html__( 'This will be the icon class for your link', 'hdia' ),
			'default'     => '',
		),
		'link_url'   => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Link URL', 'hdia' ),
			'description' => esc_html__( 'This will be the link URL', 'hdia' ),
			'default'     => '',
		),
	),
) );
