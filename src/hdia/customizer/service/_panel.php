<?php
$panel    = 'service';
$priority = 1;

Hdia_Kirki::add_section( 'archive_service', array(
	'title'    => esc_html__( 'Service Archive', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'single_service', array(
	'title'    => esc_html__( 'Service Single', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
