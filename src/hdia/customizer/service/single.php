<?php
$section  = 'single_service';
$priority = 1;
$prefix   = 'single_service_';

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => 'single_service_style',
	'label'    => esc_html__( 'Style', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '02',
	'choices'  => array(
		'01' => esc_attr__( 'Style 01', 'hdia' ),
		'02' => esc_attr__( 'Style 02', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'comment_enable',
	'label'       => esc_html__( 'Comments', 'hdia' ),
	'description' => esc_html__( 'Turn on to display comments on single project posts.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );
