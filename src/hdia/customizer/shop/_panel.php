<?php
$panel    = 'shop';
$priority = 1;

Hdia_Kirki::add_section( 'shop_archive', array(
	'title'    => esc_html__( 'Shop Archive', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'shop_single', array(
	'title'    => esc_html__( 'Shop Single', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'shopping_cart', array(
	'title'    => esc_html__( 'Shopping Cart', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
