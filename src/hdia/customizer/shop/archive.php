<?php
$section  = 'shop_archive';
$priority = 1;
$prefix   = 'shop_archive_';

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'shop_archive_new_days',
	'label'       => esc_html__( 'New Badge (Days)', 'hdia' ),
	'description' => esc_html__( 'If the product was published within the newness time frame display the new badge.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '90',
	'choices'     => array(
		'0'  => esc_html__( 'None', 'hdia' ),
		'1'  => esc_html__( '1 day', 'hdia' ),
		'2'  => esc_html__( '2 days', 'hdia' ),
		'3'  => esc_html__( '3 days', 'hdia' ),
		'4'  => esc_html__( '4 days', 'hdia' ),
		'5'  => esc_html__( '5 days', 'hdia' ),
		'6'  => esc_html__( '6 days', 'hdia' ),
		'7'  => esc_html__( '7 days', 'hdia' ),
		'8'  => esc_html__( '8 days', 'hdia' ),
		'9'  => esc_html__( '9 days', 'hdia' ),
		'10' => esc_html__( '10 days', 'hdia' ),
		'15' => esc_html__( '15 days', 'hdia' ),
		'20' => esc_html__( '20 days', 'hdia' ),
		'25' => esc_html__( '25 days', 'hdia' ),
		'30' => esc_html__( '30 days', 'hdia' ),
		'60' => esc_html__( '60 days', 'hdia' ),
		'90' => esc_html__( '90 days', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'shop_archive_quick_view',
	'label'       => esc_html__( 'Quick view', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the quick view button', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'shop_archive_compare',
	'label'       => esc_html__( 'Compare', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the compare button', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'shop_archive_wishlist',
	'label'       => esc_html__( 'Wishlist', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the wishlist button', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );
