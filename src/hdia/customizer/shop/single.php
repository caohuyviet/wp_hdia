<?php
$section  = 'shop_single';
$priority = 1;
$prefix   = 'single_product_';

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_categories_enable',
	'label'       => esc_html__( 'Categories', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the categories.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_tags_enable',
	'label'       => esc_html__( 'Tags', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the tags.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_sharing_enable',
	'label'       => esc_html__( 'Sharing', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the sharing.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_up_sells_enable',
	'label'       => esc_html__( 'Up-sells products', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the up-sells products section.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_related_enable',
	'label'       => esc_html__( 'Related products', 'hdia' ),
	'description' => esc_html__( 'Turn on to display the related products section.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'hdia' ),
		'1' => esc_html__( 'On', 'hdia' ),
	),
) );
