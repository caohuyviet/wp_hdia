<?php
$panel    = 'shortcode';
$priority = 1;

Hdia_Kirki::add_section( 'shortcode_animation', array(
	'title'    => esc_html__( 'Animation', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
