<?php
$section  = 'shortcode_animation';
$priority = 1;
$prefix   = 'shortcode_animation_';

Hdia_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'enable',
	'label'       => esc_html__( 'Mobile Animation', 'hdia' ),
	'description' => esc_html__( 'Controls the animations on mobile & tablet.', 'hdia' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'desktop',
	'choices'     => array(
		'none'    => esc_html__( 'None', 'hdia' ),
		'mobile'  => esc_html__( 'Only Mobile', 'hdia' ),
		'desktop' => esc_html__( 'Only Desktop', 'hdia' ),
		'both'    => esc_html__( 'Both', 'hdia' ),
	),
) );
