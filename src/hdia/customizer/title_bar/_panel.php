<?php
$panel    = 'title_bar';
$priority = 1;

Hdia_Kirki::add_section( 'title_bar', array(
	'title'    => esc_html__( 'General', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'title_bar_01', array(
	'title'    => esc_html__( 'Style 01', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'title_bar_02', array(
	'title'    => esc_html__( 'Style 02', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'title_bar_03', array(
	'title'    => esc_html__( 'Style 03', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'title_bar_04', array(
	'title'    => esc_html__( 'Style 04', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'title_bar_05', array(
	'title'    => esc_html__( 'Style 05', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'title_bar_06', array(
	'title'    => esc_html__( 'Style 06', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
