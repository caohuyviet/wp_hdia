<?php
$panel    = 'top_bar';
$priority = 1;

Hdia_Kirki::add_section( 'top_bar', array(
	'title'    => esc_html__( 'General', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'top_bar_style_01', array(
	'title'    => esc_html__( 'Top Bar Style 01', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'top_bar_style_02', array(
	'title'    => esc_html__( 'Top Bar Style 02', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'top_bar_style_03', array(
	'title'    => esc_html__( 'Top Bar Style 03', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'top_bar_style_04', array(
	'title'    => esc_html__( 'Top Bar Style 04', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'top_bar_style_05', array(
	'title'    => esc_html__( 'Top Bar Style 05', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'top_bar_style_06', array(
	'title'    => esc_html__( 'Top Bar Style 06', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'top_bar_style_07', array(
	'title'    => esc_html__( 'Top Bar Style 07', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'top_bar_style_08', array(
	'title'    => esc_html__( 'Top Bar Style 08', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'top_bar_style_09', array(
	'title'    => esc_html__( 'Top Bar Style 09', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Hdia_Kirki::add_section( 'top_bar_style_10', array(
	'title'    => esc_html__( 'Top Bar Style 10', 'hdia' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
