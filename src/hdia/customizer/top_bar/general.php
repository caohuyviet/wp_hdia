<?php
$section  = 'top_bar';
$priority = 1;
$prefix   = 'top_bar_';

Hdia_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => 'global_top_bar',
	'label'    => esc_html__( 'Default Top Bar', 'hdia' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '01',
	'choices'  => Hdia_Helper::get_top_bar_list(),
) );
