<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Do nothing if not an admin page
if ( ! is_admin() ) {
	return;
}

/**
 * Hook & filter that run only on admin pages.
 */
if ( ! class_exists( 'Hdia_Admin' ) ) {
	class Hdia_Admin {
		protected static $instance = null;

		public function __construct() {
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		}

		public static function instance() {
			if ( null === self::$instance ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Enqueue scrips & styles.
		 *
		 * @access public
		 */
		public function enqueue_scripts() {
			wp_enqueue_style( 'hdia-admin', HDIA_THEME_URI . '/assets/admin/css/style.css' );
			wp_enqueue_script( 'hdia-admin', HDIA_THEME_URI . '/assets/admin/js/admin.js', array( 'jquery' ), null, true );
		}
	}

	new Hdia_Admin();
}
