<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue scripts and styles.
 */
if ( ! class_exists( 'Hdia_Enqueue' ) ) {
	class Hdia_Enqueue {

		protected static $instance = null;

		public function __construct() {
			add_action( 'enqueue_block_editor_assets', array( $this, 'gutenberg' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ) );

			/*// Disable all contact form 7 scripts.
			add_filter( 'wpcf7_load_js', '__return_false' );
			add_filter( 'wpcf7_load_css', '__return_false' );

			// Re queue contact form 7 scripts when used.
			add_action( 'wp_enqueue_scripts', array( $this, 'requeue_wpcf7_scripts' ), 99 );*/
		}

		function gutenberg() {
			wp_enqueue_style( 'gutenberg-google-font', 'https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i', null, null );
			wp_enqueue_style( 'gutenberg-editor', HDIA_THEME_URI . '/gutenberg-style.css', null, null );
		}


		function requeue_wpcf7_scripts() {
			global $post;
			if ( is_a( $post, 'WP_Post' ) &&
			     ( has_shortcode( $post->post_content, 'contact-form-7' ) ||
			       has_shortcode( $post->post_content, 'tm_contact_form_7' ) ||
			       has_shortcode( $post->post_content, 'tm_contact_form_7_box' )
			     )
			) {
				if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
					wpcf7_enqueue_scripts();
				}

				if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
					wpcf7_enqueue_styles();
				}
			}
		}

		public static function instance() {
			if ( null === self::$instance ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Enqueue scripts & styles.
		 *
		 * @access public
		 */
		public function enqueue() {
			$post_type = get_post_type();

			// Remove font awesome from Yith Wishlist plugin.
			wp_dequeue_style( 'yith-wcwl-font-awesome' );

			// Remove font awesome from Visual Composer plugin.
			wp_deregister_style( 'font-awesome' );
			wp_dequeue_style( 'font-awesome' );

			wp_register_style( 'font-awesome', HDIA_THEME_URI . '/assets/fonts/awesome/css/fontawesome-all.min.css', null, '5.13.1' );

			wp_enqueue_style( 'font-flat', HDIA_THEME_URI . '/assets/fonts/flat/font-flat.css', null, null );
			wp_enqueue_style( 'font-icomoon', HDIA_THEME_URI . '/assets/fonts/icomoon/font-icomoon.css', null, null );
			wp_register_style( 'font-ion', HDIA_THEME_URI . '/assets/fonts/ion/font-ion.min.css', null, null );

			wp_register_style( 'spinkit', HDIA_THEME_URI . '/assets/libs/spinkit/spinkit.css', null, null );

			wp_register_style( 'justified-gallery', HDIA_THEME_URI . '/assets/libs/justified-gallery/justifiedGallery.min.css', null, '3.6.3' );
			wp_register_script( 'justified-gallery', HDIA_THEME_URI . '/assets/libs/justified-gallery/jquery.justifiedGallery.min.js', array( 'jquery' ), '3.6.3', true );

			wp_register_script( 'picturefill', HDIA_THEME_URI . '/assets/libs/picturefill/picturefill.min.js', array( 'jquery' ), null, true );
			wp_register_script( 'mousewheel', HDIA_THEME_URI . "/assets/libs/mousewheel/jquery.mousewheel.js", array( 'jquery' ), HDIA_THEME_VERSION, true );
			wp_enqueue_style( 'lightgallery', HDIA_THEME_URI . '/assets/libs/light-gallery/css/lightgallery.min.css', null, '1.6.4' );
			wp_enqueue_script( 'lightgallery', HDIA_THEME_URI . "/assets/libs/light-gallery/js/lightgallery-all.js", array(
				'jquery',
				'picturefill',
				'mousewheel',
			), null, true );

			wp_register_style( 'lightslider', HDIA_THEME_URI . '/assets/libs/light-slider/css/lightslider.min.css', null, '1.1.6' );
			wp_register_script( 'lightslider', HDIA_THEME_URI . "/assets/js/lightslider.js", array( 'jquery' ), '1.1.6', true );

			wp_register_style( 'swiper', HDIA_THEME_URI . '/assets/libs/swiper/css/swiper.min.css', null, '4.0.3' );
			wp_register_script( 'swiper', HDIA_THEME_URI . "/assets/libs/swiper/js/swiper.js", array( 'jquery' ), '4.0.3', true );

			// Fix VC waypoints.
			if ( ! wp_script_is( 'vc_waypoints', 'registered' ) ) {
				wp_register_script( 'vc_waypoints', HDIA_THEME_URI . '/assets/libs/vc-waypoints/vc-waypoints.min.js', array( 'jquery' ), null, true );
			}

			// Register scripts
			wp_register_script( 'slimscroll', HDIA_THEME_URI . '/assets/libs/slimscroll/jquery.slimscroll.min.js', array( 'jquery' ), '1.3.8', true );
			wp_register_script( 'easing', HDIA_THEME_URI . '/assets/libs/easing/jquery.easing.min.js', array( 'jquery' ), '1.3', true );
			wp_register_script( 'matchheight', HDIA_THEME_URI . '/assets/libs/matchheight/jquery.matchheight-min.js', array( 'jquery' ), HDIA_THEME_VERSION, true );
			wp_register_script( 'gmap3', HDIA_THEME_URI . '/assets/libs/gmap3/gmap3.min.js', array( 'jquery' ), HDIA_THEME_VERSION, true );
			wp_register_script( 'countdown', HDIA_THEME_URI . '/assets/libs/jquery.countdown/js/jquery.countdown.min.js', array( 'jquery' ), HDIA_THEME_VERSION, true );
			wp_register_script( 'typed', HDIA_THEME_URI . '/assets/libs/typed/typed.min.js', array( 'jquery' ), null, true );

			// Fix WordPress old version not registered this script
			if ( ! wp_script_is( 'imagesloaded', 'registered' ) ) {
				wp_register_script( 'imagesloaded', HDIA_THEME_URI . '/assets/libs/imagesloaded/imagesloaded.min.js', array( 'jquery' ), null, true );
			}

			wp_register_script( 'isotope-masonry', HDIA_THEME_URI . '/assets/libs/isotope/js/isotope.pkgd.min.js', array( 'jquery' ), HDIA_THEME_VERSION, true );
			wp_register_script( 'isotope-packery', HDIA_THEME_URI . '/assets/libs/packery-mode/packery-mode.pkgd.min.js', array(
				'jquery',
				'imagesloaded',
				'isotope-masonry',
			), HDIA_THEME_VERSION, true );

			wp_register_script( 'smooth-scroll', HDIA_THEME_URI . '/assets/libs/smooth-scroll-for-web/SmoothScroll.min.js', array(
				'jquery',
			), '1.4.6', true );

			wp_register_script( 'smooth-scroll-for-web', HDIA_THEME_URI . "/assets/libs/smooth-scroll-for-web/SmoothScroll.min.js", array(), HDIA_THEME_VERSION, true );

			wp_register_script( 'lazyload', HDIA_THEME_URI . "/assets/libs/lazyload/lazyload.js", array(), HDIA_THEME_VERSION, true );

			wp_register_script( 'odometer', HDIA_THEME_URI . '/assets/libs/odometer/odometer.min.js', array(
				'jquery',
			), HDIA_THEME_VERSION, true );

			wp_register_script( 'counter-up', HDIA_THEME_URI . '/assets/libs/countTo/jquery.countTo.js', array(
				'jquery',
			), HDIA_THEME_VERSION, true );

			// Counter JS by Hdia theme
			wp_register_script( 'hdia-counter', HDIA_THEME_URI . "/assets/js/counter.js", array(
				'jquery',
			), HDIA_THEME_VERSION, true );

			wp_register_script( 'chart', HDIA_THEME_URI . '/assets/libs/chart/chart.min.js', array(
				'jquery',
			), HDIA_THEME_VERSION, true );

			// Chart JS by Hdia theme
			wp_register_script( 'hdia-chart', HDIA_THEME_URI . "/assets/js/chart.js", array(
				'jquery',
				'vc_waypoints',
				'chart',
			), HDIA_THEME_VERSION, true );

			wp_register_script( 'circle-progress', HDIA_THEME_URI . '/assets/libs/circle-progress/circle-progress.min.js', array( 'jquery' ), null, true );

			// Circle Progress JS by Hdia theme
			wp_register_script( 'hdia-circle-progress', HDIA_THEME_URI . "/assets/js/circle-progress.js", array(
				'jquery',
				'vc_waypoints',
				'circle-progress',
			), null, true );

			wp_register_script( 'hdia-pricing', HDIA_THEME_URI . "/assets/js/pricing.js", array(
				'jquery',
				'matchheight',
			), null, true );

			// Accordion JS by Hdia theme
			wp_register_script( 'hdia-accordion', HDIA_THEME_URI . "/assets/js/accordion.js", array( 'jquery' ), HDIA_THEME_VERSION, true );

			// Enqueue the theme's style.css
			wp_enqueue_style( 'hdia-style', get_stylesheet_uri() );
			wp_enqueue_style( 'font-ion' );
			wp_enqueue_style( 'font-awesome' );
			wp_enqueue_style( 'swiper' );
			wp_enqueue_style( 'spinkit' );

			// Register scripts
			if ( Hdia::setting( 'header_sticky_enable' ) ) {
				wp_enqueue_script( 'headroom', HDIA_THEME_URI . "/assets/js/headroom.js", array( 'jquery' ), HDIA_THEME_VERSION, true );
			}

			if ( Hdia::setting( 'smooth_scroll_enable' ) ) {
				// Smooth scroll bar
				wp_enqueue_script( 'smooth-scroll' );
			}

			// Smooth scroll link
			wp_enqueue_script( 'jquery-smooth-scroll', HDIA_THEME_URI . '/assets/libs/smooth-scroll/jquery.smooth-scroll.min.js', array( 'jquery' ), HDIA_THEME_VERSION, true );

			wp_enqueue_script( 'swiper' );
			wp_enqueue_script( 'matchheight' );
			wp_enqueue_script( 'vc_waypoints' );
			wp_enqueue_script( 'smartmenus', HDIA_THEME_URI . "/assets/libs/smartmenus/jquery.smartmenus.js", array( 'jquery' ), HDIA_THEME_VERSION, true );

			if ( Hdia::setting( 'lazy_load_images' ) ) {
				wp_enqueue_script( 'lazyload' );
			}

			// The comment-reply script
			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				if ( $post_type === 'post' ) {
					if ( Hdia::setting( 'single_post_comment_enable' ) === '1' ) {
						wp_enqueue_script( 'comment-reply' );
					}
				} elseif ( $post_type === 'project' ) {
					if ( Hdia::setting( 'single_project_comment_enable' ) === '1' ) {
						wp_enqueue_script( 'comment-reply' );
					}
				} elseif ( $post_type === 'service' ) {
					if ( Hdia::setting( 'single_service_comment_enable' ) === '1' ) {
						wp_enqueue_script( 'comment-reply' );
					}
				} else {
					wp_enqueue_script( 'comment-reply' );
				}
			}

			// Load Visual Composer JS above theme JS
			wp_enqueue_script( 'wpb_composer_front_js' );

			// Enqueue main JS
			wp_enqueue_script( 'hdia-script', HDIA_THEME_URI . "/assets/js/main.js", array(
				'jquery',
			), HDIA_THEME_VERSION, true );

			if ( Hdia_Helper::active_woocommerce() ) {
				wp_enqueue_script( 'hdia-woo', HDIA_THEME_URI . "/assets/js/woo.js", array(
					'hdia-script',
				), HDIA_THEME_VERSION, true );
			}

			// Enqueue custom variable JS
			$js_variables = array(
				'templateUrl'               => HDIA_THEME_URI,
				'ajaxurl'                   => admin_url( 'admin-ajax.php' ),
				'primary_color'             => Hdia::setting( 'primary_color' ),
				'header_sticky_enable'      => Hdia::setting( 'header_sticky_enable' ),
				'scroll_top_enable'         => Hdia::setting( 'scroll_top_enable' ),
				'lazyLoadImages'            => Hdia::setting( 'lazy_load_images' ),
				'light_gallery_auto_play'   => Hdia::setting( 'light_gallery_auto_play' ),
				'light_gallery_download'    => Hdia::setting( 'light_gallery_download' ),
				'light_gallery_full_screen' => Hdia::setting( 'light_gallery_full_screen' ),
				'light_gallery_zoom'        => Hdia::setting( 'light_gallery_zoom' ),
				'light_gallery_thumbnail'   => Hdia::setting( 'light_gallery_thumbnail' ),
				'light_gallery_share'       => Hdia::setting( 'light_gallery_share' ),
				'mobile_menu_breakpoint'    => Hdia::setting( 'mobile_menu_breakpoint' ),
				'isSingleProduct'           => is_singular( 'product' ),
				'like'                      => esc_html__( 'Like', 'hdia' ),
				'unlike'                    => esc_html__( 'Unlike', 'hdia' ),
			);
			wp_localize_script( 'hdia-script', '$insight', $js_variables );

			// Custom JS
			if ( Hdia::setting( 'custom_js_enable' ) == 1 ) {
				wp_add_inline_script( 'hdia-script', html_entity_decode( Hdia::setting( 'custom_js' ) ) );
			}
		}
	}

	new Hdia_Enqueue();
}
