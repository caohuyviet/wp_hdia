<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Initialize Global Variables
 */
if ( ! class_exists( 'Hdia_Global' ) ) {
	class Hdia_Global {
		protected static $instance = null;
		protected static $slider = '';
		protected static $slider_position = 'below';
		protected static $top_bar_type = '01';
		protected static $header_type = '01';
		protected static $title_bar_type = '01';
		protected static $sidebar_1 = '';
		protected static $sidebar_2 = '';
		protected static $sidebar_position = '';
		protected static $sidebar_special = '';
		protected static $sidebar_status = 'none';
		protected static $single_sidebar_width = '';
		protected static $dual_sidebar_width = '';
		protected static $footer_type = '';

		function __construct() {
			add_action( 'wp', array( $this, 'init_global_variable' ) );

			/**
			 * Setup global variables.
			 * Used priority 12 to wait override settings setup.
			 *
			 * @see Hdia_Customize->setup_override_settings()
			 */
			add_action( 'wp', array( $this, 'setup_global_variables' ), 12 );
		}

		public static function instance() {
			if ( null === self::$instance ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		function init_global_variable() {
			global $hdia_page_options;
			if ( is_singular( 'post' ) ) {
				$hdia_page_options = unserialize( get_post_meta( get_the_ID(), 'insight_post_options', true ) );
			} elseif ( is_singular( 'page' ) ) {
				$hdia_page_options = unserialize( get_post_meta( get_the_ID(), 'insight_page_options', true ) );
			} elseif ( is_singular( 'product' ) ) {
				$hdia_page_options = unserialize( get_post_meta( get_the_ID(), 'insight_product_options', true ) );
			} elseif ( is_singular( 'project' ) ) {
				$hdia_page_options = unserialize( get_post_meta( get_the_ID(), 'insight_project_options', true ) );
			} elseif ( is_singular( 'service' ) ) {
				$hdia_page_options = unserialize( get_post_meta( get_the_ID(), 'insight_service_options', true ) );
			}
			if ( function_exists( 'is_shop' ) && is_shop() ) {
				// Get page id of shop.
				$page_id              = wc_get_page_id( 'shop' );
				$hdia_page_options = unserialize( get_post_meta( $page_id, 'insight_page_options', true ) );
			}
		}

		function setup_global_variables() {
			$this->set_slider();
			$this->set_top_bar_type();
			$this->set_header_type();
			$this->set_title_bar_type();
			$this->set_sidebars();
			$this->set_footer_type();
		}

		function set_slider() {
			$alias    = Hdia_Helper::get_post_meta( 'revolution_slider', '' );
			$position = Hdia_Helper::get_post_meta( 'slider_position', '' );

			if ( $alias === '' ) {
				if ( is_search() && ! is_post_type_archive( 'product' ) ) {
					$alias    = Hdia::setting( 'search_page_rev_slider' );
					$position = Hdia::setting( 'search_page_slider_position' );
				} elseif ( is_post_type_archive( 'product' ) || ( function_exists( 'is_product_taxonomy' ) && is_product_taxonomy() ) ) {
					$alias    = Hdia::setting( 'product_archive_page_rev_slider' );
					$position = Hdia::setting( 'product_archive_page_slider_position' );
				} elseif ( is_archive() ) {
					$alias    = Hdia::setting( 'blog_archive_page_rev_slider' );
					$position = Hdia::setting( 'blog_archive_page_slider_position' );
				} elseif ( is_home() ) {
					$alias    = Hdia::setting( 'home_page_rev_slider' );
					$position = Hdia::setting( 'home_page_slider_position' );
				}
			}

			self::$slider          = $alias;
			self::$slider_position = $position;
		}

		function get_slider_alias() {
			return self::$slider;
		}

		function get_slider_position() {
			return self::$slider_position;
		}

		function set_top_bar_type() {
			$type = Hdia_Helper::get_post_meta( 'top_bar_type', '' );

			if ( $type === '' ) {
				$type = Hdia::setting( 'global_top_bar' );
			}

			self::$top_bar_type = $type;
		}

		function get_top_bar_type() {
			return self::$top_bar_type;
		}

		function set_header_type() {
			$header_type = Hdia_Helper::get_post_meta( 'header_type', '' );
			if ( $header_type === '' ) {
				if ( is_singular( 'post' ) ) {
					$header_type = Hdia::setting( 'single_post_header_type' );
				} elseif ( is_singular( 'service' ) ) {
					$header_type = Hdia::setting( 'single_service_header_type' );
				} elseif ( is_singular( 'project' ) ) {
					$header_type = Hdia::setting( 'single_project_header_type' );
				} elseif ( is_singular( 'product' ) ) {
					$header_type = Hdia::setting( 'single_product_header_type' );
				} elseif ( is_singular( 'page' ) ) {
					$header_type = Hdia::setting( 'single_page_header_type' );
				} else {
					$header_type = Hdia::setting( 'global_header' );
				}
			}

			if ( $header_type === '' ) {
				$header_type = Hdia::setting( 'global_header' );
			}

			self::$header_type = $header_type;
		}

		function get_header_type() {
			return self::$header_type;
		}

		function set_title_bar_type() {
			$title_bar_layout = Hdia_Helper::get_post_meta( 'page_title_bar_layout', 'default' );

			if ( $title_bar_layout === 'default' ) {
				if ( is_singular( 'post' ) ) {
					$title_bar_layout = Hdia::setting( 'single_post_title_bar_layout' );
				} elseif ( is_singular( 'page' ) ) {
					$title_bar_layout = Hdia::setting( 'single_page_title_bar_layout' );
				} elseif ( is_singular( 'product' ) ) {
					$title_bar_layout = Hdia::setting( 'single_product_title_bar_layout' );
				} elseif ( is_singular( 'project' ) ) {
					$title_bar_layout = Hdia::setting( 'single_project_title_bar_layout' );
				} elseif ( is_singular( 'service' ) ) {
					$title_bar_layout = Hdia::setting( 'single_service_title_bar_layout' );
				} else {
					$title_bar_layout = Hdia::setting( 'title_bar_layout' );
				}

				if ( $title_bar_layout === 'default' ) {
					$title_bar_layout = Hdia::setting( 'title_bar_layout' );
				}
			}

			self::$title_bar_type = $title_bar_layout;
		}

		function get_title_bar_type() {
			return self::$title_bar_type;
		}

		function set_sidebars() {
			$sidebar_special      = 'none';
			$single_sidebar_width = '';

			if ( is_search() && ! is_post_type_archive( 'product' ) ) {
				$page_sidebar1    = Hdia::setting( 'search_page_sidebar_1' );
				$page_sidebar2    = Hdia::setting( 'search_page_sidebar_2' );
				$sidebar_position = Hdia::setting( 'search_page_sidebar_position' );
				$sidebar_special  = Hdia::setting( 'search_page_sidebar_special' );
			} elseif ( is_post_type_archive( 'product' ) || ( function_exists( 'is_product_taxonomy' ) && is_product_taxonomy() ) ) {
				$page_sidebar1    = Hdia::setting( 'product_archive_page_sidebar_1' );
				$page_sidebar2    = Hdia::setting( 'product_archive_page_sidebar_2' );
				$sidebar_position = Hdia::setting( 'product_archive_page_sidebar_position' );
				$sidebar_special  = Hdia::setting( 'product_archive_page_sidebar_special' );
			} elseif ( is_post_type_archive( 'project' ) || is_tax( get_object_taxonomies( 'project' ) ) ) {
				$page_sidebar1    = Hdia::setting( 'project_archive_page_sidebar_1' );
				$page_sidebar2    = Hdia::setting( 'project_archive_page_sidebar_2' );
				$sidebar_position = Hdia::setting( 'project_archive_page_sidebar_position' );
				$sidebar_special  = Hdia::setting( 'project_archive_page_sidebar_special' );
			} elseif ( is_post_type_archive( 'service' ) || is_tax( get_object_taxonomies( 'service' ) ) ) {
				$page_sidebar1    = Hdia::setting( 'service_archive_page_sidebar_1' );
				$page_sidebar2    = Hdia::setting( 'service_archive_page_sidebar_2' );
				$sidebar_position = Hdia::setting( 'service_archive_page_sidebar_position' );
				$sidebar_special  = Hdia::setting( 'service_archive_page_sidebar_special' );
			} elseif ( is_archive() ) {
				$page_sidebar1    = Hdia::setting( 'blog_archive_page_sidebar_1' );
				$page_sidebar2    = Hdia::setting( 'blog_archive_page_sidebar_2' );
				$sidebar_position = Hdia::setting( 'blog_archive_page_sidebar_position' );
				$sidebar_special  = Hdia::setting( 'blog_archive_page_sidebar_special' );
			} elseif ( is_home() ) {
				$page_sidebar1    = Hdia::setting( 'home_page_sidebar_1' );
				$page_sidebar2    = Hdia::setting( 'home_page_sidebar_2' );
				$sidebar_position = Hdia::setting( 'home_page_sidebar_position' );
				$sidebar_special  = Hdia::setting( 'home_page_sidebar_special' );
			} elseif ( is_singular( 'post' ) ) {
				$page_sidebar1    = Hdia_Helper::get_post_meta( 'page_sidebar_1', 'default' );
				$page_sidebar2    = Hdia_Helper::get_post_meta( 'page_sidebar_2', 'default' );
				$sidebar_position = Hdia_Helper::get_post_meta( 'page_sidebar_position', 'default' );
				$sidebar_special  = Hdia::setting( 'post_page_sidebar_special' );

				if ( $page_sidebar1 === 'default' ) {
					$page_sidebar1 = Hdia::setting( 'post_page_sidebar_1' );
				}

				if ( $page_sidebar2 === 'default' ) {
					$page_sidebar2 = Hdia::setting( 'post_page_sidebar_2' );
				}

				if ( $sidebar_position === 'default' ) {
					$sidebar_position = Hdia::setting( 'post_page_sidebar_position' );
				}

			} elseif ( is_singular( 'project' ) ) {
				$page_sidebar1    = Hdia_Helper::get_post_meta( 'page_sidebar_1', 'default' );
				$page_sidebar2    = Hdia_Helper::get_post_meta( 'page_sidebar_2', 'default' );
				$sidebar_position = Hdia_Helper::get_post_meta( 'page_sidebar_position', 'default' );
				$sidebar_special  = Hdia::setting( 'project_page_sidebar_special' );

				if ( $page_sidebar1 === 'default' ) {
					$page_sidebar1 = Hdia::setting( 'project_page_sidebar_1' );
				}

				if ( $page_sidebar2 === 'default' ) {
					$page_sidebar2 = Hdia::setting( 'project_page_sidebar_2' );
				}

				if ( $sidebar_position === 'default' ) {
					$sidebar_position = Hdia::setting( 'project_page_sidebar_position' );
				}
			} elseif ( is_singular( 'service' ) ) {
				$page_sidebar1        = Hdia_Helper::get_post_meta( 'page_sidebar_1', 'default' );
				$page_sidebar2        = Hdia_Helper::get_post_meta( 'page_sidebar_2', 'default' );
				$sidebar_position     = Hdia_Helper::get_post_meta( 'page_sidebar_position', 'default' );
				$sidebar_special      = Hdia::setting( 'service_page_sidebar_special' );
				$single_sidebar_width = Hdia::setting( 'service_page_single_sidebar_width' );

				if ( $page_sidebar1 === 'default' ) {
					$page_sidebar1 = Hdia::setting( 'service_page_sidebar_1' );
				}

				if ( $page_sidebar2 === 'default' ) {
					$page_sidebar2 = Hdia::setting( 'service_page_sidebar_2' );
				}

				if ( $sidebar_position === 'default' ) {
					$sidebar_position = Hdia::setting( 'service_page_sidebar_position' );
				}
			} elseif ( is_singular( 'product' ) ) {
				$page_sidebar1    = Hdia_Helper::get_post_meta( 'page_sidebar_1', 'default' );
				$page_sidebar2    = Hdia_Helper::get_post_meta( 'page_sidebar_2', 'default' );
				$sidebar_position = Hdia_Helper::get_post_meta( 'page_sidebar_position', 'default' );
				$sidebar_special  = Hdia::setting( 'product_page_sidebar_special' );

				if ( $page_sidebar1 === 'default' ) {
					$page_sidebar1 = Hdia::setting( 'product_page_sidebar_1' );
				}

				if ( $page_sidebar2 === 'default' ) {
					$page_sidebar2 = Hdia::setting( 'product_page_sidebar_2' );
				}

				if ( $sidebar_position === 'default' ) {
					$sidebar_position = Hdia::setting( 'product_page_sidebar_position' );
				}

			} else {
				$page_sidebar1    = Hdia_Helper::get_post_meta( 'page_sidebar_1', 'default' );
				$page_sidebar2    = Hdia_Helper::get_post_meta( 'page_sidebar_2', 'default' );
				$sidebar_position = Hdia_Helper::get_post_meta( 'page_sidebar_position', 'default' );
				$sidebar_special  = Hdia::setting( 'page_sidebar_special' );

				if ( $page_sidebar1 === 'default' ) {
					$page_sidebar1 = Hdia::setting( 'page_sidebar_1' );
				}

				if ( $page_sidebar2 === 'default' ) {
					$page_sidebar2 = Hdia::setting( 'page_sidebar_2' );
				}

				if ( $sidebar_position === 'default' ) {
					$sidebar_position = Hdia::setting( 'page_sidebar_position' );
				}
			}

			if ( ! is_active_sidebar( $page_sidebar1 ) ) {
				$page_sidebar1 = 'none';
			}

			if ( ! is_active_sidebar( $page_sidebar2 ) ) {
				$page_sidebar2 = 'none';
			}

			if ( $single_sidebar_width === '' ) {
				$single_sidebar_width = Hdia::setting( 'single_sidebar_width' );
			}

			self::$sidebar_1            = $page_sidebar1;
			self::$sidebar_2            = $page_sidebar2;
			self::$sidebar_position     = $sidebar_position;
			self::$sidebar_special      = $sidebar_special;
			self::$single_sidebar_width = $single_sidebar_width;

			if ( $page_sidebar1 !== 'none' || $page_sidebar2 !== 'none' ) {
				self::$sidebar_status = 'one';
			}

			if ( $page_sidebar1 !== 'none' && $page_sidebar2 !== 'none' ) {
				self::$sidebar_status = 'both';
			}
		}

		function get_sidebar_1() {
			return self::$sidebar_1;
		}

		function get_sidebar_2() {
			return self::$sidebar_2;
		}

		function get_sidebar_position() {
			return self::$sidebar_position;
		}

		function get_sidebar_special() {
			return self::$sidebar_special;
		}

		function get_sidebar_status() {
			return self::$sidebar_status;
		}

		function get_single_sidebar_width() {
			return self::$single_sidebar_width;
		}

		function set_footer_type() {
			$footer = Hdia_Helper::get_post_meta( 'footer_page', 'default' );

			if ( $footer === 'default' ) {
				if ( is_singular( 'service' ) ) {
					$footer = Hdia::setting( 'single_service_footer_page' );
				} elseif ( is_singular( 'project' ) ) {
					$footer = Hdia::setting( 'single_project_footer_page' );
				}
			}

			if ( $footer === 'default' ) {
				$footer = Hdia::setting( 'footer_page' );
			}

			self::$footer_type = $footer;
		}

		function get_footer_type() {
			return self::$footer_type;
		}
	}

	new Hdia_Global();
}
