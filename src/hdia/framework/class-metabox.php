<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Hdia_Metabox' ) ) {
	class Hdia_Metabox {

		/**
		 * Hdia_Metabox constructor.
		 */
		public function __construct() {
			add_filter( 'insight_core_meta_boxes', array( $this, 'register_meta_boxes' ) );
			add_filter( 'hdia_page_meta_box_presets', array( $this, 'page_meta_box_presets' ) );
		}

		public function page_meta_box_presets( $presets ) {
			$presets[] = 'color_preset';

			return $presets;
		}

		/**
		 * Register Metabox
		 *
		 * @param $meta_boxes
		 *
		 * @return array
		 */
		public function register_meta_boxes( $meta_boxes ) {
			$page_registered_sidebars = Hdia_Helper::get_registered_sidebars( true );

			$general_options = array(
				array(
					'title'  => esc_html__( 'General', 'hdia' ),
					'fields' => array(
						array(
							'id'      => 'site_layout',
							'type'    => 'select',
							'title'   => esc_html__( 'Layout', 'hdia' ),
							'desc'    => 'When you choose Default, the value in <a href="' . admin_url( 'customize.php' ) . '" target="_blank">Customizer</a> will be used.',
							'options' => array(
								''      => esc_html__( 'Default', 'hdia' ),
								'boxed' => esc_html__( 'Boxed', 'hdia' ),
								'wide'  => esc_html__( 'Wide', 'hdia' ),
							),
							'default' => '',
						),
						array(
							'id'    => 'site_width',
							'type'  => 'text',
							'title' => esc_html__( 'Site Width', 'hdia' ),
							'desc'  => esc_html__( 'Controls the site width for this page. Enter value including any valid CSS unit, e.g 1200px. Leave blank to use global setting.', 'hdia' ),
						),
						array(
							'id'      => 'content_padding',
							'type'    => 'switch',
							'title'   => esc_html__( 'Content Padding', 'hdia' ),
							'desc'    => 'By default, the page content has the padding top & bottom is 100px.',
							'default' => '1',
							'options' => array(
								'1'      => esc_html__( 'Default', 'hdia' ),
								'0'      => esc_html__( 'No Padding', 'hdia' ),
								'top'    => esc_html__( 'No Top Padding', 'hdia' ),
								'bottom' => esc_html__( 'No Bottom Padding', 'hdia' ),
							),
						),
						array(
							'id'      => 'body_class',
							'type'    => 'text',
							'title'   => esc_html__( 'Body Class', 'hdia' ),
							'desc'    => esc_html__( 'Add a class name to body and refer to it in custom CSS.', 'hdia' ),
							'default' => '',
						),
					),
				),
				array(
					'title'  => esc_html__( 'Color', 'hdia' ),
					'fields' => array(
						array(
							'id'      => 'color_preset',
							'type'    => 'select',
							'title'   => esc_html__( 'Preset', 'hdia' ),
							'desc'    => esc_html__( 'Select a preset of color for this page.', 'hdia' ),
							'options' => array(
								'-1' => esc_html__( 'None', 'hdia' ),
								'01' => esc_html__( 'Preset 01', 'hdia' ),
								'02' => esc_html__( 'Preset 02', 'hdia' ),
								'03' => esc_html__( 'Preset 03', 'hdia' ),
								'04' => esc_html__( 'Preset 04', 'hdia' ),
								'05' => esc_html__( 'Preset 05', 'hdia' ),
								'06' => esc_html__( 'Preset 06', 'hdia' ),
								'07' => esc_html__( 'Preset 07', 'hdia' ),
								'08' => esc_html__( 'Preset 08', 'hdia' ),
								'10' => esc_html__( 'Preset 10', 'hdia' ),
								'11' => esc_html__( 'Preset 11', 'hdia' ),
							),
							'default' => '-1',
						),
					),
				),
				array(
					'title'  => esc_html__( 'Background', 'hdia' ),
					'fields' => array(
						array(
							'id'      => 'site_background_message',
							'type'    => 'message',
							'title'   => esc_html__( 'Info', 'hdia' ),
							'message' => esc_html__( 'These options controls the background on boxed mode.', 'hdia' ),
						),
						array(
							'id'    => 'site_background_color',
							'type'  => 'color',
							'title' => esc_html__( 'Background Color', 'hdia' ),
							'desc'  => esc_html__( 'Controls the background color of the outer background area in boxed mode of this page.', 'hdia' ),
						),
						array(
							'id'    => 'site_background_image',
							'type'  => 'media',
							'title' => esc_html__( 'Background Image', 'hdia' ),
							'desc'  => esc_html__( 'Controls the background image of the outer background area in boxed mode of this page.', 'hdia' ),
						),
						array(
							'id'      => 'site_background_repeat',
							'type'    => 'select',
							'title'   => esc_html__( 'Background Repeat', 'hdia' ),
							'desc'    => esc_html__( 'Controls the background repeat of the outer background area in boxed mode of this page.', 'hdia' ),
							'options' => array(
								'no-repeat' => esc_html__( 'No repeat', 'hdia' ),
								'repeat'    => esc_html__( 'Repeat', 'hdia' ),
								'repeat-x'  => esc_html__( 'Repeat X', 'hdia' ),
								'repeat-y'  => esc_html__( 'Repeat Y', 'hdia' ),
							),
						),
						array(
							'id'      => 'site_background_attachment',
							'type'    => 'select',
							'title'   => esc_html__( 'Background Attachment', 'hdia' ),
							'desc'    => esc_html__( 'Controls the background attachment of the outer background area in boxed mode of this page.', 'hdia' ),
							'options' => array(
								''       => esc_html__( 'Default', 'hdia' ),
								'fixed'  => esc_html__( 'Fixed', 'hdia' ),
								'scroll' => esc_html__( 'Scroll', 'hdia' ),
							),
						),
						array(
							'id'    => 'site_background_position',
							'type'  => 'text',
							'title' => esc_html__( 'Background Position', 'hdia' ),
							'desc'  => esc_html__( 'Controls the background position of the outer background area in boxed mode of this page.', 'hdia' ),
						),
						array(
							'id'    => 'site_background_size',
							'type'  => 'text',
							'title' => esc_html__( 'Background Size', 'hdia' ),
							'desc'  => esc_html__( 'Controls the background size of the outer background area in boxed mode of this page.', 'hdia' ),
						),
						array(
							'id'      => 'content_background_message',
							'type'    => 'message',
							'title'   => esc_html__( 'Info', 'hdia' ),
							'message' => esc_html__( 'These options controls the background of main content on this page.', 'hdia' ),
						),
						array(
							'id'    => 'content_background_color',
							'type'  => 'color',
							'title' => esc_html__( 'Background Color', 'hdia' ),
							'desc'  => esc_html__( 'Controls the background color of main content on this page.', 'hdia' ),
						),
						array(
							'id'    => 'content_background_image',
							'type'  => 'media',
							'title' => esc_html__( 'Background Image', 'hdia' ),
							'desc'  => esc_html__( 'Controls the background image of main content on this page.', 'hdia' ),
						),
						array(
							'id'      => 'content_background_repeat',
							'type'    => 'select',
							'title'   => esc_html__( 'Background Repeat', 'hdia' ),
							'desc'    => esc_html__( 'Controls the background repeat of main content on this page.', 'hdia' ),
							'options' => array(
								'no-repeat' => esc_html__( 'No repeat', 'hdia' ),
								'repeat'    => esc_html__( 'Repeat', 'hdia' ),
								'repeat-x'  => esc_html__( 'Repeat X', 'hdia' ),
								'repeat-y'  => esc_html__( 'Repeat Y', 'hdia' ),
							),
						),
						array(
							'id'    => 'content_background_position',
							'type'  => 'text',
							'title' => esc_html__( 'Background Position', 'hdia' ),
							'desc'  => esc_html__( 'Controls the background position of main content on this page.', 'hdia' ),
						),
					),
				),
				array(
					'title'  => esc_html__( 'Header', 'hdia' ),
					'fields' => array(
						array(
							'id'      => 'top_bar_type',
							'type'    => 'select',
							'title'   => esc_html__( 'Top Bar Type', 'hdia' ),
							'desc'    => 'When you choose Default, the value in <a href="' . admin_url( 'customize.php' ) . '" target="_blank">Customizer</a> will be used.',
							'default' => '',
							'options' => Hdia_Helper::get_top_bar_list( true ),
						),
						array(
							'id'      => 'header_type',
							'type'    => 'select',
							'title'   => esc_html__( 'Header Type', 'hdia' ),
							'desc'    => 'When you choose Default, the value in <a href="' . admin_url( 'customize.php' ) . '" target="_blank">Customizer</a> will be used.',
							'default' => '',
							'options' => Hdia_Helper::get_header_list( true ),
						),
						array(
							'id'      => 'menu_display',
							'type'    => 'select',
							'title'   => esc_html__( 'Primary Menu', 'hdia' ),
							'desc'    => esc_html__( 'Select which menu displays on this page.', 'hdia' ),
							'default' => '',
							'options' => Hdia_Helper::get_all_menus(),
						),
						array(
							'id'      => 'menu_one_page',
							'type'    => 'switch',
							'title'   => esc_html__( 'One Page Menu', 'hdia' ),
							'default' => '0',
							'options' => array(
								'0' => esc_html__( 'Disable', 'hdia' ),
								'1' => esc_html__( 'Enable', 'hdia' ),
							),
						),
						array(
							'id'      => 'custom_logo',
							'type'    => 'media',
							'title'   => esc_html__( 'Custom Logo', 'hdia' ),
							'desc'    => esc_html__( 'Select custom logo for this page.', 'hdia' ),
							'default' => '',
						),
						array(
							'id'      => 'custom_logo_retina',
							'type'    => 'media',
							'title'   => esc_html__( 'Custom Logo Retina', 'hdia' ),
							'desc'    => esc_html__( 'Select custom logo retina for this page.', 'hdia' ),
							'default' => '',
						),
						array(
							'id'      => 'custom_logo_width',
							'type'    => 'text',
							'title'   => esc_html__( 'Custom Logo Width', 'hdia' ),
							'desc'    => esc_html__( 'Controls the width of logo, e.g 190px', 'hdia' ),
							'default' => '',
						),
						array(
							'id'      => 'custom_sticky_logo_width',
							'type'    => 'text',
							'title'   => esc_html__( 'Custom Sticky Logo Width', 'hdia' ),
							'desc'    => esc_html__( 'Controls the width of sticky logo, e.g 150px', 'hdia' ),
							'default' => '',
						),
					),
				),
				array(
					'title'  => esc_html__( 'Title', 'hdia' ),
					'fields' => array(
						array(
							'id'      => 'page_title_bar_layout',
							'type'    => 'select',
							'title'   => esc_html__( 'Layout', 'hdia' ),
							'desc'    => 'When you choose Default, the value in <a href="' . admin_url( 'customize.php' ) . '" target="_blank">Customizer</a> will be used.',
							'default' => 'default',
							'options' => Hdia_Helper::get_title_bar_list( true ),
						),
						array(
							'id'      => 'page_title_bar_background_color',
							'type'    => 'color',
							'title'   => esc_html__( 'Background Color', 'hdia' ),
							'default' => '',
						),
						array(
							'id'      => 'page_title_bar_background',
							'type'    => 'media',
							'title'   => esc_html__( 'Background Image', 'hdia' ),
							'default' => '',
						),
						array(
							'id'      => 'page_title_bar_background_overlay',
							'type'    => 'color',
							'title'   => esc_html__( 'Background Overlay', 'hdia' ),
							'default' => '',
						),
						array(
							'id'    => 'page_title_bar_custom_heading',
							'type'  => 'text',
							'title' => esc_html__( 'Custom Title', 'hdia' ),
							'desc'  => esc_html__( 'Insert custom heading for the page title bar. Leave blank to use default.', 'hdia' ),
						),
					),
				),
				array(
					'title'  => esc_html__( 'Sidebar', 'hdia' ),
					'fields' => array(
						array(
							'id'      => 'page_sidebar_position',
							'type'    => 'switch',
							'title'   => esc_html__( 'Position', 'hdia' ),
							'desc'    => 'When you choose Default, the value in <a href="' . admin_url( 'customize.php' ) . '" target="_blank">Customizer</a> will be used.',
							'default' => 'default',
							'options' => Hdia_Helper::get_list_sidebar_positions( true ),
						),
						array(
							'id'      => 'page_sidebar_1',
							'type'    => 'select',
							'title'   => esc_html__( 'Sidebar 1', 'hdia' ),
							'desc'    => esc_html__( 'Select sidebar 1 that will display on this page.', 'hdia' ),
							'default' => 'default',
							'options' => $page_registered_sidebars,
						),
						array(
							'id'      => 'page_sidebar_2',
							'type'    => 'select',
							'title'   => esc_html__( 'Sidebar 2', 'hdia' ),
							'desc'    => esc_html__( 'Select sidebar 2 that will display on this page.', 'hdia' ),
							'default' => 'default',
							'options' => $page_registered_sidebars,
						),
					),
				),
				array(
					'title'  => esc_html__( 'Slider', 'hdia' ),
					'fields' => array(
						array(
							'id'      => 'revolution_slider',
							'type'    => 'select',
							'title'   => esc_html__( 'Revolution Slider', 'hdia' ),
							'desc'    => esc_html__( 'Select the unique name of the slider.', 'hdia' ),
							'options' => Hdia_Helper::get_list_revslider(),
						),
						array(
							'id'      => 'slider_position',
							'type'    => 'select',
							'title'   => esc_html__( 'Position', 'hdia' ),
							'default' => 'below',
							'options' => array(
								'above' => esc_html__( 'Above Header', 'hdia' ),
								'below' => esc_html__( 'Below Header', 'hdia' ),
							),
						),
					),
				),
				array(
					'title'  => esc_html__( 'Footer', 'hdia' ),
					'fields' => array(
						array(
							'id'      => 'footer_page',
							'type'    => 'select',
							'title'   => esc_html__( 'Footer', 'hdia' ),
							'desc'    => 'When you choose Default, the value in <a href="' . admin_url( 'customize.php' ) . '" target="_blank">Customizer</a> will be used.',
							'default' => 'default',
							'options' => Hdia_Footer::get_list_footers( true ),
						),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_page_options',
				'title'      => esc_html__( 'Page Options', 'hdia' ),
				'post_types' => array( 'page' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => $general_options,
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_post_options',
				'title'      => esc_html__( 'Page Options', 'hdia' ),
				'post_types' => array( 'post' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array_merge( array(
							array(
								'title'  => esc_html__( 'Post', 'hdia' ),
								'fields' => array(
									array(
										'id'    => 'post_gallery',
										'type'  => 'gallery',
										'title' => esc_html__( 'Gallery Format', 'hdia' ),
									),
									array(
										'id'    => 'post_video',
										'type'  => 'textarea',
										'title' => esc_html__( 'Video Format', 'hdia' ),
									),
									array(
										'id'    => 'post_audio',
										'type'  => 'textarea',
										'title' => esc_html__( 'Audio Format', 'hdia' ),
									),
									array(
										'id'    => 'post_quote_text',
										'type'  => 'text',
										'title' => esc_html__( 'Quote Format - Source Text', 'hdia' ),
									),
									array(
										'id'    => 'post_quote_name',
										'type'  => 'text',
										'title' => esc_html__( 'Quote Format - Source Name', 'hdia' ),
									),
									array(
										'id'    => 'post_quote_url',
										'type'  => 'text',
										'title' => esc_html__( 'Quote Format - Source Url', 'hdia' ),
									),
									array(
										'id'    => 'post_link',
										'type'  => 'text',
										'title' => esc_html__( 'Link Format', 'hdia' ),
									),
								),
							),
						), $general_options ),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_product_options',
				'title'      => esc_html__( 'Page Options', 'hdia' ),
				'post_types' => array( 'product' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => $general_options,
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_project_options',
				'title'      => esc_html__( 'Page Options', 'hdia' ),
				'post_types' => array( 'project' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array_merge( array(
							array(
								'title'  => esc_html__( 'Project', 'hdia' ),
								'fields' => array(
									array(
										'id'      => 'project_message',
										'type'    => 'message',
										'title'   => esc_html__( 'Info', 'hdia' ),
										'message' => esc_html__( 'These options just use for the project.', 'hdia' ),
									),
									array(
										'id'      => 'project_vi_tri',
										'type'    => 'text',
										'title'   => esc_html__( 'Vị trí', 'hdia' ),
										'default' => '',
									),
									array(
										'id'      => 'project_chu_dau_tu',
										'type'    => 'text',
										'title'   => esc_html__( 'Chủ đầu tư', 'hdia' ),
										'default' => '',
									),
								),
							),
						), $general_options ),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_service_options',
				'title'      => esc_html__( 'Page Options', 'hdia' ),
				'post_types' => array( 'service' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array_merge( array(
							array(
								'title'  => esc_html__( 'Service', 'hdia' ),
								'fields' => array(
									array(
										'id'      => 'service_message',
										'type'    => 'message',
										'title'   => esc_html__( 'Info', 'hdia' ),
										'message' => esc_html__( 'These options just use for the service.', 'hdia' ),
									),
									array(
										'id'      => 'service_style',
										'type'    => 'select',
										'title'   => esc_html__( 'Style', 'hdia' ),
										'desc'    => esc_html__( 'Select style for the single service page.', 'hdia' ),
										'default' => '',
										'options' => array(
											''   => esc_html__( 'Default', 'hdia' ),
											'01' => esc_html__( 'Style 01', 'hdia' ),
											'02' => esc_html__( 'Style 02', 'hdia' ),
										),
									),
									array(
										'id'      => 'service_icon',
										'type'    => 'text',
										'title'   => esc_html__( 'Icon', 'hdia' ),
										'desc'    => esc_html__( 'Add the icon for this service.', 'hdia' ),
										'default' => '',
									),
								),
							),
						), $general_options ),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_testimonial_options',
				'title'      => esc_html__( 'Testimonial Options', 'hdia' ),
				'post_types' => array( 'testimonial' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array(
							array(
								'title'  => esc_html__( 'Testimonial Details', 'hdia' ),
								'fields' => array(
									array(
										'id'      => 'by_line',
										'type'    => 'text',
										'title'   => esc_html__( 'By Line', 'hdia' ),
										'desc'    => esc_html__( 'Enter a byline for the customer giving this testimonial (for example: "CEO of Thememove").', 'hdia' ),
										'default' => '',
									),
									array(
										'id'      => 'url',
										'type'    => 'text',
										'title'   => esc_html__( 'Url', 'hdia' ),
										'desc'    => esc_html__( 'Enter a URL that applies to this customer (for example: http://www.thememove.com/).', 'hdia' ),
										'default' => '',
									),
									array(
										'id'      => 'rating',
										'type'    => 'select',
										'title'   => esc_html__( 'Rating', 'hdia' ),
										'default' => '',
										'options' => array(
											''  => esc_html__( 'Select a rating', 'hdia' ),
											'1' => esc_html__( '1 Star', 'hdia' ),
											'2' => esc_html__( '2 Stars', 'hdia' ),
											'3' => esc_html__( '3 Stars', 'hdia' ),
											'4' => esc_html__( '4 Stars', 'hdia' ),
											'5' => esc_html__( '5 Stars', 'hdia' ),
										),
									),
								),
							),
						),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_footer_options',
				'title'      => esc_html__( 'Footer Options', 'hdia' ),
				'post_types' => array( 'ic_footer' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array(
							array(
								'title'  => esc_html__( 'Effect', 'hdia' ),
								'fields' => array(
									array(
										'id'      => 'effect',
										'type'    => 'switch',
										'title'   => esc_html__( 'Effect', 'hdia' ),
										'default' => '',
										'options' => array(
											''         => esc_html__( 'Normal', 'hdia' ),
											'parallax' => esc_html__( 'Parallax', 'hdia' ),
										),
									),
								),
							),
							array(
								'title'  => esc_html__( 'Styling', 'hdia' ),
								'fields' => array(
									array(
										'id'      => 'style',
										'type'    => 'select',
										'title'   => esc_html__( 'Style', 'hdia' ),
										'default' => '01',
										'options' => array(
											'01' => esc_html__( 'Style 01', 'hdia' ),
											'02' => esc_html__( 'Style 02', 'hdia' ),
											'03' => esc_html__( 'Style 03', 'hdia' ),
											'04' => esc_html__( 'Style 04', 'hdia' ),
											'05' => esc_html__( 'Style 05', 'hdia' ),
											'06' => esc_html__( 'Style 06', 'hdia' ),
											'07' => esc_html__( 'Style 07', 'hdia' ),
											'08' => esc_html__( 'Style 08', 'hdia' ),
											'10' => esc_html__( 'Style 10', 'hdia' ),
											'11' => esc_html__( 'Style 11', 'hdia' ),
										),
									),
								),
							),
						),
					),
				),
			);

			return $meta_boxes;
		}

	}

	new Hdia_Metabox();
}
