<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Hdia {

	const PRIMARY_FONT = 'Roboto';
	const PRIMARY_COLOR = '#01418B';
	const SECONDARY_COLOR = '#262626';
	const HEADING_COLOR = '#262626';
	const TEXT_COLOR = '#888';

	/**
	 * Requirement one file.
	 *
	 * @param string $file path of file
	 */
	public static function require_file( $file = '' ) {
		if ( file_exists( $file ) ) {
			require_once $file;
		} else {
			wp_die( esc_html__( 'Could not load theme file: ', 'hdia' ) . $file );
		}
	}

	/**
	 * Requirement multi files.
	 *
	 * @param array $files array path of files
	 */
	public static function require_files( $files = array() ) {
		if ( empty( $files ) ) {
			return;
		}

		foreach ( $files as $file ) {
			if ( file_exists( $file ) ) {
				require_once $file;
			} else {
				wp_die( esc_html__( 'Could not load theme file: ', 'hdia' ) . $file );
			}
		}
	}

	/**
	 * Mobile Menu
	 */
	public static function menu_mobile() {
		$args = array(
			'theme_location' => 'mobile',
			'container'      => 'ul',
			'menu_class'     => 'menu__container',
			'menu_id'        => 'mobile-menu-primary',
		);

		if ( ! has_nav_menu( 'mobile' ) && has_nav_menu( 'primary' ) ) {
			$args['theme_location'] = 'primary';
		}

		if ( ( has_nav_menu( 'mobile' ) || has_nav_menu( 'primary' ) ) && class_exists( 'Hdia_Walker_Nav_Menu' ) ) {
			$args['walker'] = new Hdia_Walker_Nav_Menu;
		}

		wp_nav_menu( $args );
	}

	/**
	 * Off Canvas Menu
	 */
	public static function off_canvas_menu_primary() {
		self::menu_primary( array(
			'menu_class' => 'menu__container',
			'menu_id'    => 'off-canvas-menu-primary',
		) );
	}

	/**
	 * Primary Menu
	 */
	public static function menu_primary( $args = array() ) {
		$defaults = array(
			'theme_location' => 'primary',
			'container'      => 'ul',
			'menu_class'     => 'menu__container sm sm-simple',
		);
		$args     = wp_parse_args( $args, $defaults );

		if ( has_nav_menu( 'primary' ) && class_exists( 'Hdia_Walker_Nav_Menu' ) ) {
			$args['walker'] = new Hdia_Walker_Nav_Menu;
		}

		$menu = Hdia_Helper::get_post_meta( 'menu_display', '' );

		if ( $menu !== '' ) {
			$args['menu'] = $menu;
		}

		wp_nav_menu( $args );
	}

	/**
	 * Logo
	 */
	public static function branding_logo() {
		$logo_url              = Hdia_Helper::get_post_meta( 'custom_logo', '' );
		$logo_retina_url       = Hdia_Helper::get_post_meta( 'custom_logo_retina', '' );
		$logo_light_url        = Hdia::setting( 'logo_light' );
		$logo_light_retina_url = Hdia::setting( 'logo_light_retina' );
		$logo_dark_url         = Hdia::setting( 'logo_dark' );
		$logo_dark_retina_url  = Hdia::setting( 'logo_dark_retina' );
		?>
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
			<?php if ( $logo_url !== '' ) { ?>
				<img src="<?php echo esc_url( $logo_url ); ?>"
				     alt="<?php bloginfo( 'name' ); ?>" class="custom-logo"
					<?php echo( ( $logo_retina_url != '' ) ? 'srcset="' . esc_url( $logo_retina_url ) . ' 2x"' : '' ); ?>/>
			<?php } else { ?>
				<img src="<?php echo esc_url( $logo_light_url ); ?>"
				     alt="<?php bloginfo( 'name' ); ?>" class="light-logo"
					<?php echo( ( $logo_light_retina_url != '' ) ? 'srcset="' . esc_url( $logo_light_retina_url ) . ' 2x"' : '' ); ?>/>
				<img src="<?php echo esc_url( $logo_dark_url ); ?>"
				     alt="<?php bloginfo( 'name' ); ?>" class="dark-logo"
					<?php echo( ( $logo_dark_retina_url != '' ) ? 'srcset="' . esc_url( $logo_dark_retina_url ) . ' 2x"' : '' ); ?>/>
			<?php } ?>
		</a>
		<?php
	}

	/**
	 * Get settings for Kirki
	 *
	 * @param string $setting
	 *
	 * @return mixed
	 */
	public static function setting( $setting = '' ) {
		$settings = Hdia_Kirki::get_option( 'theme', $setting );

		return $settings;
	}

	/**
	 * Adds custom attributes to the body tag.
	 */
	public static function body_attributes() {
		$attrs = apply_filters( 'hdia_body_attributes', array() );

		$attrs_string = '';
		if ( ! empty( $attrs ) ) {
			foreach ( $attrs as $attr => $value ) {
				$attrs_string .= " {$attr}=" . '"' . esc_attr( $value ) . '"';
			}
		}

		echo '' . $attrs_string;
	}

	/**
	 * Adds custom classes to the header.
	 *
	 * @param string $class extra class
	 */
	public static function top_bar_class( $class = '' ) {
		$classes = array( 'page-top-bar' );

		$type = Hdia_Global::instance()->get_top_bar_type();

		$classes[] = "top-bar-{$type}";

		if ( ! empty( $class ) ) {
			if ( ! is_array( $class ) ) {
				$class = preg_split( '#\s+#', $class );
			}
			$classes = array_merge( $classes, $class );
		} else {
			// Ensure that we always coerce class to being an array.
			$class = array();
		}

		$classes = apply_filters( 'hdia_top_bar_class', $classes, $class );

		echo 'class="' . esc_attr( join( ' ', $classes ) ) . '"';
	}

	/**
	 * Adds custom classes to the header.
	 */
	public static function header_class( $class = '' ) {
		$classes = array( 'page-header' );

		$header_type = Hdia_Global::instance()->get_header_type();

		$classes[] = "header-{$header_type}";

		$_overlay_enable = Hdia::setting( "header_style_{$header_type}_overlay" );

		if ( $_overlay_enable === '1' ) {
			$classes[] = 'header-layout-fixed';
		}

		$_logo     = Hdia::setting( "header_style_{$header_type}_logo" );
		$classes[] = "{$_logo}-logo-version";

		$_sticky_logo = Hdia::setting( "header_style_{$header_type}_sticky_logo" );
		$classes[]    = "{$_sticky_logo}-logo-version";

		if ( ! empty( $class ) ) {
			if ( ! is_array( $class ) ) {
				$class = preg_split( '#\s+#', $class );
			}
			$classes = array_merge( $classes, $class );
		} else {
			// Ensure that we always coerce class to being an array.
			$class = array();
		}

		$classes = apply_filters( 'hdia_header_class', $classes, $class );

		echo 'class="' . esc_attr( join( ' ', $classes ) ) . '"';
	}

	/**
	 * Adds custom classes to the branding.
	 */
	public static function branding_class( $class = '' ) {
		$classes = array( 'branding' );

		if ( ! empty( $class ) ) {
			if ( ! is_array( $class ) ) {
				$class = preg_split( '#\s+#', $class );
			}
			$classes = array_merge( $classes, $class );
		} else {
			// Ensure that we always coerce class to being an array.
			$class = array();
		}

		$classes = apply_filters( 'hdia_branding_class', $classes, $class );

		echo 'class="' . esc_attr( join( ' ', $classes ) ) . '"';
	}

	/**
	 * Adds custom classes to the navigation.
	 */
	public static function navigation_class( $class = '' ) {
		$classes = array( 'navigation page-navigation' );

		if ( ! empty( $class ) ) {
			if ( ! is_array( $class ) ) {
				$class = preg_split( '#\s+#', $class );
			}
			$classes = array_merge( $classes, $class );
		} else {
			// Ensure that we always coerce class to being an array.
			$class = array();
		}

		$classes = apply_filters( 'hdia_navigation_class', $classes, $class );

		echo 'class="' . esc_attr( join( ' ', $classes ) ) . '"';
	}

	/**
	 * Adds custom classes to the footer.
	 */
	public static function footer_class( $class = '' ) {
		$classes = array( 'page-footer' );

		if ( ! empty( $class ) ) {
			if ( ! is_array( $class ) ) {
				$class = preg_split( '#\s+#', $class );
			}
			$classes = array_merge( $classes, $class );
		} else {
			// Ensure that we always coerce class to being an array.
			$class = array();
		}

		$classes = apply_filters( 'hdia_footer_class', $classes, $class );

		echo 'class="' . esc_attr( join( ' ', $classes ) ) . '"';
	}
}
