<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Hdia_VC_Icon_Flat' ) ) {
	class Hdia_VC_Icon_Flat {

		public function __construct() {
			/*
			 * Add styles & script file only on add new or edit post type.
			 */
			add_action( 'load-post.php', array( $this, 'enqueue_scripts' ) );
			add_action( 'load-post-new.php', array( $this, 'enqueue_scripts' ) );

			add_filter( 'vc_iconpicker-type-flat', array( $this, 'add_fonts' ) );

			add_action( 'vc_enqueue_font_icon_element', array( $this, 'vc_element_enqueue' ) );

			add_filter( 'hdia_vc_icon_libraries', array( $this, 'add_to_libraries' ) );
		}

		public function add_to_libraries( $libraries ) {
			$libraries[ esc_html__( 'Flat', 'hdia' ) ] = 'flat';

			return $libraries;
		}

		public function vc_element_enqueue( $font ) {
			switch ( $font ) {
				case 'flat':
					wp_enqueue_style( 'font-flat', HDIA_THEME_URI . '/assets/fonts/flat/font-flat.css', null, null );
					break;
			}
		}

		public function enqueue_scripts() {
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
		}

		function admin_enqueue_scripts() {
			wp_enqueue_style( 'font-flat', HDIA_THEME_URI . '/assets/fonts/flat/font-flat.css', null, null );
		}

		public function add_fonts( $icons ) {
			$new_icons = array(
				array( 'flaticon-001-worker-1' => 'worker 1' ),
				array( 'flaticon-002-welding' => 'welding' ),
				array( 'flaticon-003-waste' => 'waste' ),
				array( 'flaticon-004-walkie-talkie' => 'walkie talkie' ),
				array( 'flaticon-005-valve' => 'valve' ),
				array( 'flaticon-006-truck' => 'truck' ),
				array( 'flaticon-007-tools' => 'tools' ),
				array( 'flaticon-008-machine-1' => 'machine 1' ),
				array( 'flaticon-009-storage' => 'storage' ),
				array( 'flaticon-010-tank-1' => 'tank 1' ),
				array( 'flaticon-011-siren' => 'siren' ),
				array( 'flaticon-012-scheme' => 'scheme' ),
				array( 'flaticon-013-danger' => 'danger' ),
				array( 'flaticon-014-robot-arm' => 'robot arm' ),
				array( 'flaticon-015-cart' => 'cart' ),
				array( 'flaticon-016-gear' => 'gear' ),
				array( 'flaticon-017-pump' => 'pump' ),
				array( 'flaticon-018-power-tower' => 'power tower' ),
				array( 'flaticon-019-power-press' => 'power press' ),
				array( 'flaticon-020-planning' => 'planning' ),
				array( 'flaticon-021-worker' => 'worker' ),
				array( 'flaticon-022-tank' => 'tank' ),
				array( 'flaticon-023-microprocessor' => 'microprocessor' ),
				array( 'flaticon-024-statistics' => 'statistics' ),
				array( 'flaticon-025-meter' => 'meter' ),
				array( 'flaticon-026-mechanism' => 'mechanism' ),
				array( 'flaticon-027-material' => 'material' ),
				array( 'flaticon-028-manufacturing-plant' => 'manufacturing plant' ),
				array( 'flaticon-029-manufacturing' => 'manufacturing' ),
				array( 'flaticon-030-management' => 'management' ),
				array( 'flaticon-031-machine' => 'machine' ),
				array( 'flaticon-032-gears-1' => 'gears 1' ),
				array( 'flaticon-033-laser' => 'laser' ),
				array( 'flaticon-034-industrial-robot' => 'industrial robot' ),
				array( 'flaticon-035-parcel' => 'parcel' ),
				array( 'flaticon-036-gears' => 'gears' ),
				array( 'flaticon-037-forklift' => 'forklift' ),
				array( 'flaticon-038-food' => 'food' ),
				array( 'flaticon-039-factory-1' => 'factory 1' ),
				array( 'flaticon-040-factory' => 'factory' ),
				array( 'flaticon-041-eco' => 'eco' ),
				array( 'flaticon-042-monitor' => 'monitor' ),
				array( 'flaticon-043-wheel' => 'wheel' ),
				array( 'flaticon-044-conveyor' => 'conveyor' ),
				array( 'flaticon-045-controller' => 'controller' ),
				array( 'flaticon-046-control-system' => 'control system' ),
				array( 'flaticon-047-control-lever' => 'control lever' ),
				array( 'flaticon-048-chemical' => 'chemical' ),
				array( 'flaticon-049-container' => 'container' ),
				array( 'flaticon-050-boxes' => 'boxes' ),
			);

			return array_merge( $icons, $new_icons );
		}
	}

	new Hdia_VC_Icon_Flat();
}
