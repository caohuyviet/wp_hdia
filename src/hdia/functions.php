<?php
/**
 * Define constant
 */
$theme = wp_get_theme();

if ( ! empty( $theme['Template'] ) ) {
	$theme = wp_get_theme( $theme['Template'] );
}

if ( ! defined( 'DS' ) ) {
	define( 'DS', DIRECTORY_SEPARATOR );
}

define( 'HDIA_THEME_NAME', $theme['Name'] );
define( 'HDIA_THEME_VERSION', $theme['Version'] );
define( 'HDIA_THEME_DIR', get_template_directory() );
define( 'HDIA_THEME_URI', get_template_directory_uri() );
define( 'HDIA_THEME_IMAGE_DIR', get_template_directory() . DS . 'assets' . DS . 'images' );
define( 'HDIA_THEME_IMAGE_URI', get_template_directory_uri() . DS . 'assets' . DS . 'images' );
define( 'HDIA_CHILD_THEME_URI', get_stylesheet_directory_uri() );
define( 'HDIA_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'HDIA_FRAMEWORK_DIR', get_template_directory() . DS . 'framework' );
define( 'HDIA_CUSTOMIZER_DIR', HDIA_THEME_DIR . DS . 'customizer' );
define( 'HDIA_WIDGETS_DIR', HDIA_THEME_DIR . DS . 'widgets' );
define( 'HDIA_VC_MAPS_DIR', HDIA_THEME_DIR . DS . 'vc-extend' . DS . 'vc-maps' );
define( 'HDIA_VC_PARAMS_DIR', HDIA_THEME_DIR . DS . 'vc-extend' . DS . 'vc-params' );
define( 'HDIA_VC_SHORTCODE_CATEGORY', esc_html__( 'By', 'hdia' ) . ' ' . HDIA_THEME_NAME );
define( 'HDIA_PROTOCOL', is_ssl() ? 'https' : 'http' );

require_once HDIA_FRAMEWORK_DIR . '/class-static.php';

$files = array(
	HDIA_FRAMEWORK_DIR . '/class-init.php',
	HDIA_FRAMEWORK_DIR . '/class-global.php',
	HDIA_FRAMEWORK_DIR . '/class-actions-filters.php',
	HDIA_FRAMEWORK_DIR . '/class-admin.php',
	HDIA_FRAMEWORK_DIR . '/class-customize.php',
	HDIA_FRAMEWORK_DIR . '/class-enqueue.php',
	HDIA_FRAMEWORK_DIR . '/class-functions.php',
	HDIA_FRAMEWORK_DIR . '/class-helper.php',
	HDIA_FRAMEWORK_DIR . '/class-color.php',
	HDIA_FRAMEWORK_DIR . '/class-import.php',
	HDIA_FRAMEWORK_DIR . '/class-kirki.php',
	HDIA_FRAMEWORK_DIR . '/class-metabox.php',
	HDIA_FRAMEWORK_DIR . '/class-plugins.php',
	HDIA_FRAMEWORK_DIR . '/class-query.php',
	HDIA_FRAMEWORK_DIR . '/class-custom-css.php',
	HDIA_FRAMEWORK_DIR . '/class-templates.php',
	HDIA_FRAMEWORK_DIR . '/class-aqua-resizer.php',
	HDIA_FRAMEWORK_DIR . '/class-visual-composer.php',
	HDIA_FRAMEWORK_DIR . '/class-vc-icon-ion.php',
	HDIA_FRAMEWORK_DIR . '/class-vc-icon-themify.php',
	HDIA_FRAMEWORK_DIR . '/class-vc-icon-pe-stroke-7.php',
	HDIA_FRAMEWORK_DIR . '/class-vc-icon-flat.php',
	HDIA_FRAMEWORK_DIR . '/class-vc-icon-fontawesome5.php',
	HDIA_FRAMEWORK_DIR . '/class-vc-icon-icomoon.php',
	HDIA_FRAMEWORK_DIR . '/class-walker-nav-menu.php',
	HDIA_FRAMEWORK_DIR . '/class-widget.php',
	HDIA_FRAMEWORK_DIR . '/class-widgets.php',
	HDIA_FRAMEWORK_DIR . '/class-footer.php',
	HDIA_FRAMEWORK_DIR . '/class-post-type-blog.php',
	HDIA_FRAMEWORK_DIR . '/class-post-type-service.php',
	HDIA_FRAMEWORK_DIR . '/class-post-type-project.php',
	HDIA_FRAMEWORK_DIR . '/class-woo.php',
	HDIA_FRAMEWORK_DIR . '/tgm-plugin-activation.php',
	HDIA_FRAMEWORK_DIR . '/tgm-plugin-registration.php',
);

/**
 * Load Framework.
 */
Hdia::require_files( $files );

/**
 * Init the theme
 */
Hdia_Init::instance();
