<?php if ( has_post_thumbnail() ) { ?>
	<?php
	$hdia_thumbnail_w = 1170;
	$hdia_thumbnail_h = 740;

	$full_image_size = get_the_post_thumbnail_url( null, 'full' );
	?>
	<div class="post-feature post-thumbnail">
		<?php
		Hdia_Helper::get_lazy_load_image( array(
			'url'    => $full_image_size,
			'width'  => $hdia_thumbnail_w,
			'height' => $hdia_thumbnail_h,
			'crop'   => true,
			'echo'   => true,
			'alt'    => get_the_title(),
		) );
		?>

	</div>
<?php } ?>
