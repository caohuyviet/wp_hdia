<div class="post-meta">
	<?php if ( Hdia::setting( 'single_post_author_enable' ) === '1' ) : ?>
		<div class="post-author-meta">
			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>">
				<?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
				<span><?php the_author(); ?></span>
			</a>
		</div>
	<?php endif; ?>

	<?php if ( Hdia::setting( 'single_post_date_enable' ) === '1' ) : ?>
		<div class="post-date">
			<span class="meta-icon"><i class="far fa-calendar"></i></span>
			<?php echo get_the_date(); ?>
		</div>
	<?php endif; ?>

	<?php if ( Hdia::setting( 'single_post_comment_count_enable' ) ) : ?>
		<div class="post-comments-number">
			<span class="meta-icon">
					<i class="far fa-comment-alt-lines"></i>
				</span>
			<?php
			$comment_count = get_comments_number();
			printf( esc_html( _n( '%1$s Comment', '%1$s Comments', $comment_count, 'hdia' ) ), $comment_count );
			?>
		</div>
	<?php endif; ?>

	<?php if ( Hdia::setting( 'single_post_view_enable' ) === '1' && class_exists( 'InsightCore_View' ) ) : ?>
		<div class="post-view">
			<span class="meta-icon">
				<i class="far fa-eye"></i>
			</span>
			<?php
			$views = InsightCore_View::get_views();
			printf( esc_html( _n( '%1$s View', '%1$s Views', $views, 'hdia' ) ), $views );
			?>
		</div>
	<?php endif; ?>

	<?php if ( Hdia::setting( 'single_post_like_enable' ) === '1' ) : ?>
		<?php Hdia_Templates::post_likes(); ?>
	<?php endif; ?>
</div>
