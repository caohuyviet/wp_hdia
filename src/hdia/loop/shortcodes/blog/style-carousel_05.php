<?php
while ( $hdia_query->have_posts() ) :
	$hdia_query->the_post();
	$classes = array( 'post-item swiper-slide' );
	?>
	<div <?php post_class( implode( ' ', $classes ) ); ?>>
		<div class="post-item-wrap">

			<?php if ( has_post_thumbnail() ) { ?>
				<div class="post-feature-wrap">
					<div class="post-feature post-thumbnail">
						<?php if ( has_category() ) : ?>
							<div class="post-categories">
								<?php the_category( ' ' ); ?>
							</div>
						<?php endif; ?>

						<a href="<?php the_permalink(); ?>">
							<?php
							$full_image_size = get_the_post_thumbnail_url( null, 'full' );
							Hdia_Helper::get_lazy_load_image( array(
								'url'    => $full_image_size,
								'width'  => 470,
								'height' => 298,
								'crop'   => true,
								'echo'   => true,
								'alt'    => get_the_title(),
							) );
							?>
						</a>
					</div>
				</div>
			<?php } ?>

			<div class="post-info">
				<div class="post-meta">
					<div class="post-date">
						<span class="meta-icon"><i class="far fa-calendar"></i></span>
						<?php echo get_the_date(); ?>
					</div>

					<?php if ( class_exists( 'InsightCore_View' ) ) : ?>
						<div class="post-view">
						<span class="meta-icon">
							<i class="far fa-eye"></i>
						</span>

							<?php
							$views = InsightCore_View::get_views();
							printf( esc_html( _n( '%1$s View', '%1$s Views', $views, 'hdia' ) ), $views );
							?>
						</div>
					<?php endif; ?>
				</div>

				<?php get_template_part( 'loop/blog/title' ); ?>

				<div class="post-excerpt">
					<?php Hdia_Templates::excerpt( array(
						'limit' => 120,
						'type'  => 'character',
					) ); ?>
				</div>

				<div class="post-read-more">
					<a href="<?php the_permalink(); ?>">
						<span class="btn-text">
							<?php esc_html_e( 'Chi tiết', 'hdia' ); ?>
						</span>
						<span class="btn-icon ion-arrow-right-c"></span>
					</a>
				</div>
			</div>
		</div>
	</div>
<?php endwhile;
