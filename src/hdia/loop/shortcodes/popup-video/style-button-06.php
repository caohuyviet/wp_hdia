<?php
extract( $hdia_shortcode_atts );
?>
<a href="<?php echo esc_url( $video ); ?>">
	<div class="video-play">
		<i class="ion-ios-play"></i>
	</div>
	<div class="video-text">
		<?php echo esc_html( $video_text ); ?>
	</div>
</a>
