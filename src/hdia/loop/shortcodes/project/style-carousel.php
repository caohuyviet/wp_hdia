<?php
while ( $hdia_query->have_posts() ) :
	$hdia_query->the_post();
	$classes = array( 'project-item swiper-slide' );

	$meta               = unserialize( get_post_meta( get_the_ID(), 'insight_project_options', true ) );
	$project_vi_tri     = Hdia_Helper::get_the_post_meta( $meta, 'project_vi_tri', '' );
	$project_chu_dau_tu = Hdia_Helper::get_the_post_meta( $meta, 'project_chu_dau_tu', '' );
	?>
	<div <?php post_class( implode( ' ', $classes ) ); ?>>
		<div class="swiper-item">
			<a href="<?php the_permalink(); ?>">
				<div class="post-item-wrap">
					<div class="post-thumbnail-wrap">

						<div class="post-thumbnail">
							<a href="<?php the_permalink(); ?>">
								<?php if ( has_post_thumbnail() ) { ?>
									<?php
									$image_url = get_the_post_thumbnail_url( null, 'full' );

									if ( $image_size !== '' ) {
										$_sizes  = explode( 'x', $image_size );
										$_width  = $_sizes[0];
										$_height = $_sizes[1];

										Hdia_Helper::get_lazy_load_image( array(
											'url'    => $image_url,
											'width'  => $_width,
											'height' => $_height,
											'crop'   => true,
											'echo'   => true,
											'alt'    => get_the_title(),
										) );
									}
									?>
								<?php } else { ?>
									<?php Hdia_Templates::image_placeholder( 480, 480 ); ?>
								<?php } ?>
							</a>
						</div>
					</div>

					<div class="post-info">
						<h5 class="post-title">
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h5>

						<?php if ( $project_vi_tri !== '' || $project_chu_dau_tu !== '' ) { ?>
							<div class="project-info-wrap">
								<?php if ( $project_vi_tri !== '' ) { ?>
									<div class="project-info project-vi-tri">
										<i class="fal fa-map-marker-alt"></i>
										<span
											class="project-info-label"><?php echo esc_html( 'Vị trí: ', 'hdia' ) ?></span><?php echo esc_html( $project_vi_tri ); ?>
									</div>
								<?php } ?>

								<?php if ( $project_chu_dau_tu !== '' ) { ?>
									<div class="project-info project-chu-dau-tu">
										<i class="fal fa-user-tie"></i>
										<span
											class="project-info-label"><?php echo esc_html( 'Chủ đầu tư: ', 'hdia' ) ?></span><?php echo esc_html( $project_chu_dau_tu ); ?>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
					</div>
				</div>
			</a>
		</div>
	</div>
<?php endwhile;
