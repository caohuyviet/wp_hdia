<?php
extract( $hdia_shortcode_atts );
?>

<div class="swiper-container">
	<div class="swiper-wrapper">
		<?php while ( $hdia_query->have_posts() ) : $hdia_query->the_post(); ?>
			<div class="swiper-slide">
				<?php $_meta = unserialize( get_post_meta( get_the_ID(), 'insight_testimonial_options', true ) ); ?>
				<div class="testimonial-item">
					<?php
					if ( has_post_thumbnail() ) {
						$full_image_size = get_the_post_thumbnail_url( null, 'full' );
						$image_url       = Hdia_Helper::aq_resize( array(
							'url'    => $full_image_size,
							'width'  => 85,
							'height' => 85,
							'crop'   => true,
						) );
						echo '<div class="post-thumbnail"><img src="' . esc_url( $image_url ) . '" alt="' . esc_attr__( 'Slide Image', 'hdia' ) . '"/></div>';
					}
					?>
					<div class="testimonial-desc heading-color"><?php the_content(); ?></div>
					<?php if ( isset( $_meta['rating'] ) && $_meta['rating'] !== '' ): ?>
						<div class="testimonial-rating">
							<?php Hdia_Templates::get_rating_template( $_meta['rating'] ); ?>
						</div>
					<?php endif; ?>
					<div class="testimonial-main-info">
						<div class="testimonial-name">
							<?php the_title(); ?>
						</div>
						<?php if ( isset( $_meta['by_line'] ) ) : ?>
							<span class="testimonial-by-line"><?php echo esc_html( $_meta['by_line'] ); ?></span>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
</div>
