=== Hdia ===

Contributors: hdia
Tags: editor-style, featured-images, microformats, post-formats, rtl-language-support, sticky-post, threaded-comments, translation-ready

Requires at least: 4.4
Tested up to: 4.9.x
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Hdia - Industrial & Manufacturing Businesses WordPress Theme

== Description ==

Hdia brings the young and vibrant look to your website with high flexibility in customization. This is the theme of beautifully crafted pages and elements for multiple purposes.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =


== Changelog ==

= 1.1.0 - September 29, 2023 =
- Updated:
Insight Core plugin - v.2.6.5
WPBakery Page Builder plugin - v.7.0
WPBakery Page Builder (Visual Composer) Clipboard plugin - v.5.0.4
Revolution Slider plugin - v.6.6.16
- Fixed:
Compatible with PHP 8.x
Compatible with WooCommerce 8.1.0

= 1.0.2 - November 22, 2021 =
- Updated:
Revolution Slider plugin - v.6.5.9
Insight Core plugin - v.2.2.4
- Fixed:
Contact Form

= 1.0.1 - August 06 2021 =
- Updated:
Revolution Slider plugin - v.6.5.5
WPBakery Page Builder plugin - v.6.7.0
- Fixed:
Compatible with WPC Smart new plugin

= 1.0.0 - July 18 2021 =
* Initial release

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2015 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
