<?php

class WPBakeryShortCode_TM_Accordion extends WPBakeryShortCode {

	public function get_inline_css( $selector, $atts ) {
		Hdia_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Accordion', 'hdia' ),
	'base'                      => 'tm_accordion',
	'category'                  => HDIA_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-accordion',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'hdia' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Style 01', 'hdia' ) => '1',
				esc_html__( 'Style 02', 'hdia' ) => '2',
				esc_html__( 'Style 03', 'hdia' ) => '3',
			),
			'std'         => '1',
		),
		array(
			'heading'    => esc_html__( 'Multi Open', 'hdia' ),
			'type'       => 'checkbox',
			'param_name' => 'multi_open',
			'value'      => array( esc_html__( 'Yes', 'hdia' ) => '1' ),
		),
		Hdia_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Items', 'hdia' ),
			'heading'    => esc_html__( 'Items', 'hdia' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Title', 'hdia' ),
					'type'        => 'textfield',
					'param_name'  => 'title',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Content', 'hdia' ),
					'type'       => 'textarea',
					'param_name' => 'content',
				),
			),
		),
	), Hdia_VC::get_vc_spacing_tab() ),
) );
