<?php

class WPBakeryShortCode_TM_Attribute_List extends WPBakeryShortCode {

	public function get_inline_css( $selector, $atts ) {
		Hdia_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'     => esc_html__( 'Attribute List', 'hdia' ),
	'base'     => 'tm_attribute_list',
	'category' => HDIA_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-grid',
	'params'   => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'hdia' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Style 01', 'hdia' ) => '01',
			),
			'std'         => '01',
		),
		Hdia_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Attributes', 'hdia' ),
			'heading'    => esc_html__( 'Attributes', 'hdia' ),
			'type'       => 'param_group',
			'param_name' => 'attributes',
			'params'     => array(
				array(
					'type'       => 'iconpicker',
					'heading'    => esc_html__( 'Icon', 'hdia' ),
					'param_name' => 'icon_ion',
					'settings'   => array(
						'emptyIcon'    => true,
						'type'         => 'fontawesome5',
						'iconsPerPage' => 400,
					),
					'description' => esc_html__( 'Select icon from library.', 'hdia' ),
				),
				array(
					'heading'     => esc_html__( 'Name', 'hdia' ),
					'type'        => 'textfield',
					'param_name'  => 'name',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Value', 'hdia' ),
					'type'       => 'textfield',
					'param_name' => 'value',
				),
			),
		),
	), Hdia_VC::get_vc_spacing_tab() ),
) );

