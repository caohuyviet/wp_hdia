<?php

class WPBakeryShortCode_TM_Callout_Box extends WPBakeryShortCodesContainer {

}

vc_map( array(
	'name'                    => esc_html__( 'Callout Box', 'hdia' ),
	'base'                    => 'tm_callout_box',
	'content_element'         => true,
	'show_settings_on_create' => false,
	'is_container'            => true,
	'category'                => HDIA_VC_SHORTCODE_CATEGORY,
	'icon'                    => 'insight-i insight-i-contact-form-7',
	'js_view'                 => 'VcColumnView',
	'params'                  => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'hdia' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( '01', 'hdia' ) => '01',
			),
			'std'         => '01',
		),
		Hdia_VC::extra_class_field(),
	), Hdia_VC::get_custom_style_tab() ),
) );
