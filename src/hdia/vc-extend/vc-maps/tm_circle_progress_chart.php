<?php

class WPBakeryShortCode_TM_Circle_Progress_Chart extends WPBakeryShortCode {

	public function get_inline_css( $selector, $atts ) {
		global $hdia_shortcode_lg_css;

		if ( isset( $atts['number_font_size'] ) ) {
			Hdia_VC::get_responsive_css( array(
				'element' => "$selector .chart-number",
				'atts'    => array(
					'font-size' => array(
						'media_str' => $atts['number_font_size'],
						'unit'      => 'px',
					),
				),
			) );
		}

		$icon_tmp      = Hdia_Helper::get_shortcode_css_color_inherit( 'color', $atts['icon_color'], $atts['custom_icon_color'] );
		$title_tmp     = Hdia_Helper::get_shortcode_css_color_inherit( 'color', $atts['title_color'], $atts['custom_title_color'] );
		$sub_title_tmp = Hdia_Helper::get_shortcode_css_color_inherit( 'color', $atts['sub_title_color'], $atts['custom_sub_title_color'] );

		if ( $icon_tmp !== '' ) {
			$hdia_shortcode_lg_css .= "$selector .chart-icon { $icon_tmp }";
		}

		if ( $title_tmp !== '' ) {
			$hdia_shortcode_lg_css .= "$selector .title { $title_tmp }";
		}

		if ( $sub_title_tmp !== '' ) {
			$hdia_shortcode_lg_css .= "$selector .subtitle { $sub_title_tmp }";
		}
	}
}

$content_group = esc_html__( 'Content', 'hdia' );
$style_group   = esc_html__( 'Styling', 'hdia' );

vc_map( array(
	'name'                      => esc_html__( 'Circle Progress Chart', 'hdia' ),
	'base'                      => 'tm_circle_progress_chart',
	'category'                  => HDIA_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-pie-chart',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'hdia' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Style 01', 'hdia' ) => '01',
				esc_html__( 'Style 02', 'hdia' ) => '02',
			),
			'std'         => '01',
		),
		array(
			'heading'     => esc_html__( 'Number', 'hdia' ),
			'description' => esc_html__( 'Controls the number you would like to display in pie chart.', 'hdia' ),
			'type'        => 'number',
			'param_name'  => 'number',
			'min'         => 1,
			'max'         => 100,
			'std'         => 75,
		),
		array(
			'heading'     => esc_html__( 'Circle Size', 'hdia' ),
			'description' => esc_html__( 'Controls the size of the pie chart circle. Default: 220', 'hdia' ),
			'type'        => 'number',
			'param_name'  => 'size',
			'suffix'      => 'px',
			'std'         => 220,
		),
		array(
			'heading'     => esc_html__( 'Measuring unit', 'hdia' ),
			'description' => esc_html__( 'Controls the unit of chart.', 'hdia' ),
			'type'        => 'textfield',
			'param_name'  => 'unit',
			'std'         => '%',
		),
		Hdia_VC::extra_class_field(),
		array(
			'group'      => $content_group,
			'heading'    => esc_html__( 'Title', 'hdia' ),
			'type'       => 'textfield',
			'param_name' => 'title',
		),
		array(
			'group'      => $content_group,
			'heading'    => esc_html__( 'Subtitle', 'hdia' ),
			'type'       => 'textfield',
			'param_name' => 'subtitle',
		),
	), Hdia_VC::icon_libraries( array( 'allow_none' => true, ) ), array(
		array(
			'group'      => $style_group,
			'heading'    => esc_html__( 'Line Cap', 'hdia' ),
			'type'       => 'dropdown',
			'param_name' => 'line_cap',
			'value'      => array(
				esc_html__( 'Butt', 'hdia' )   => 'butt',
				esc_html__( 'Round', 'hdia' )  => 'round',
				esc_html__( 'Square', 'hdia' ) => 'square',
			),
			'std'        => 'square',
		),
		array(
			'group'       => $style_group,
			'heading'     => esc_html__( 'Line Width', 'hdia' ),
			'description' => esc_html__( 'Controls the line width of chart.', 'hdia' ),
			'type'        => 'number',
			'param_name'  => 'line_width',
			'suffix'      => 'px',
			'min'         => 1,
			'max'         => 50,
			'std'         => 6,
		),
		array(
			'group'            => $style_group,
			'heading'          => esc_html__( 'Bar Color', 'hdia' ),
			'type'             => 'dropdown',
			'param_name'       => 'bar_color',
			'value'            => array(
				esc_html__( 'Primary Color', 'hdia' )   => 'primary',
				esc_html__( 'Secondary Color', 'hdia' ) => 'secondary',
				esc_html__( 'Custom Color', 'hdia' )    => 'custom',
			),
			'std'              => 'primary',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $style_group,
			'heading'          => esc_html__( 'Custom Bar Color', 'hdia' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_bar_color',
			'dependency'       => array( 'element' => 'bar_color', 'value' => array( 'custom' ) ),
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $style_group,
			'heading'          => esc_html__( 'Track Color', 'hdia' ),
			'type'             => 'dropdown',
			'param_name'       => 'track_color',
			'value'            => array(
				esc_html__( 'Default Color', 'hdia' )   => '',
				esc_html__( 'Primary Color', 'hdia' )   => 'primary',
				esc_html__( 'Secondary Color', 'hdia' ) => 'secondary',
				esc_html__( 'Custom Color', 'hdia' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $style_group,
			'heading'          => esc_html__( 'Custom Track Color', 'hdia' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_track_color',
			'dependency'       => array( 'element' => 'track_color', 'value' => array( 'custom' ) ),
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $style_group,
			'heading'          => esc_html__( 'Icon Color', 'hdia' ),
			'type'             => 'dropdown',
			'param_name'       => 'icon_color',
			'value'            => array(
				esc_html__( 'Default Color', 'hdia' )   => '',
				esc_html__( 'Primary Color', 'hdia' )   => 'primary',
				esc_html__( 'Secondary Color', 'hdia' ) => 'secondary',
				esc_html__( 'Custom Color', 'hdia' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $style_group,
			'heading'          => esc_html__( 'Custom Icon Color', 'hdia' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_icon_color',
			'dependency'       => array( 'element' => 'icon_color', 'value' => array( 'custom' ) ),
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $style_group,
			'heading'          => esc_html__( 'Title Color', 'hdia' ),
			'type'             => 'dropdown',
			'param_name'       => 'title_color',
			'value'            => array(
				esc_html__( 'Default Color', 'hdia' )   => '',
				esc_html__( 'Primary Color', 'hdia' )   => 'primary',
				esc_html__( 'Secondary Color', 'hdia' ) => 'secondary',
				esc_html__( 'Custom Color', 'hdia' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $style_group,
			'heading'          => esc_html__( 'Custom Title Color', 'hdia' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_title_color',
			'dependency'       => array( 'element' => 'title_color', 'value' => array( 'custom' ) ),
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $style_group,
			'heading'          => esc_html__( 'Sub Title Color', 'hdia' ),
			'type'             => 'dropdown',
			'param_name'       => 'sub_title_color',
			'value'            => array(
				esc_html__( 'Default Color', 'hdia' )   => '',
				esc_html__( 'Primary Color', 'hdia' )   => 'primary',
				esc_html__( 'Secondary Color', 'hdia' ) => 'secondary',
				esc_html__( 'Custom Color', 'hdia' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $style_group,
			'heading'          => esc_html__( 'Custom Sub Title Color', 'hdia' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_sub_title_color',
			'dependency'       => array( 'element' => 'sub_title_color', 'value' => array( 'custom' ) ),
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'       => $style_group,
			'heading'     => esc_html__( 'Number Font Size', 'hdia' ),
			'type'        => 'number_responsive',
			'param_name'  => 'number_font_size',
			'min'         => 8,
			'suffix'      => 'px',
			'media_query' => array(
				'lg' => '',
				'md' => '',
				'sm' => '',
				'xs' => '',
			),
		),
	) ),
) );
