<?php

class WPBakeryShortCode_TM_Gmaps extends WPBakeryShortCode {

	public function convertAttributesToNewMarker( $atts ) {
		if ( isset( $atts['markers'] ) && strlen( $atts['markers'] ) > 0 ) {
			$markers = vc_param_group_parse_atts( $atts['markers'] );

			if ( ! is_array( $markers ) ) {
				$temp         = explode( ',', $atts['markers'] );
				$paramMarkers = array();

				foreach ( $temp as $marker ) {
					$data = explode( '|', $marker );

					$newMarker            = array();
					$newMarker['address'] = isset( $data[0] ) ? $data[0] : '';
					$newMarker['icon']    = isset( $data[1] ) ? $data[1] : '';
					$newMarker['title']   = isset( $data[2] ) ? $data[2] : '';
					$newMarker['info']    = isset( $data[3] ) ? $data[3] : '';

					$paramMarkers[] = $newMarker;
				}

				$atts['markers'] = urlencode( json_encode( $paramMarkers ) );

			}

			return $atts;
		}
	}
}

vc_map( array(
	'name'     => esc_html__( 'Google Maps', 'hdia' ),
	'base'     => 'tm_gmaps',
	'icon'     => 'insight-i insight-i-map',
	'category' => HDIA_VC_SHORTCODE_CATEGORY,
	'params'   => array(
		array(
			'heading'     => esc_html__( 'Height', 'hdia' ),
			'description' => esc_html__( 'Enter map height (in pixels or %)', 'hdia' ),
			'type'        => 'textfield',
			'param_name'  => 'map_height',
			'value'       => '480',
		),
		array(
			'heading'     => esc_html__( 'Width', 'hdia' ),
			'description' => esc_html__( 'Enter map width (in pixels or %)', 'hdia' ),
			'type'        => 'textfield',
			'param_name'  => 'map_width',
			'value'       => '100%',
		),
		array(
			'heading'    => esc_html__( 'Button', 'hdia' ),
			'type'       => 'vc_link',
			'param_name' => 'button',
			'value'      => esc_html__( 'Button', 'hdia' ),
		),
		array(
			'heading'     => esc_html__( 'Zoom Level', 'hdia' ),
			'description' => esc_html__( 'Map zoom level', 'hdia' ),
			'type'        => 'number',
			'param_name'  => 'zoom',
			'value'       => 16,
			'max'         => 17,
			'min'         => 0,
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'zoom_enable',
			'value'      => array(
				esc_html__( 'Enable mouse scroll wheel zoom', 'hdia' ) => 'yes',
			),
		),
		array(
			'heading'     => esc_html__( 'Map Type', 'hdia' ),
			'description' => esc_html__( 'Choose a map type', 'hdia' ),
			'type'        => 'dropdown',
			'admin_label' => true,
			'param_name'  => 'map_type',
			'value'       => array(
				esc_html__( 'Roadmap', 'hdia' )   => 'roadmap',
				esc_html__( 'Satellite', 'hdia' ) => 'satellite',
				esc_html__( 'Hybrid', 'hdia' )    => 'hybrid',
				esc_html__( 'Terrain', 'hdia' )   => 'terrain',
			),
		),
		array(
			'heading'     => esc_html__( 'Map Style', 'hdia' ),
			'description' => esc_html__( 'Choose a map style. This approach changes the style of the Roadmap types (base imagery in terrain and satellite views is not affected, but roads, labels, etc. respect styling rules)', 'hdia' ),
			'type'        => 'image_radio',
			'admin_label' => true,
			'param_name'  => 'map_style',
			'value'       => array(
				'default'                 => array(
					'url'   => HDIA_THEME_IMAGE_URI . '/maps/default.png',
					'title' => esc_attr__( 'Default', 'hdia' ),
				),
				'grayscale'               => array(
					'url'   => HDIA_THEME_IMAGE_URI . '/maps/greyscale.png',
					'title' => esc_attr__( 'Grayscale', 'hdia' ),
				),
				'subtle_grayscale'        => array(
					'url'   => HDIA_THEME_IMAGE_URI . '/maps/subtle-grayscale.png',
					'title' => esc_attr__( 'Subtle Grayscale', 'hdia' ),
				),
				'apple_paps_esque'        => array(
					'url'   => HDIA_THEME_IMAGE_URI . '/maps/apple-maps-esque.png',
					'title' => esc_attr__( 'Apple Maps-esque', 'hdia' ),
				),
				'pale_dawn'               => array(
					'url'   => HDIA_THEME_IMAGE_URI . '/maps/pale-dawn.png',
					'title' => esc_attr__( 'Pale Dawn', 'hdia' ),
				),
				'midnight_commander'      => array(
					'url'   => HDIA_THEME_IMAGE_URI . '/maps/midnight-commander.png',
					'title' => esc_attr__( 'Midnight Commander', 'hdia' ),
				),
				'blue_water'              => array(
					'url'   => HDIA_THEME_IMAGE_URI . '/maps/blue-water.png',
					'title' => esc_attr__( 'Blue Water', 'hdia' ),
				),
				'retro'                   => array(
					'url'   => HDIA_THEME_IMAGE_URI . '/maps/retro.png',
					'title' => esc_attr__( 'Retro', 'hdia' ),
				),
				'paper'                   => array(
					'url'   => HDIA_THEME_IMAGE_URI . '/maps/paper.png',
					'title' => esc_attr__( 'Paper', 'hdia' ),
				),
				'ultra_light_with_labels' => array(
					'url'   => HDIA_THEME_IMAGE_URI . '/maps/ultra-light-with-labels.png',
					'title' => esc_attr__( 'Ultra Light with Labels', 'hdia' ),
				),
				'shades_of_grey'          => array(
					'url'   => HDIA_THEME_IMAGE_URI . '/maps/shades-of-grey.png',
					'title' => esc_attr__( 'Shades Of Grey', 'hdia' ),
				),
			),
			'std'         => 'default',
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'overlay_enable',
			'value'      => array(
				esc_html__( 'Use overlay instead of marker items', 'hdia' ) => '1',
			),
		),
		array(
			'heading'    => esc_html__( 'Overlay Style', 'hdia' ),
			'type'       => 'dropdown',
			'param_name' => 'overlay_style',
			'value'      => array(
				esc_html__( 'Style 01', 'hdia' ) => '01',
				esc_html__( 'Style 02', 'hdia' ) => '02',
			),
			'std'        => '01',
		),
		array(
			'group'       => esc_html__( 'Markers', 'hdia' ),
			'heading'     => esc_html__( 'Markers', 'hdia' ),
			'description' => esc_html__( 'You can add multiple markers to the map', 'hdia' ),
			'type'        => 'param_group',
			'param_name'  => 'markers',
			'value'       => urlencode( json_encode( array(
				array(
					'address' => '40.7590615,-73.969231',
				),
			) ) ),
			'params'      => array(
				array(
					'heading'     => esc_html__( 'Address or Coordinate', 'hdia' ),
					'description' => sprintf( wp_kses( __( 'Enter address or coordinate. Find coordinates using the name and/or address of the place using <a href="%s" target="_blank">this simple tool here.</a>', 'hdia' ), array(
						'a' => array(
							'href'   => array(),
							'target' => array(),
						),
					) ), esc_url( 'http://universimmedia.pagesperso-orange.fr/geo/loc.htm' ) ),
					'type'        => 'textfield',
					'param_name'  => 'address',
					'admin_label' => true,
				),
				array(
					'heading'     => esc_html__( 'Marker icon', 'hdia' ),
					'description' => esc_html__( 'Choose a image for marker address', 'hdia' ),
					'type'        => 'attach_image',
					'param_name'  => 'icon',
				),
				array(
					'heading'    => esc_html__( 'Marker Title', 'hdia' ),
					'type'       => 'textfield',
					'param_name' => 'title',
				),
				array(
					'heading'     => esc_html__( 'Marker Information', 'hdia' ),
					'description' => esc_html__( 'Content for info window', 'hdia' ),
					'type'        => 'textarea',
					'param_name'  => 'info',
				),
			),
		),
	),
) );
