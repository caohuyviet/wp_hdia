<?php

class WPBakeryShortCode_TM_Image extends WPBakeryShortCode {

	public function get_inline_css( $selector, $atts ) {
		global $hdia_shortcode_lg_css;
		global $hdia_shortcode_md_css;
		global $hdia_shortcode_sm_css;
		global $hdia_shortcode_xs_css;
		$tmp = $image_tmp = '';

		$tmp .= "text-align: {$atts['align']}";

		if ( $tmp !== '' ) {
			$hdia_shortcode_lg_css .= "$selector{ $tmp }";
		}

		if ( $atts['md_align'] !== '' ) {
			$hdia_shortcode_md_css .= "$selector { text-align: {$atts['md_align']} }";
		}

		if ( $atts['sm_align'] !== '' ) {
			$hdia_shortcode_sm_css .= "$selector { text-align: {$atts['sm_align']} }";
		}

		if ( $atts['xs_align'] !== '' ) {
			$hdia_shortcode_xs_css .= "$selector { text-align: {$atts['xs_align']} }";
		}

		if ( $atts['rounded'] !== '' ) {
			$image_tmp .= Hdia_Helper::get_css_prefix( 'border-radius', "{$atts['rounded']}px" );
		}

		if ( $atts['box_shadow'] !== '' ) {
			$image_tmp .= Hdia_Helper::get_css_prefix( 'box-shadow', $atts['box_shadow'] );
		}

		if ( $atts['full_wide'] === '1' ) {
			$image_tmp .= "width: 100%;";
		}

		if ( $image_tmp !== '' ) {
			$hdia_shortcode_lg_css .= "$selector img { $image_tmp }";
		}

		Hdia_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$styling_tab = esc_html__( 'Styling', 'hdia' );

vc_map( array(
	'name'                      => esc_html__( 'Single Image', 'hdia' ),
	'base'                      => 'tm_image',
	'category'                  => HDIA_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-singleimage',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Image', 'hdia' ),
			'type'        => 'attach_image',
			'param_name'  => 'image',
			'admin_label' => true
		),
		array(
			'heading'     => esc_html__( 'Image Size', 'hdia' ),
			'description' => esc_html__( 'Controls the size of image.', 'hdia' ),
			'type'        => 'dropdown',
			'param_name'  => 'image_size',
			'value'       => array(
				esc_html__( 'Full', 'hdia' )   => 'full',
				esc_html__( 'Custom', 'hdia' ) => 'custom',
			),
			'std'         => 'full',
		),
		array(
			'heading'          => esc_html__( 'Image Width', 'hdia' ),
			'type'             => 'number',
			'param_name'       => 'image_size_width',
			'min'              => 0,
			'max'              => 1920,
			'step'             => 10,
			'suffix'           => 'px',
			'dependency'       => array(
				'element' => 'image_size',
				'value'   => array( 'custom' ),
			),
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'heading'          => esc_html__( 'Image Height', 'hdia' ),
			'type'             => 'number',
			'param_name'       => 'image_size_height',
			'min'              => 0,
			'max'              => 1920,
			'step'             => 10,
			'suffix'           => 'px',
			'dependency'       => array(
				'element' => 'image_size',
				'value'   => array( 'custom' ),
			),
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'heading'    => esc_html__( 'On Click Action', 'hdia' ),
			'desc'       => esc_html__( 'Select action for click action.', 'hdia' ),
			'type'       => 'dropdown',
			'param_name' => 'action',
			'value'      => array(
				esc_html__( 'None', 'hdia' )             => '',
				esc_html__( 'Open Popup', 'hdia' )       => 'popup',
				esc_html__( 'Open Custom Link', 'hdia' ) => 'custom_link',
				esc_html__( 'Return To Home', 'hdia' )   => 'go_to_home',
			),
			'std'        => '',
		),
		array(
			'heading'     => esc_html__( 'Link', 'hdia' ),
			'description' => esc_html__( 'Add a link to image.', 'hdia' ),
			'type'        => 'vc_link',
			'param_name'  => 'custom_link',
			'dependency'  => array(
				'element' => 'action',
				'value'   => 'custom_link',
			),
		),
		array(
			'heading'     => esc_html__( 'Full Width', 'hdia' ),
			'description' => esc_html__( 'Make image fit wide of container', 'hdia' ),
			'type'        => 'checkbox',
			'param_name'  => 'full_wide',
			'value'       => array( esc_html__( 'Yes', 'hdia' ) => '1' ),
		),
		array(
			'group'       => $styling_tab,
			'heading'     => esc_html__( 'Rounded', 'hdia' ),
			'description' => esc_html__( 'Controls the rounded of image', 'hdia' ),
			'type'        => 'number',
			'param_name'  => 'rounded',
			'min'         => 0,
			'max'         => 100,
			'step'        => 1,
			'suffix'      => 'px',
		),
		array(
			'group'       => $styling_tab,
			'heading'     => esc_html__( 'Box Shadow', 'hdia' ),
			'description' => esc_html__( 'E.g 0 20px 30px #ccc', 'hdia' ),
			'type'        => 'textfield',
			'param_name'  => 'box_shadow',
		),
		array(
			'heading'     => esc_html__( 'Caption', 'hdia' ),
			'type'        => 'textfield',
			'param_name'  => 'caption',
			'admin_label' => true,
		),
	), Hdia_VC::get_alignment_fields(), array(
		Hdia_VC::get_animation_field(),
		Hdia_VC::extra_class_field(),
	), Hdia_VC::get_vc_spacing_tab() ),

) );
