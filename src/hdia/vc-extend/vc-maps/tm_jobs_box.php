<?php

class WPBakeryShortCode_TM_Jobs_Box extends WPBakeryShortCode {
	public function get_inline_css( $selector, $atts ) {
		Hdia_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'     => esc_html__( 'Jobs Box', 'hdia' ),
	'base'     => 'tm_jobs_box',
	'category' => HDIA_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-info-boxes',
	'params'   => array_merge( array(
		array(
			'heading'     => esc_html__( 'Title', 'hdia' ),
			'type'        => 'textfield',
			'param_name'  => 'title',
			'admin_label' => true,
			'std'         => '',
		),
		array(
			'heading'    => esc_html__( 'Link', 'hdia' ),
			'type'       => 'vc_link',
			'param_name' => 'link',
		),
		Hdia_VC::get_animation_field(),
		Hdia_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Jobs', 'hdia' ),
			'heading'    => esc_html__( 'Jobs', 'hdia' ),
			'type'       => 'param_group',
			'param_name' => 'jobs',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Title', 'hdia' ),
					'type'        => 'textfield',
					'param_name'  => 'title',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Text', 'hdia' ),
					'type'       => 'textfield',
					'param_name' => 'text',
				),
				array(
					'heading'    => esc_html__( 'Link', 'hdia' ),
					'type'       => 'vc_link',
					'param_name' => 'link',
				),
			),
		),
	), Hdia_VC::get_vc_spacing_tab() ),
) );

