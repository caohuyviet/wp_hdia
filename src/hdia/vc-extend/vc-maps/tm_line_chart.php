<?php

class WPBakeryShortCode_TM_Line_Chart extends WPBakeryShortCode {

	public function get_inline_css( $selector, $atts ) {
		Hdia_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$legend_tab = esc_html__( 'Tooltips and Legends', 'hdia' );

vc_map( array(
	'name'                      => esc_html__( 'Line Chart', 'hdia' ),
	'base'                      => 'tm_line_chart',
	'category'                  => HDIA_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-accordion',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'X axis labels', 'hdia' ),
			'description' => esc_html__( 'List of labels for X axis (separate labels with ";").', 'hdia' ),
			'type'        => 'textfield',
			'param_name'  => 'labels',
			'std'         => 'Jul; Aug; Sep; Oct; Nov; Dec',
		),
		array(
			'heading'    => esc_html__( 'Datasets', 'hdia' ),
			'type'       => 'param_group',
			'param_name' => 'datasets',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Title', 'hdia' ),
					'description' => esc_html__( 'Dataset title used in tooltips and legends.', 'hdia' ),
					'type'        => 'textfield',
					'param_name'  => 'title',
					'admin_label' => true,
				),
				array(
					'heading'     => esc_html__( 'Values', 'hdia' ),
					'description' => esc_html__( 'text format for the tooltip (available placeholders: {d} dataset title, {x} X axis label, {y} Y axis value)', 'hdia' ),
					'type'        => 'textfield',
					'param_name'  => 'values',
				),
				array(
					'heading'    => esc_html__( 'Dataset Color', 'hdia' ),
					'type'       => 'colorpicker',
					'param_name' => 'color',
				),
				array(
					'heading'     => esc_html__( 'Area filling', 'hdia' ),
					'description' => esc_html__( 'How to fill the area below the line', 'hdia' ),
					'type'        => 'dropdown',
					'param_name'  => 'fill',
					'value'       => array(
						esc_html__( 'Custom', 'hdia' ) => 'custom',
						esc_html__( 'None', 'hdia' )   => 'none',
					),
					'std'         => 'none',
				),
				array(
					'heading'    => esc_html__( 'Fill Color', 'hdia' ),
					'type'       => 'colorpicker',
					'param_name' => 'fill_color',
					'dependency' => array(
						'element' => 'fill',
						'value'   => array( 'custom' ),
					),
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'point_style',
					'heading'    => esc_html__( 'Point Style', 'hdia' ),
					'value'      => array(
						esc_html__( 'none', 'hdia' )              => 'none',
						esc_html__( 'circle', 'hdia' )            => 'circle',
						esc_html__( 'triangle', 'hdia' )          => 'triangle',
						esc_html__( 'rectangle', 'hdia' )         => 'rect',
						esc_html__( 'rotated rectangle', 'hdia' ) => 'rectRot',
						esc_html__( 'cross', 'hdia' )             => 'cross',
						esc_html__( 'rotated cross', 'hdia' )     => 'crossRot',
						esc_html__( 'star', 'hdia' )              => 'star',
					),
					'std'        => 'circle',
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'line_type',
					'heading'    => esc_html__( 'Line type', 'hdia' ),
					'value'      => array(
						esc_html__( 'normal', 'hdia' )  => 'normal',
						esc_html__( 'stepped', 'hdia' ) => 'step',
					),
					'std'        => 'normal',
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'line_style',
					'heading'    => esc_html__( 'Line style', 'hdia' ),
					'value'      => array(
						esc_html__( 'solid', 'hdia' )  => 'solid',
						esc_html__( 'dashed', 'hdia' ) => 'dashed',
						esc_html__( 'dotted', 'hdia' ) => 'dotted',
					),
					'std'        => 'solid',
				),
				array(
					'heading'     => esc_html__( 'Thickness', 'hdia' ),
					'description' => esc_html__( 'line and points thickness', 'hdia' ),
					'type'        => 'dropdown',
					'param_name'  => 'thickness',
					'value'       => array(
						esc_html__( 'thin', 'hdia' )    => 'thin',
						esc_html__( 'normal', 'hdia' )  => 'normal',
						esc_html__( 'thick', 'hdia' )   => 'thick',
						esc_html__( 'thicker', 'hdia' ) => 'thicker',
					),
					'std'         => 'normal',
				),
				array(
					'heading'     => esc_html__( 'Line tension', 'hdia' ),
					'description' => esc_html__( 'tension of the line ( 100 for a straight line )', 'hdia' ),
					'type'        => 'number',
					'param_name'  => 'line_tension',
					'std'         => 10,
					'min'         => 0,
					'max'         => 100,
					'step'        => 1,
				),
			),
			'value'      => rawurlencode( wp_json_encode( array(
				array(
					'title'        => esc_html__( 'Item 01', 'hdia' ),
					'values'       => '15; 10; 22; 19; 23; 17',
					'color'        => 'rgba(105, 59, 255, 0.55)',
					'fill'         => 'none',
					'thickness'    => 'normal',
					'point_style'  => 'circle',
					'line_style'   => 'solid',
					'line_tension' => 10,

				),
				array(
					'title'        => esc_html__( 'Item 02', 'hdia' ),
					'values'       => '34; 38; 35; 33; 37; 40',
					'color'        => 'rgba(0, 110, 253, 0.56)',
					'fill'         => 'none',
					'thickness'    => 'normal',
					'point_style'  => 'circle',
					'line_style'   => 'solid',
					'line_tension' => 10,
				),
			) ) ),
		),
		Hdia_VC::extra_class_field(),
		array(
			'group'      => $legend_tab,
			'heading'    => esc_html__( 'Enable legends', 'hdia' ),
			'type'       => 'checkbox',
			'param_name' => 'legend',
			'value'      => array(
				esc_html__( 'Yes', 'hdia' ) => '1',
			),
			'std'        => '1',
		),
		array(
			'group'      => $legend_tab,
			'heading'    => esc_html__( 'Legends Style', 'hdia' ),
			'type'       => 'dropdown',
			'param_name' => 'legend_style',
			'value'      => array(
				esc_html__( 'Normal', 'hdia' )          => 'normal',
				esc_html__( 'Use Point Style', 'hdia' ) => 'point',
			),
			'std'        => 'normal',
		),
		array(
			'group'      => $legend_tab,
			'heading'    => esc_html__( 'Legends Position', 'hdia' ),
			'type'       => 'dropdown',
			'param_name' => 'legend_position',
			'value'      => array(
				esc_html__( 'Top', 'hdia' )    => 'top',
				esc_html__( 'Right', 'hdia' )  => 'right',
				esc_html__( 'Bottom', 'hdia' ) => 'bottom',
				esc_html__( 'Left', 'hdia' )   => 'left',
			),
			'std'        => 'bottom',
		),
		array(
			'group'       => $legend_tab,
			'heading'     => esc_html__( 'Click on legends', 'hdia' ),
			'description' => esc_html__( 'Hide dataset on click on legend', 'hdia' ),
			'type'        => 'checkbox',
			'param_name'  => 'legend_onclick',
			'value'       => array(
				esc_html__( 'Yes', 'hdia' ) => '1',
			),
			'std'         => '1',
		),
		array(
			'group'      => esc_html__( 'Chart Options', 'hdia' ),
			'heading'    => esc_html__( 'Aspect Ratio', 'hdia' ),
			'type'       => 'dropdown',
			'param_name' => 'aspect_ratio',
			'value'      => array(
				'1:1'  => '1:1',
				'21:9' => '21:9',
				'16:9' => '16:9',
				'4:3'  => '4:3',
				'3:4'  => '3:4',
				'9:16' => '9:16',
				'9:21' => '9:21',
			),
			'std'        => '4:3',
		),
	), Hdia_VC::get_vc_spacing_tab() ),
) );
