<?php

class WPBakeryShortCode_TM_Mailchimp_Form extends WPBakeryShortCode {

}

vc_map( array(
	'name'                      => esc_html__( 'Mailchimp Form', 'hdia' ),
	'base'                      => 'tm_mailchimp_form',
	'category'                  => HDIA_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-mailchimp-form',
	'allowed_container_element' => 'vc_row',
	'params'                    => array(
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Widget title', 'hdia' ),
			'param_name'  => 'title',
			'description' => esc_html__( 'What text use as a widget title. Leave blank to use default widget title.', 'hdia' ),
		),
		array(
			'heading'     => esc_html__( 'Form ID', 'hdia' ),
			'description' => esc_html__( 'Input the id of form. Leave blank to show default form.', 'hdia' ),
			'type'        => 'textfield',
			'param_name'  => 'form_id',
		),
		array(
			'heading'     => esc_html__( 'Style', 'hdia' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( '1', 'hdia' )  => '1',
				esc_html__( '2', 'hdia' )  => '2',
				esc_html__( '3', 'hdia' )  => '3',
				esc_html__( '4', 'hdia' )  => '4',
				esc_html__( '5', 'hdia' )  => '5',
				esc_html__( '6', 'hdia' )  => '6',
				esc_html__( '7', 'hdia' )  => '7',
				esc_html__( '8', 'hdia' )  => '8',
				esc_html__( '9', 'hdia' )  => '9',
				esc_html__( '10', 'hdia' ) => '10',
				esc_html__( '11', 'hdia' ) => '11',
			),
			'std'         => '1',
		),
		Hdia_VC::extra_class_field(),
	),
) );
