<?php

class WPBakeryShortCode_TM_Office_Info extends WPBakeryShortCode {
}

vc_map( array(
	'name'     => esc_html__( 'Office Info', 'hdia' ),
	'base'     => 'tm_office_info',
	'category' => HDIA_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-info-boxes',
	'params'   => array(
		array(
			'heading'     => esc_html__( 'Image', 'hdia' ),
			'type'        => 'attach_image',
			'param_name'  => 'image',
			'admin_label' => true,
		),
		array(
			'heading'     => esc_html__( 'Title', 'hdia' ),
			'type'        => 'textfield',
			'param_name'  => 'title',
			'admin_label' => true,
		),
		array(
			'heading'     => esc_html__( 'Address', 'hdia' ),
			'type'        => 'textfield',
			'param_name'  => 'address',
			'admin_label' => true,
		),
		array(
			'heading'     => esc_html__( 'Email', 'hdia' ),
			'type'        => 'textfield',
			'param_name'  => 'email',
			'admin_label' => true,
		),
		array(
			'heading'     => esc_html__( 'Phone', 'hdia' ),
			'type'        => 'textfield',
			'param_name'  => 'phone',
			'admin_label' => true,
		),
		array(
			'heading'    => esc_html__( 'Link', 'hdia' ),
			'type'       => 'vc_link',
			'param_name' => 'link',
		),
		Hdia_VC::extra_class_field(),
		Hdia_VC::get_animation_field()
	)
) );

