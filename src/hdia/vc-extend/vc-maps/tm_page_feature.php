<?php
add_filter( 'vc_autocomplete_tm_page_feature_items_page_callback', 'hdia_tm_page_feature_page_field_callback', 10, 1 );

add_filter( 'vc_autocomplete_tm_page_feature_items_page_render', 'hdia_tm_page_feature_page_field_render', 10, 1 );

function hdia_tm_page_feature_page_field_render( $term ) {
	$args = array(
		'post_type'      => 'page',
		'posts_per_page' => - 1,
		'post_status'    => 'publish',
		'name'           => $term['value'],
	);

	$query = new WP_Query( $args );
	$data  = false;
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) :
			$query->the_post();
			global $post;

			$data = array(
				'label' => get_the_title(),
				'value' => $post->post_name,
			);
		endwhile;
		wp_reset_postdata();
	}

	return $data;
}

function hdia_tm_page_feature_page_field_callback( $search_string ) {
	$data = array();
	$args = array(
		'post_type'      => 'page',
		'posts_per_page' => - 1,
		'post_status'    => 'publish',
		's'              => $search_string,
	);

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) :
			$query->the_post();
			global $post;

			$data[] = array(
				'label' => get_the_title(),
				'value' => $post->post_name,
			);
		endwhile;
		wp_reset_postdata();
	}

	return $data;
}

class WPBakeryShortCode_TM_Page_Feature extends WPBakeryShortCode {

	public function get_inline_css( $selector, $atts ) {
		Hdia_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'     => esc_html__( 'Page Feature', 'hdia' ),
	'base'     => 'tm_page_feature',
	'category' => HDIA_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-grid',
	'params'   => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'hdia' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Style 01', 'hdia' ) => '01',
			),
			'std'         => '01',
		),
		Hdia_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Items', 'hdia' ),
			'heading'    => esc_html__( 'Items', 'hdia' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Page', 'hdia' ),
					'type'        => 'autocomplete',
					'param_name'  => 'page',
					'admin_label' => true,
				),
				array(
					'type'        => 'iconpicker',
					'heading'     => esc_html__( 'Icon', 'hdia' ),
					'param_name'  => 'icon_icomoon',
					'value'       => 'icomoon-alarm-clock',
					'settings'    => array(
						'emptyIcon'    => true,
						'type'         => 'icomoon',
						'iconsPerPage' => 400,
					),
					'description' => esc_html__( 'Select icon from library.', 'hdia' ),
				),
			),
		),
	), Hdia_VC::get_vc_spacing_tab(), Hdia_VC::get_custom_style_tab() ),
) );

