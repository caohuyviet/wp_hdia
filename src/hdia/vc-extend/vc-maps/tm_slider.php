<?php

class WPBakeryShortCode_TM_Slider extends WPBakeryShortCode {

	public function get_inline_css( $selector, $atts ) {
		global $hdia_shortcode_lg_css;
		$slide_tmp = '';

		if ( isset( $atts['text_align'] ) && $atts['text_align'] !== '' ) {
			$slide_tmp .= "text-align: {$atts['text_align']};";
		}

		if ( $slide_tmp !== '' ) {
			$hdia_shortcode_lg_css .= "$selector .swiper-slide { $slide_tmp }";
		}

		if ( $atts['style'] === '3' ) {
			if ( isset( $atts['item_width'] ) ) {
				Hdia_VC::get_responsive_css( array(
					'element' => "$selector .swiper-slide",
					'atts'    => array(
						'width' => array(
							'media_str' => $atts['item_width'],
							'unit'      => 'px',
						),
					),
				) );
			}
		}

		Hdia_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$slides_tab  = esc_html__( 'Slides', 'hdia' );
$styling_tab = esc_html__( 'Styling', 'hdia' );

vc_map( array(
	'name'                      => esc_html__( 'Slider', 'hdia' ),
	'base'                      => 'tm_slider',
	'category'                  => HDIA_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-carousel',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'hdia' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'value'       => array(
				esc_html__( 'Carousel 01', 'hdia' )                    => '1',
				esc_html__( 'Carousel 02', 'hdia' )                    => '2',
				esc_html__( 'Carousel 03 - Auto Items', 'hdia' )       => '3',
				esc_html__( 'Carousel 04 - Modern Full Wide', 'hdia' ) => '4',
				esc_html__( 'Carousel 05', 'hdia' )                    => '5',
			),
			'std'         => '1',
			'admin_label' => true
		),
		array(
			'heading'     => esc_html__( 'Items Display', 'hdia' ),
			'type'        => 'number_responsive',
			'param_name'  => 'item_width',
			'min'         => 100,
			'max'         => 1000,
			'step'        => 10,
			'suffix'      => 'px',
			'media_query' => array(
				'lg' => 370,
				'md' => '',
				'sm' => '',
				'xs' => '',
			),
			'dependency'  => array(
				'element' => 'style',
				'value'   => array(
					'3',
				),
			),
		),
		array(
			'heading'    => esc_html__( 'Centered', 'hdia' ),
			'type'       => 'checkbox',
			'param_name' => 'centered',
			'value'      => array( esc_html__( 'Yes', 'hdia' ) => '1' ),
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'3',
					'4',
				),
			),
		),
		array(
			'heading'    => esc_html__( 'Image Size', 'hdia' ),
			'type'       => 'dropdown',
			'param_name' => 'image_size',
			'value'      => array(
				esc_html__( '1170x560 (1 Column)', 'hdia' ) => '1170x560',
				esc_html__( '600x400 (1 Column)', 'hdia' )  => '600x400',
				esc_html__( '500x338 (3 Columns)', 'hdia' ) => '500x338',
				esc_html__( '500x676 (3 Columns)', 'hdia' ) => '500x676',
				esc_html__( 'Full', 'hdia' )                => 'full',
			),
			'std'        => '500x338',
		),
		array(
			'heading'    => esc_html__( 'Auto Height', 'hdia' ),
			'type'       => 'checkbox',
			'param_name' => 'auto_height',
			'value'      => array( esc_html__( 'Yes', 'hdia' ) => '1' ),
			'std'        => '1',
		),
		array(
			'heading'    => esc_html__( 'Loop', 'hdia' ),
			'type'       => 'checkbox',
			'param_name' => 'loop',
			'value'      => array( esc_html__( 'Yes', 'hdia' ) => '1' ),
			'std'        => '1',
		),
		array(
			'heading'     => esc_html__( 'Auto Play', 'hdia' ),
			'description' => esc_html__( 'Delay between transitions (in ms), e.g 3000. Leave blank to disabled.', 'hdia' ),
			'type'        => 'number',
			'suffix'      => 'ms',
			'param_name'  => 'auto_play',
		),
		array(
			'heading'    => esc_html__( 'Equal Height', 'hdia' ),
			'type'       => 'checkbox',
			'param_name' => 'equal_height',
			'value'      => array( esc_html__( 'Yes', 'hdia' ) => '1' ),
		),
		array(
			'heading'    => esc_html__( 'Vertically Center', 'hdia' ),
			'type'       => 'checkbox',
			'param_name' => 'v_center',
			'value'      => array( esc_html__( 'Yes', 'hdia' ) => '1' ),
		),
		array(
			'heading'    => esc_html__( 'Navigation', 'hdia' ),
			'type'       => 'dropdown',
			'param_name' => 'nav',
			'value'      => Hdia_VC::get_slider_navs(),
			'std'        => '',
		),
		Hdia_VC::extra_id_field( array(
			'heading'    => esc_html__( 'Slider Button ID', 'hdia' ),
			'param_name' => 'slider_button_id',
			'dependency' => array(
				'element' => 'nav',
				'value'   => array(
					'custom',
				),
			),
		) ),
		array(
			'heading'    => esc_html__( 'Pagination', 'hdia' ),
			'type'       => 'dropdown',
			'param_name' => 'pagination',
			'value'      => Hdia_VC::get_slider_dots(),
			'std'        => '',
		),
		array(
			'heading'     => esc_html__( 'Items Display', 'hdia' ),
			'type'        => 'number_responsive',
			'param_name'  => 'items_display',
			'min'         => 1,
			'max'         => 10,
			'suffix'      => 'item (s)',
			'media_query' => array(
				'lg' => 3,
				'md' => 3,
				'sm' => 2,
				'xs' => 1,
			),
			'dependency'  => array(
				'element' => 'style',
				'value'   => array(
					'1',
					'2',
					'5',
				),
			),
		),
		array(
			'heading'    => esc_html__( 'Gutter', 'hdia' ),
			'type'       => 'number',
			'param_name' => 'gutter',
			'std'        => 30,
			'min'        => 0,
			'max'        => 50,
			'step'       => 1,
			'suffix'     => 'px',
		),
		array(
			'heading'    => esc_html__( 'Full-width Image', 'hdia' ),
			'type'       => 'checkbox',
			'param_name' => 'fw_image',
			'value'      => array( esc_html__( 'Yes', 'hdia' ) => '1' ),
		),
		Hdia_VC::extra_class_field(),
		array(
			'group'      => $slides_tab,
			'heading'    => esc_html__( 'Slides', 'hdia' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Image', 'hdia' ),
					'type'        => 'attach_image',
					'param_name'  => 'image',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Sub Title', 'hdia' ),
					'type'       => 'textfield',
					'param_name' => 'sub_title',
				),
				array(
					'heading'     => esc_html__( 'Title', 'hdia' ),
					'type'        => 'textfield',
					'param_name'  => 'title',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Text', 'hdia' ),
					'type'       => 'textarea',
					'param_name' => 'text',
				),
				array(
					'heading'    => esc_html__( 'Link', 'hdia' ),
					'type'       => 'vc_link',
					'param_name' => 'link',
				),
			),
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Text Align', 'hdia' ),
			'type'       => 'dropdown',
			'param_name' => 'text_align',
			'value'      => array(
				esc_html__( 'Default', 'hdia' ) => '',
				esc_html__( 'Left', 'hdia' )    => 'left',
				esc_html__( 'Center', 'hdia' )  => 'center',
				esc_html__( 'Right', 'hdia' )   => 'right',
				esc_html__( 'Justify', 'hdia' ) => 'justify',
			),
			'std'        => '',

		),
	), Hdia_VC::get_vc_spacing_tab() ),
) );
