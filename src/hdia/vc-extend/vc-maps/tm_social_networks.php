<?php

class WPBakeryShortCode_TM_Social_Networks extends WPBakeryShortCode {

	public function get_inline_css( $selector, $atts ) {
		global $hdia_shortcode_lg_css;
		global $hdia_shortcode_md_css;
		global $hdia_shortcode_sm_css;
		global $hdia_shortcode_xs_css;

		$tmp = $link_css = $link_hover_css = $icon_css = $icon_hover_css = $text_css = $text_hover_css = '';
		extract( $atts );

		$icon_css .= Hdia_Helper::get_shortcode_css_color_inherit( 'color', $atts['icon_color'], $atts['custom_icon_color'] );
		$icon_hover_css .= Hdia_Helper::get_shortcode_css_color_inherit( 'color', $atts['icon_hover_color'], $atts['custom_icon_hover_color'] );
		$text_css .= Hdia_Helper::get_shortcode_css_color_inherit( 'color', $atts['text_color'], $atts['custom_text_color'] );
		$text_hover_css .= Hdia_Helper::get_shortcode_css_color_inherit( 'color', $atts['text_hover_color'], $atts['custom_text_hover_color'] );
		$link_css .= Hdia_Helper::get_shortcode_css_color_inherit( 'border-color', $atts['border_color'], $atts['custom_border_color'] );
		$link_hover_css .= Hdia_Helper::get_shortcode_css_color_inherit( 'border-color', $atts['border_hover_color'], $atts['custom_border_hover_color'] );
		$link_css .= Hdia_Helper::get_shortcode_css_color_inherit( 'background-color', $atts['background_color'], $atts['custom_background_color'] );
		$link_hover_css .= Hdia_Helper::get_shortcode_css_color_inherit( 'background-color', $atts['background_hover_color'], $atts['custom_background_hover_color'] );

		if ( $atts['align'] !== '' ) {
			$tmp .= "text-align: {$atts['align']};";
		}

		if ( $atts['md_align'] !== '' ) {
			$hdia_shortcode_md_css .= "$selector { text-align: {$atts['md_align']} }";
		}

		if ( $atts['sm_align'] !== '' ) {
			$hdia_shortcode_sm_css .= "$selector { text-align: {$atts['sm_align']} }";
		}

		if ( $atts['xs_align'] !== '' ) {
			$hdia_shortcode_xs_css .= "$selector { text-align: {$atts['xs_align']} }";
		}

		if ( $tmp !== '' ) {
			$hdia_shortcode_lg_css .= "$selector { $tmp }";
		}

		if ( $icon_css !== '' ) {
			$hdia_shortcode_lg_css .= "$selector .link-icon { $icon_css }";
		}

		if ( $icon_hover_css !== '' ) {
			$hdia_shortcode_lg_css .= "$selector .item:hover .link-icon { $icon_hover_css }";
		}

		if ( $text_css !== '' ) {
			$hdia_shortcode_lg_css .= "$selector .link-text { $text_css }";
		}

		if ( $text_hover_css !== '' ) {
			$hdia_shortcode_lg_css .= "$selector .item:hover .link-text { $text_hover_css }";
		}

		if ( $link_css !== '' ) {
			$hdia_shortcode_lg_css .= "$selector .link { $link_css }";
		}

		if ( $link_hover_css !== '' ) {
			$hdia_shortcode_lg_css .= "$selector .item:hover .link { $link_hover_css }";
		}
	}
}

$color_tab = esc_html__( 'Color', 'hdia' );

vc_map( array(
	'name'                      => esc_html__( 'Social Networks', 'hdia' ),
	'base'                      => 'tm_social_networks',
	'category'                  => HDIA_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-social-networks',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'hdia' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Icons', 'hdia' )                 => 'icons',
				esc_html__( 'Large Icons', 'hdia' )           => 'large-icons',
				esc_html__( 'Solid Square Icon', 'hdia' )     => 'solid-square-icon',
				esc_html__( 'Solid Rounded Icon', 'hdia' )    => 'solid-rounded-icon',
				esc_html__( 'Solid Rounded Icon 02', 'hdia' ) => 'solid-rounded-icon-02',
				esc_html__( 'Title', 'hdia' )                 => 'title',
				esc_html__( 'Icon + Title', 'hdia' )          => 'icon-title',
				esc_html__( 'Rounded Icon + Title', 'hdia' )  => 'rounded-icon-title',
			),
			'std'         => 'icons',
		),
		array(
			'heading'     => esc_html__( 'Layout', 'hdia' ),
			'type'        => 'dropdown',
			'param_name'  => 'layout',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Inline', 'hdia' )    => 'inline',
				esc_html__( 'List', 'hdia' )      => 'list',
				esc_html__( '2 Columns', 'hdia' ) => 'two-columns',
			),
			'std'         => 'inline',
		),

	), Hdia_VC::get_alignment_fields(), array(
		array(
			'heading'    => esc_html__( 'Open link in a new tab', 'hdia' ),
			'type'       => 'checkbox',
			'param_name' => 'target',
			'value'      => array(
				esc_html__( 'Yes', 'hdia' ) => '1',
			),
			'std'        => '1',
		),
		array(
			'heading'    => esc_html__( 'Show tooltip as item title', 'hdia' ),
			'type'       => 'checkbox',
			'param_name' => 'tooltip_enable',
			'value'      => array(
				esc_html__( 'Yes', 'hdia' ) => '1',
			),
		),
		array(
			'heading'    => esc_html__( 'Tooltip position', 'hdia' ),
			'type'       => 'dropdown',
			'param_name' => 'tooltip_position',
			'value'      => array(
				esc_html__( 'Top', 'hdia' )    => 'top',
				esc_html__( 'Right', 'hdia' )  => 'right',
				esc_html__( 'Bottom', 'hdia' ) => 'bottom',
				esc_html__( 'Left', 'hdia' )   => 'left',
			),
			'std'        => 'top',
			'dependency' => array(
				'element' => 'tooltip_enable',
				'value'   => '1',
			),
		),
		array(
			'heading'    => esc_html__( 'Tooltip skin', 'hdia' ),
			'type'       => 'dropdown',
			'param_name' => 'tooltip_skin',
			'value'      => array(
				esc_html__( 'Default', 'hdia' ) => '',
				esc_html__( 'Primary', 'hdia' ) => 'primary',
			),
			'std'        => '',
			'dependency' => array(
				'element' => 'tooltip_enable',
				'value'   => '1',
			),
		),
		Hdia_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Items', 'hdia' ),
			'heading'    => esc_html__( 'Items', 'hdia' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array_merge( array(
				array(
					'heading'     => esc_html__( 'Title', 'hdia' ),
					'type'        => 'textfield',
					'param_name'  => 'title',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Link', 'hdia' ),
					'type'       => 'textfield',
					'param_name' => 'link',
				),
			), Hdia_VC::icon_libraries_no_depend() ),

			'value' => rawurlencode( wp_json_encode( array(
				array(
					'title'     => esc_html__( 'Twitter', 'hdia' ),
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-twitter',
				),
				array(
					'title'     => esc_html__( 'Facebook', 'hdia' ),
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-facebook',
				),
				array(
					'title'     => esc_html__( 'Vimeo', 'hdia' ),
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-vimeo',
				),
				array(
					'title'     => esc_html__( 'Linkedin', 'hdia' ),
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-linkedin',
				),
				array(
					'title'     => esc_html__( 'Pinterest', 'hdia' ),
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-pinterest',
				),
				array(
					'title'     => esc_html__( 'Google Plus', 'hdia' ),
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-googleplus',
				),
			) ) ),

		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Icon color', 'hdia' ),
			'type'             => 'dropdown',
			'param_name'       => 'icon_color',
			'value'            => array(
				esc_html__( 'Default color', 'hdia' )   => '',
				esc_html__( 'Primary color', 'hdia' )   => 'primary',
				esc_html__( 'Secondary color', 'hdia' ) => 'secondary',
				esc_html__( 'Custom color', 'hdia' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Custom icon color', 'hdia' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_icon_color',
			'dependency'       => array(
				'element' => 'icon_color',
				'value'   => 'custom',
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Icon hover color', 'hdia' ),
			'type'             => 'dropdown',
			'param_name'       => 'icon_hover_color',
			'value'            => array(
				esc_html__( 'Default color', 'hdia' )   => '',
				esc_html__( 'Primary color', 'hdia' )   => 'primary',
				esc_html__( 'Secondary color', 'hdia' ) => 'secondary',
				esc_html__( 'Custom color', 'hdia' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Custom icon hover color', 'hdia' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_icon_hover_color',
			'dependency'       => array(
				'element' => 'icon_hover_color',
				'value'   => 'custom',
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Text color', 'hdia' ),
			'type'             => 'dropdown',
			'param_name'       => 'text_color',
			'value'            => array(
				esc_html__( 'Default color', 'hdia' )   => '',
				esc_html__( 'Primary color', 'hdia' )   => 'primary',
				esc_html__( 'Secondary color', 'hdia' ) => 'secondary',
				esc_html__( 'Custom color', 'hdia' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Custom text color', 'hdia' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_text_color',
			'dependency'       => array(
				'element' => 'text_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Text hover color', 'hdia' ),
			'type'             => 'dropdown',
			'param_name'       => 'text_hover_color',
			'value'            => array(
				esc_html__( 'Default color', 'hdia' )   => '',
				esc_html__( 'Primary color', 'hdia' )   => 'primary',
				esc_html__( 'Secondary color', 'hdia' ) => 'secondary',
				esc_html__( 'Custom color', 'hdia' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Custom text hover color', 'hdia' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_text_hover_color',
			'dependency'       => array(
				'element' => 'text_hover_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Border color', 'hdia' ),
			'type'             => 'dropdown',
			'param_name'       => 'border_color',
			'value'            => array(
				esc_html__( 'Default color', 'hdia' )   => '',
				esc_html__( 'Primary color', 'hdia' )   => 'primary',
				esc_html__( 'Secondary color', 'hdia' ) => 'secondary',
				esc_html__( 'Custom color', 'hdia' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Custom border color', 'hdia' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_border_color',
			'dependency'       => array(
				'element' => 'border_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Border hover color', 'hdia' ),
			'type'             => 'dropdown',
			'param_name'       => 'border_hover_color',
			'value'            => array(
				esc_html__( 'Default color', 'hdia' )   => '',
				esc_html__( 'Primary color', 'hdia' )   => 'primary',
				esc_html__( 'Secondary color', 'hdia' ) => 'secondary',
				esc_html__( 'Custom color', 'hdia' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Custom border hover color', 'hdia' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_border_hover_color',
			'dependency'       => array(
				'element' => 'border_hover_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Background color', 'hdia' ),
			'type'             => 'dropdown',
			'param_name'       => 'background_color',
			'value'            => array(
				esc_html__( 'Default color', 'hdia' )   => '',
				esc_html__( 'Primary color', 'hdia' )   => 'primary',
				esc_html__( 'Secondary color', 'hdia' ) => 'secondary',
				esc_html__( 'Custom color', 'hdia' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Custom background color', 'hdia' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_background_color',
			'dependency'       => array(
				'element' => 'background_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Background hover color', 'hdia' ),
			'type'             => 'dropdown',
			'param_name'       => 'background_hover_color',
			'value'            => array(
				esc_html__( 'Default color', 'hdia' )   => '',
				esc_html__( 'Primary color', 'hdia' )   => 'primary',
				esc_html__( 'Secondary color', 'hdia' ) => 'secondary',
				esc_html__( 'Custom color', 'hdia' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Custom background hover color', 'hdia' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_background_hover_color',
			'dependency'       => array(
				'element' => 'background_hover_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
	) ),
) );
