<?php

class WPBakeryShortCode_TM_Timeline extends WPBakeryShortCode {
	public function get_inline_css( $selector, $atts ) {
		Hdia_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Timeline', 'hdia' ),
	'base'                      => 'tm_timeline',
	'category'                  => HDIA_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-timeline',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge(
		array(
			array(
				'heading'     => esc_html__( 'Style', 'hdia' ),
				'type'        => 'dropdown',
				'param_name'  => 'style',
				'admin_label' => true,
				'value'       => array(
					esc_html__( '01', 'hdia' ) => '01',
					esc_html__( '02', 'hdia' ) => '02',
				),
				'std'         => '01',
			),
			array(
				'group'      => esc_html__( 'Items', 'hdia' ),
				'heading'    => esc_html__( 'Items', 'hdia' ),
				'type'       => 'param_group',
				'param_name' => 'items',
				'params'     => array(
					array(
						'heading'    => esc_html__( 'Image', 'hdia' ),
						'type'       => 'attach_image',
						'param_name' => 'image',
					),
					array(
						'heading'     => esc_html__( 'Title', 'hdia' ),
						'type'        => 'textfield',
						'param_name'  => 'title',
						'admin_label' => true,
					),
					array(
						'heading'     => esc_html__( 'Date Time', 'hdia' ),
						'description' => esc_html__( 'Date and time format (yyyy/mm/dd hh:mm).', 'hdia' ),
						'type'        => 'datetimepicker',
						'param_name'  => 'datetime',
						'value'       => '',
						'admin_label' => true,
					),
					array(
						'heading'    => esc_html__( 'Text', 'hdia' ),
						'type'       => 'textarea',
						'param_name' => 'text',
					),
				),

			),
		), Hdia_VC::get_vc_spacing_tab()
	),
) );
