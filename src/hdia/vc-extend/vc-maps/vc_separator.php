<?php

vc_add_params( 'vc_separator', array(
	array(
		'heading'     => esc_html__( 'Position', 'hdia' ),
		'description' => esc_html__( 'Make the separator position absolute with column', 'hdia' ),
		'type'        => 'dropdown',
		'param_name'  => 'position',
		'value'       => array(
			esc_html__( 'None', 'hdia' )   => '',
			esc_html__( 'Top', 'hdia' )    => 'top',
			esc_html__( 'Bottom', 'hdia' ) => 'bottom',
		),
		'std'         => '',
	),
) );
