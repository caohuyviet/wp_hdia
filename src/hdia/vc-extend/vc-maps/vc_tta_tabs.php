<?php
$_color_field                                                = WPBMap::getParam( 'vc_tta_tabs', 'color' );
$_color_field['value'][ esc_html__( 'Primary', 'hdia' ) ] = 'primary';
$_color_field['std']                                         = 'primary';
vc_update_shortcode_param( 'vc_tta_tabs', $_color_field );

vc_update_shortcode_param( 'vc_tta_tabs', array(
	'param_name' => 'style',
	'value'      => array(
		esc_html__( 'Hdia 01', 'hdia' ) => 'hdia-01',
		esc_html__( 'Hdia 02', 'hdia' ) => 'hdia-02',
		esc_html__( 'Hdia 03', 'hdia' ) => 'hdia-03',
		esc_html__( 'Classic', 'hdia' )    => 'classic',
		esc_html__( 'Modern', 'hdia' )     => 'modern',
		esc_html__( 'Flat', 'hdia' )       => 'flat',
		esc_html__( 'Outline', 'hdia' )    => 'outline',
	),
) );
