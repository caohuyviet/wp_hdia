<?php
vc_update_shortcode_param( 'vc_tta_tour', array(
	'param_name' => 'style',
	'value'      => array(
		esc_html__( 'Hdia 01', 'hdia' ) => 'hdia-tour-01',
		esc_html__( 'Hdia 02', 'hdia' ) => 'hdia-tour-02',
		esc_html__( 'Hdia 03', 'hdia' ) => 'hdia-tour-03',
		esc_html__( 'Classic', 'hdia' )    => 'classic',
		esc_html__( 'Modern', 'hdia' )     => 'modern',
		esc_html__( 'Flat', 'hdia' )       => 'flat',
		esc_html__( 'Outline', 'hdia' )    => 'outline',
	),
) );
