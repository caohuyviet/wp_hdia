<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$style = $el_class = '';

$atts   = vc_map_get_attributes( $this->getShortcode(), $atts );
$css_id = uniqid( 'tm-testimonial-list-' );
$this->get_inline_css( '#' . $css_id, $atts );
extract( $atts );

$el_class  = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'tm-testimonial-list ' . $el_class, $this->settings['base'], $atts );
$css_class .= " style-$style";

$hdia_post_args = array(
	'post_type'      => 'testimonial',
	'posts_per_page' => $number,
	'orderby'        => $orderby,
	'order'          => $order,
);

if ( in_array( $orderby, array( 'meta_value', 'meta_value_num' ), true ) ) {
	$hdia_post_args['meta_key'] = $meta_key;
}

$hdia_post_args = Hdia_VC::get_tax_query_of_taxonomies( $hdia_post_args, $taxonomies );

$hdia_query = new WP_Query( $hdia_post_args );

$css_class .= Hdia_Helper::get_animation_classes();
?>
<?php if ( $hdia_query->have_posts() ) : ?>

	<div class="<?php echo esc_attr( trim( $css_class ) ); ?>" id="<?php echo esc_attr( $css_id ); ?>">

		<?php
		set_query_var( 'hdia_shortcode_atts', $atts );
		set_query_var( 'hdia_query', $hdia_query );
		get_template_part( 'loop/shortcodes/testimonial-list/style', $style );
		?>

	</div>
	<?php
	wp_reset_postdata();
endif; ?>
